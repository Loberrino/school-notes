#+title: Differential Equations
#+author: Arad Fathalian
#+startup: latexpreview
#+latex_class: article
#+latex_class_options: [letterpaper]
#+options: num:nil toc:nil
#+latex_header: \usetikzlibrary{quotes,angles,decorations.pathreplacing} \DeclareMathOperator{\atantwo}{atan2} \newcommand*\intd{\mathop{}\!\mathrm{d}} \newcommand*\intD[1]{\mathop{}\!\mathrm{d^#1}} \DeclareMathOperator{\arctantwo}{arctan2} \everymath{\displaystyle}

* What Are Differential Equations
+ Differential Equations are, simply, equations that relate derivatives to expressions
+ For instance, \(\diff{x^{n}}{x}=nx^{n-1}\) is a differential equation describing the rate of change of some exponential function \(x^{n}\)
  + We say that this rate of change is a product of \(n\) and \(x^{n-1}\) and so if \(n\) is a bigger number, the rate at which \(x^{n}\) increases is larger
  + Similarly, the rate at which \(x^{n}\) changes is also dependant on the rate at which \(x^{n-1}\) changes
+ We say that the rate at which \(x^{n}\) changes is /proportional/ to \(n\) and /proportional/ to \(x^{n-1}\), since \(n\) and \(x^{n-1}\) are factors of the rate of change of \(x^{n}\)
  + This is denoted by \(\propto\) and so we say \(\diff{x^{n}}{x}\propto n\) and \(\diff{x^{n}}{x}\propto x^{n-1}\)
+ Sometimes, we call such proportionalities /direct/ proportionalities as well
+ Suppose \(\diff{\log_{n}(x)}{x}=\frac{1}{x\ln(n)}\); now, if \(\ln(n)\) or \(x\) are larger values, that'd mean \(\log_{n}(x)\) grows at a slower rate
+ This is called an /inverse/, or indirect proportionality; we say that \(\log_{n}(x)\propto x^{-1}\) and \(\log_{n}(x)\propto\ln(n)^{-1}\)
+ Whenever we only know a proportionality, such as \(P(t)\propto P(t)^{2}\), we can express this in the form of an equation as \(\diff{P(t)}{t}=k\cdot P(t)^{2}\)
  + We don't know what \(k\) is, but it just signifies that \(\diff{P(t)}{t}\) and \(P(t)^{2}\) are not /exactly/ equal, but that \(P(t)^{2}\) is a factor of \(\diff{P(t)}{t}\) with... Something
  + \(k\) is called a proportionality constant

* Verifying Differential Equations
+ Sometimes we may be given differential equations and proposed solution functions to them, asked to verify if that function makes the equation true
+ Because we are /verifying/ an equation, we do not know if the equation is true until the end, and as such, we must work with the left and right sides separately

** Example 1
Verify that the function \(y=e^{-4x}+2x+3\) is a solution to the differential equation \(\dot{y}+4y-8x=14\)

+ Here, firstly, we are introduced to yet /another/ way to represent derivatives; \(\dot{y}=\diff{y}{x},\ \ddot{y}=\diff[2]{y}{x}\), and so on
  + This is called /Newtonian/ Notation and it is typically very uncommon and considered somewhat of a sin to try and use this notation past a second order derivative
  + \(\dddot{y}\) is just... Wrong
+ So, we must find \(\dot{y}\) by differentiating \(y\), then we can plug in the functions and see if they make the differential equation true or not
  + We can pretty easily see that if \(y=e^{-4x}+2x+3\), then \(\dot{y}=-4e^{-4x}+2\)
+ So, the left side of our differential equation is as follows:

\begin{align*}
\dot{y}+4y-8x&=\left(-4e^{-4x}+2\right)+4\left(e^{-4x}+2x+3\right)-8x \\
&=-4e^{-4x}+2+4e^{-4x}+8x+12-8x \\
&=14
\end{align*}

+ Thus, we have proven that \(y\) is the solution to this differential equation

* Slope Fields
+ Take an equation like \(\diff{y}{x}=2x\); we can integrate both sides to solve this which results in \(y=x^{2}+C\)
  + That \(+C\) at the end, as we've mentioned before, means that there is an infinite /family/ of functions that are solutions to this differential equation
+ The thing that every member of this family of functions have in common is that their "slopes", their \(\diff{y}{x}\), are equal to \(2x\)
+ So, we can express the very many solutions to this differential equation using a /Slope Field/
+ Here is the slope field for \(\diff{y}{x}=2x\):

\begin{tikzpicture}
\begin{axis}[
  grid=major,
  axis lines=left,
  ylabel=\(y\),
  xlabel=\(x\),
  xmin=-5,
  xmax=5,
  ymin=-5,
  ymax=5,
  xtick distance=1,
  ytick distance=1,
  zmin=0,
  zmax=1,
  view = {0}{90},
]
\addplot3[
  quiver = {
    u = {1},
    v = {2*x/sqrt(x^2+y^2)},
    scale arrows = 0.25,
  },
  quiver/colored=red,
  -stealth,
  domain = -5:5,
  domain y = -5:5
]{0};
\addplot[
  color=blue,
  samples=5000
]{x^2+2};
\end{axis}
\end{tikzpicture}

+ This Slope Field is our standard \(x\) and \(y\) plane, but we've placed arrows to indicate the /flow/ of the family of functions
+ All of the solutions to this function have a slope of \(2x\), so this graph describes the way that functions with a slope of \(2x\) would roughly go
+ To graph a Slope Field such as \(\diff{y}{x}=2x\), we do it coordinate by coordinate
  + So at the point \((1,2)\), for instance, we know \(x=1\), and so the differential equation tells us that at this point, \(\diff{y}{x}=2(1)\)
  + So, we place a Vector (or a line segment) at the coordinate \((1,2)\) of this graph with a slope of 2
  + Doing this for a whole bunch of points around the origin reveals a flow that looks like Parabolic
+ To find any one particular solution to this differential equation, we must be given more information about that solution; some kind of /initial condition/
  + This initial condition will reveal to us what the \(+C\) at the end /has/ to be
+ For instance, say we know there is some solution to \(\diff{y}{x}=2x\) that /has/ to intersect through the point \((2,6)\)
  + Again, we can integrate this equation to get \(y=x^{2}+C\), and then we can plug in these points to get \(6=2^{2}+C\)
  + This reveals that the particular solution to this equation we're talking about is \(y=x^{2}+2\)
+ Slope Fields are also good for finding the general flow and solutions to equations such as \(\diff{y}{x}=\frac{x}{y}\)
+ An equation we can make a slope field out of typically takes the form \(\diff{y}{x}=F(x,y)\)
+ You'll notice that if a differential is only in terms of \(x\), such as \(\diff{y}{x}=2x\), then the line segments/vectors along a vertical line are all the same slope
  + Similarly, if a differential is only in terms of \(y\), such as \(\diff{y}{x}=y-1\), then the line segments/vectors along a horizontal line are all the same slope
+ A differential in terms of both, such as \(\diff{y}{x}=\frac{y}{x}\), will have neither of these patterns; all line segments/vectors along a horizontal /or/ vertical will be different

** Example 1
Sketch the Slope Field of \(\diff{y}{x}=xy\)

+ So, again, remember that the strategy here is to start with a blank Cartesian Plane and pick sample points like \((2,3)\)
  + Putting a point like this into the equation, we get \(\diff{y}{x}=(2)(3)=6\), so at the point \((2,3)\), we place a line segment (or vector) of slope 6
+ Doing this for a whole bunch of points around the origin reveals something like this:

\begin{tikzpicture}
\begin{axis}[
  grid=major,
  axis lines=middle,
  ylabel=\(y\),
  xlabel=\(x\),
  xmin=-5,
  xmax=5,
  ymin=-5,
  ymax=5,
  xtick distance=1,
  ytick distance=1,
  zmin=0,
  zmax=1,
  view = {0}{90},
]
\addplot3[
  quiver = {
    u = {1},
    v = {(x*y)/sqrt(x^2+y^2)},
    scale arrows = 0.25,
  },
  quiver/colored=red,
  -stealth,
  domain = -5:5,
  domain y = -5:5
]{0};
\end{axis}
\end{tikzpicture}

* Euler's Method
+ Euler's Method is a way of approximating a value from a particular solution for a differential equation when given a starting condition for said solution
+ So, for instance, say we have the differential \(\diff{y}{x}=2x-y\); we don't know how to integrate this, but we know it has infinitely many solutions
  + Suppose that \(f(x)\) was one of these solutions which we knew had \(f(0)=1\)
+ We don't know what \(f(x)\) is, but say we wanted to find /roughly/ what \(f(2)\) will be
  + This is what Euler's Method is all about!
+ We begin by plugging \((0,1)\) into the differential equation to get that \(\diff{y}{x}=2(0)-1=-1\)
  + That means that a line segment, or a linear equation, intersecting \((0,1)\) with a slope of -1 exists as a solution to this differential
+ Using that, we're able to find the linear function \(y_{1}=-x+1\), which will be tangent to \(f(x)\) since it has the same slope at \((0,1)\)
  + We can go along this line towards \(x=2\) a little, but this strays us away further and further from \(f(x)\)
  + So, let's pick some arbitrary distance along the \(x\) direction to re-orient ourselves at
  + Let's say that this distance shall be \(\Delta x=\frac{1}{2}\)
+ So, we go along this line by a half, meaning we are now at the point \(\left(\frac{1}{2},y_{1}\left(\frac{1}{2}\right)\right)\)
  + Plugging in \(\frac{1}{2}\) into \(y_{1}\), we get \(y_{1}=-\frac{1}{2}+1=\frac{1}{2}\)
+ So, at this point, let's readjust the line segment we're following to again make it follow the differential equation
  + So, we plug in \(\left(\frac{1}{2},\frac{1}{2}\right)\) into the differential equation again to get \(\diff{y}{x}=2\left(\frac{1}{2}\right)-\frac{1}{2}=\frac{1}{2}\)
+ Then, we have a /new/ line with a slope of \(\frac{1}{2}\) going through the point \(\left(\frac{1}{2},\frac{1}{2}\right)\), meaning our new line is \(y_{2}=\frac{1}{2}x+\frac{1}{4}\)
+ Now, again, let's follow this line by \(\Delta x\) to get to \(x=1\), where we'll be at the point \(\left(1,y_{2}(1)\right)\), or \(\left(1,\frac{3}{4}\right)\)
+ We again readjust our line at this point to remain along the differential equation by plugging this point into the differential to get \(\diff{y}{x}=2(1)-\frac{3}{4}=\frac{5}{4}\)
+ So, the new line intersects at \(\left(1,\frac{3}{4}\right)\) with a slope of \(\frac{5}{4}\) making it \(y_{3}=\frac{5}{4}x-\frac{1}{2}\)
+ We follow this by \(\Delta x\) to get to \(x=\frac{3}{2}\) now, giving us the point \(\left(\frac{3}{2},y_{3}\left(\frac{3}{2}\right)\right)\), or \(\left(\frac{3}{2},\frac{11}{8}\right)\)
+ Again, we readjust by finding \(\diff{y}{x}=2\left(\frac{3}{2}\right)-\frac{11}{8}=\frac{13}{8}\), meaning our new line segment goes through \(\left(\frac{3}{2},\frac{11}{8}\right)\) and has slope \(\frac{13}{8}\)
+ This gives it the equation \(y_{4}=\frac{13}{8}x-\frac{17}{16}\), which following by \(\Delta x\) one more times gives us the point \(\left(2,y_{4}(2)\right)\) or \(\left(2,\frac{35}{16}\right)\)
+ Thus, we have just approximated through a series of linear equations all /roughly/ tangent to \(f(x)\) that \(f(2)\approx\frac{35}{16}\)
+ Keep in mind, the smaller those \(\Delta x\) intervals are, the better the approximation will be

** Example 1
Use Euler's Method to approximate \(f(2)\) for the function \(f(x)=\int_{0}^{x}\sqrt{1+6t^{2}}\intd t\) in two steps, beginning at \(x=0\)

+ Firstly, the question wants us to use Euler's Method in only two steps, so that means we're only allowed to make use of two linear equation approximations
  + To find what \(\Delta x\) must be, then, we just do \(\frac{2-0}{2}=1\); I.E. we find the distance between our start and end points and divide by the number of steps
+ So, beginning at \(x=0\) then, we have the point \(\left(0,f(0)\right)\) where \(f(0)=\int_{0}^{0}\sqrt{1+6t^{2}}\intd t=0\)
  + Knowing that now, we must find the slope of a line tangent to \(f(x)\) at \((0,0)\), which we can do by differentiating \(f(x)\)
  + The fundamental theorem of Calculus states that \(\diff{f(x)}{x}=\sqrt{1+6x^{2}}\), and so that is really what our differential equation here will be
+ Plugging \((0,0)\) into \(\diff{f(x)}{x}\) then, we get that there is a line intersecting \((0,0)\) with slope 1, meaning our first line is \(y_{1}=x\)
+ Now, we go along the line by \(\Delta x\) to reach \(x=1\), where we are at the point \(\left(1,y_{1}(1)\right)\) or \((1,1)\), where we re-orient the line to stay along the differential
+ So, we plug \((1,1)\) into \(\diff{f(x)}{x}\) to get that our new line must intersect \((1,1)\) with a slope of \(\sqrt{7}\)
+ Thus, the equation of this new line is \(y_{2}=\sqrt{7}x+\left(1-\sqrt{7}\right)\), which we can follow along \(\Delta x\) to get to \(x=2\)
+ Now then, just plugging 2 into this equation, we get \(\sqrt{7}(2)+1-\sqrt{7}\), meaning that \(f(2)\approx\sqrt{7}+1\)

* Solving Differential Equations By Separating the Variables
+ Usually when we have a differential only in terms of \(x\), such as \(\diff{y}{x}=2x-1\), this is pretty trivial to solve; we just integrate both sides
+ So for instance, here, we get \(\int\diff{y}{x}=\int2x-1\)... Except that we're missing something here
  + We have to move that \(\intd x\) over to the other side to get \(\int\intd y=\int2x-1\intd x\), which becomes \(y=x^{2}-x+C\)
+ We can do something similar whenever we have differentials of the form \(\diff{y}{x}=f(x)g(y)\) or \(\diff{y}{x}=\frac{f(x)}{g(y)}\) or \(\diff{y}{x}=\frac{g(y)}{f(x)}\)

** Example 1
Find the general solution to the differential equation \(\diff{y}{x}=4xy^{2}\cos\left(x^{2}\right)\)

+ So, like we just did, let's integrate both sides to get \(\int\diff{y}{x}=\int4xy^{2}\cos\left(x^{2}\right)\)
+ We can move the \(\intd x\) over, but there's no reason we can't move the \(y^{2}\) over too
  + That'd give us \(\int\frac{\intd y}{y^{2}}=\int4x\cos\left(x^{2}\right)\intd x\), or written in a slightly more familiar way, \(\int\frac{1}{y^{2}}\intd y=\int4x\cos\left(x^{2}\right)\intd x\)
+ Look at that! Those are both integrals we're able to do
+ The \(y\) integral just uses power rule giving us \(-y^{-1}=\int4x\cos\left(x^{2}\right)\intd x\)
+ The second integral can be made simpler by pulling the 4 out to get \(4\int x\cos\left(x^{2}\right)\intd x\), where we can now clearly see a substitution that can be made
  + Letting \(u=x^{2}\) means \(\diff{u}{x}=2x\) or that \(\frac{\intd u}{2}=x\intd x\), so we have \(2\int\cos(u)\intd u\)
+ In total then, this equation becomes \(-y^{-1}=2\sin\left(x^{2}\right)+C\), and we can now just isolate for \(y\) to get \(y=\frac{-1}{2\sin\left(x^{2}\right)}+C\)
+ So, the general solution to the differential equation \(\diff{y}{x}=4xy^{2}\cos\left(x^{2}\right)\) is the family of functions \(y=\left(2\sin\left(x^{2}\right)\right)^{-1}+C\)

* Exponential and Logistic Models
+ Whenever we have a differential of the form \(\diff{y}{x}=ky\) for some constant \(k\), we generally say that this is an exponential model
+ To see why, let's try and get a general solution here by separating the variables!
+ This gets us the equation \(\frac{\intd y}{y}=k\intd x\), which can be turned into \(\int\frac{1}{y}\intd y=\int k\intd x\), which becomes \(\ln(y)=kx+C\)
+ Isolating for \(y\) results in \(y=e^{kx+C}=e^{C}e^{kx}\), and since \(C\) is meant to represent just some constant, \(e^{C}\) still represents, well, just some constant
  + Thus, this can /really/ be written as \(y=Ce^{kx}\) by saying that, as far as what \(C\) is meant to represent, \(e^{C}\equiv C\)
+ The problem with these Exponential models, however, is that they exhibit unbounded behaviour; for some exponential model \(f(x)\), \(\lim_{x\to\infty}f(x)=\infty\)
  + This is not very accurate when modelling something like population size; here, there is a limit/horizontal asymptote eventually
+ So, /Logistic/ Models exhibit exponential growth but then plateau towards some horizontal asymptote
+ They are modelled by the differential equation \(\diff{y}{x}=ky(a-y)\) and general solutions are of the form \(y=\frac{a}{1+be^{-akt}}\)
  + Here, as \(x\) approaches \(\infty\), the term \(be^{-akt}\) becomes smaller and smaller, leading this function to approach a value of \(\frac{a}{1}\)
+ So, there is a horizontal asymptote at \(y=a\); generally with logistic models \(f(x)\), \(\lim_{x\to\infty}f(x)=a\), and \(\lim_{x\to-\infty}f(x)=0\)
+ Because the differential is of the form \(\diff{y}{x}=ky(a-y)\), you can figure that for logistic models, \(\diff{y}{x}=0\) when \(y=0\) or when \(y=a\)
+ There is a point of inflection at \(y=\frac{a}{2}\)
