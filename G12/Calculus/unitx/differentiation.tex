%& /home/arad/.emacs.d/.local/cache/org/persist/c6/8e20e1-327c-4a9c-85f3-f9609813a887-6142785011fcc40616f6059911b40dbc
% Created 2024-10-03 Thu 22:51
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage{capt-of}
\usepackage[fixamsmath]{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{longtable}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[version=4]{mhchem}
\usepackage{chemfig}
\usepackage{gensymb}
\usepackage[product-units=bracket-power, sticky-per]{siunitx}
\usepackage{cancel}
\usepackage{fourier}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{quotes,angles,decorations.pathreplacing} \everymath{\displaystyle} \pgfplotsset{width=10cm,compat=1.18}

%% ox-latex features:
%   !announce-start, !guess-pollyglossia, !guess-babel, !guess-inputenc, maths,
%   !announce-end.

\usepackage{amsmath}
\usepackage{amssymb}

%% end ox-latex features


% end precompiled preamble
\ifcsname endofdump\endcsname\endofdump\fi

\author{Arad Fathalian}
\date{\today}
\title{Differentiation}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Differentiation},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Introduction to Calculus}
\label{sec:org156bca5}
\begin{itemize}
\item Calculus essentially was created as a way of quantifying situations involving \textbf{Limits}
\begin{itemize}
\item Limits are a concept where we observe the trend of what an expression becomes equal to as we slowly make a value in that expression \emph{approach} some given value
\item This can often tell us what the value of the expression is when we substitute in the value that our limit approaches
\end{itemize}
\item For instance, we can try and calculate \(\lim_{x\to4}f(x)\) where \(f(x)=3x+2\)
\item This new notation presents us with a limit and asks of us ``What value does \(f(x)\) approach as \(x\) becomes closer and closer to a value of 4?''
\begin{itemize}
\item As we approach the value of 4 from the \emph{negative} side (approach 4 from numbers less than 4) using things such as \(x=3.9\), then \(x=3.99\), and \(x=3.999\), we can slowly see our number approaching 14
\item As we approach the value of 4 from the \emph{positive} side (approach 4 from numbers greater than 4) using things such as \(x=4.1\), then \(x=4.01\), and \(x=4.001\), we can also slowly see our number approaching 14
\end{itemize}
\item So, we can say \(\lim_{x\to4}f(x)=14\), and we can summarize this as:
\end{itemize}

\begin{align*}
f(x)&=3x+2 \\
\lim_{x\to4}f(x)&=14 \\
\because \lim_{x\to4^{+}}f(x)=14 &\land \lim_{x\to4^{-}}f(x)=14
\end{align*}

\begin{itemize}
\item Here, we're saying that as we approach the value of 4 for \(x\) from the positive side, we approach 14, and as we approach the value of 4 for \(x\) from the negative side, we also approach 14
\item Fundamentally, Calculus explores two different subjects: Differentiation, and Integration
\item Differentiation explores how to get the \textbf{rate of change} of a relation at any given moment, also called the \textbf{Derivative} of the relation
\begin{itemize}
\item This means we already know how to get the Derivative of a Linear Function; it's simply going to be the Slope of the Function, but for any other type of Function such as a Parabola, we'll need Differentiation
\end{itemize}
\item Integration now explores the \emph{inverse} of Differentiation; Integration shows how to get the Area between a relation's line and the x-axis, also called the \textbf{Integral} of the relation
\item These two are considered Inverses because if Derivatives fundamentally use the formula \(\frac{\Delta y}{\Delta x}\), then Integrals fundamentally use the formula \(\Delta x\cdot\Delta y\) which when put together, cancel each other
\end{itemize}
\section*{Differentiating Using Tangent Lines}
\label{sec:org535a171}
\subsection*{Defining Tangent and Secant Lines}
\label{sec:orgf9a14f6}
\begin{itemize}
\item Let's imagine a Unit Circle (Circle with \(r=1\)) with a line intersecting the Circumference at only one point:
\end{itemize}

\begin{tikzpicture}
\draw[very thick] (0,0) circle (2);
\filldraw (0,0) circle (1pt);
\draw[very thick,<->] (2,-2) -- (2,2) node[right,midway] {\(t\)};
\draw (0,0) coordinate (B) -- (2,0) coordinate (A) node[above,midway] {\(r\)};
\draw (0,0) -- (2,2) node[midway,left,above] {\(a\)};
\draw (0,0) -- (2,-2) coordinate (C) node[midway,left,below] {\(b\)};
\draw pic["\(\theta\)", draw, <->, angle eccentricity=1.25, angle radius=0.75cm]
    {angle=C--B--A};
\end{tikzpicture}

\begin{itemize}
\item Because this is the unit Circle, we know that line \(r\) coming out of the origin is equal to 1
\item That means that in this case, \(\tan(\theta)=\frac{t}{1}\) where \(t\) is the length of the line intersecting the Circle at one point
\item This is why we call lines that intersect with shapes and functions at \emph{only one point} Tangent Lines
\item Now, in this same situation we also know that \(\sec(\theta)=\frac{b}{1}\) where if we extend line \(b\), we'd see that it eventually intersects with the Circle \emph{twice}
\item This is why we call lines that intersect with shapes and functions at \emph{two} points Secant Lines
\end{itemize}
\subsection*{Using Tangent and Secant Lines to Differentiate}
\label{sec:orgf9009b6}
\begin{itemize}
\item Imagine we have the Function \(f(x)=x^{2}\) and we want to find the equation of a Tangent Line intersecting it at \((2,4)\)
\item To do this, we need to either know 2 different points along this Tangent Line, or we need to know one point alongside either the y-intercept or the slope
\begin{itemize}
\item For the moment, all we know about this Tangent Line is that it has the point \((2,4)\)
\end{itemize}
\item While we cannot find the equation of the Tangent Line, we can create a Secant Line intersecting through \((2,4)\) and some other point
\begin{itemize}
\item We know that any point along a Function will follow the general form \((x,f(x))\) or in this case, \((x,x^{2})\)
\end{itemize}
\item So, we can know that the slope of some Secant Line intersecting with \((2,4)\) and another point will be \(\frac{x^{2}-4}{x-2}\) which we can factor into \(\frac{(x+2)(x-2)}{x-2}\) to get \(x+2\)
\item As we substitute points in for \(x\) that become closer and closer to 1, which is the x-coordinate of our point of concern, we see that the slope approaches a value of 4
\begin{itemize}
\item In other words, \(\lim_{x\to2}x+2=4\)
\end{itemize}
\item Because as we push the arbitrary point in our Secant Line closer and closer to \((2,4)\) we see that the slope of the line approaches 4, it's safe to say that the slope of the Tangent Line at \((2,4)\) is 4
\item Because of that, we know that the overall equation for our Tangent Line at \((2,4)\) must be \(y=4x-4\)
\item However, another thing to note from this is that we just found a \textbf{rate of change} at point \((2,4)\) of the Function \(f(x)=x^{2}\), being 4
\begin{itemize}
\item Therefore, we just got the derivative of that point
\end{itemize}
\end{itemize}
\subsection*{Generalizing this process}
\label{sec:org20f8f92}
\begin{itemize}
\item If we wanna get the slope of the Tangent Line at a certain point along a Function, we can get the Secant Line of that point and some other point and then find the limit of the slope as the second point approaches the first
\item If we let \(h\) be the horizontal distance between our point of interest and the arbitrary point, then we can make the formula \(m=\lim_{h\to0}\frac{f(x+h)-f(x)}{h}\)
\end{itemize}
\section*{Differentiation Notation}
\label{sec:org42c485a}
\begin{itemize}
\item There are two main ways to express the Derivative of a Function \(f(x)\)
\item The first is called Lagrange's Notation, named after Joseph Louis Lagrange who popularized it after Euler invented it
\item To represent the derivative of a Function in this notation, we simply place a prime mark around the Function, such as with \(f'(x)\)
\item The more universally employed notation, however, is Leibniz's Notation
\item Leibniz's Notation uses the slope formula \(\frac{\Delta y}{\Delta x}\), but uses \(d\)'s in place of \(\Delta\)
\begin{itemize}
\item Why did he do this? I don't really know; I think it's stupid personally, and I think \(\frac{\Delta}{\Delta x}f(x)\) makes more symbolic sense, but using \(\frac{\Delta}{\Delta x}f(x)\) will likely cause confusion because \(\frac{d}{dx}f(x)\) is just the standard
\end{itemize}
\item Now, because \(y=f(x)\), the final form of the Notation becomes \(\frac{df(x)}{dx}\), or more commonly written as \(\frac{d}{dx}f(x)\); this means to get the derivative of \(f(x)\) with respect to \(x\)
\end{itemize}
\section*{Laws of Limits}
\label{sec:orgd8f9da6}
\begin{itemize}
\item When evaluating Limits, there are some rules which makes this much easier to do
\item Firstly, when getting the Limit of an addition or subtraction of Functions, we could get the Limit of the Functions individually then add or subtract the Limits
\begin{itemize}
\item \(\lim_{x\to a}(f(x)\pm g(x))=\lim_{x\to a}f(x)\pm\lim_{x\to a}g(x)\)
\end{itemize}
\item In fact, the same general process can be done with the Limit of a product or quotient of Functions
\begin{itemize}
\item \(\lim_{x\to a}(f(x)\cdot g(x))=\lim_{x\to a}f(x)\cdot\lim_{x\to a}g(x) \land \lim_{x\to a}\left(\frac{f(x)}{g(x)}\right)=\frac{\lim_{x\to a}f(x)}{\lim_{x\to a}g(x)}\), but remember that for the Quotient Law, \(\lim_{x\to a}g(x)\neq0\)
\end{itemize}
\item As well, the Limit of a Constant is always going to be equal to the Constant itself
\begin{itemize}
\item \(\lim_{x\to a}C=C\)
\end{itemize}
\item Lastly, the limit of some Constant multiplied by a Function is equal to the Limit of the Function multiplied by the Constant
\begin{itemize}
\item \(\lim_{x\to a}(cf(x))=c\lim_{x\to a}f(x)\)
\end{itemize}
\item Finally, if \(n\) is positive, then the following two laws are also true:
\item \(\lim_{x\to a}(f(x))^{n}=(\lim_{x\to a}f(x))^{n}\)
\item \(\lim_{x\to a}x^{n}=a^{n}\)
\end{itemize}
\section*{Derivatives and Derivative Rules}
\label{sec:orgb742d54}
\begin{itemize}
\item So far, we've only been getting the Derivative of single points along Functions; the rate of change at that point of the Function alone
\item However, it's possible - and most common - to get the Derivative of an entire Function
\begin{itemize}
\item This will give you another Function which itself models the rate of change of the first Function
\end{itemize}
\item In Physics, the Derivative of a P-t graph is a V-t graph, for instance
\item Earlier, we defined \(\frac{d}{dx}f(x)=\lim_{h\to0}\frac{f(x+h)-f(x)}{h}\) where \(h\) is the horizontal distance between our point of interest and some other arbitrary point along the same Function
\end{itemize}
\subsection*{The Power Rule}
\label{sec:org18567fd}
\begin{itemize}
\item Let's use this definition to get the Derivatives of some basic Functions
\item For \(f(x)=12\) for instance, we're simply going to get a flat horizontal line at \(y=12\), which means there is never a slope and so here, \(\frac{d}{dx}f(x)=0\)
\begin{itemize}
\item In fact, this is the case for the Derivative of all Constant Functions
\end{itemize}
\item For \(f(x)=x\), our formula becomes \(\frac{d}{dx}f(x)=\lim_{h\to0}\frac{(x+h)-x}{h}=1\), and so the derivative of all base Linear Functions is 1
\item For \(f(x)=x^{2}\), our formula becomes \(\frac{d}{dx}f(x)=\lim_{h\to0}\frac{(x+h)^{2}-x^{2}}{h}=\lim_{h\to0}\frac{x^{2}+2xh+h^{2}-x^{2}}{h}=\lim_{h\to0}2x+h\)
\begin{itemize}
\item As \(h\) approaches 0, our derivative for the base Quadratic becomes \(2x\)
\end{itemize}
\item For \(f(x)=x^{3}\), our formula becomes \(\frac{d}{dx}f(x)=\lim_{h\to0}\frac{(x+h)^{3}-x^{3}}{h}=\frac{x^{3}+3x^{2}h+3xh^{2}+h^{3}-x^{3}}{h}=3x^{2}+3xh+h^{2}\)
\begin{itemize}
\item As \(h\) approaches 0, our derivative for the base Cubic becomes \(3x^{2}\) because all other terms cancel out
\end{itemize}
\item This reveals a pattern we shall define as the Power Rule: \(\frac{d}{dx}x^{n}=nx^{n-1}\)
\end{itemize}
\subsection*{Other Derivative Rules}
\label{sec:orgcb08e79}
\begin{itemize}
\item If you are taking the Derivative of a Function multiplied by a Constant, just take the Derivative of the Function, then multiply that by the Constant
\begin{itemize}
\item \((cf(x))'=c\cdot\frac{d}{dx}f(x)\)
\end{itemize}
\item If you're taking the Derivative of some quantity plus or minus some other, you can take the Derivative of the quantities separately, then add or subtract the Derivatives
\begin{itemize}
\item \(\frac{d}{dx}(f(x)\pm g(x))=f'(x)\pm g'(x)\)
\end{itemize}
\item If you're taking the Derivative of some quantity multiplied by some other, you can apply the following formula:
\begin{itemize}
\item \(\frac{d}{dx}(f(x)\cdot g(x))=g(x)f'(x)+f(x)g'(x)\)
\end{itemize}
\item If you're taking the Derivative of some quantity over some other, you can apply the following formula:
\begin{itemize}
\item \(\frac{d}{dx}\left(\frac{f(x)}{g(x)}\right)=\frac{g(x)f'(x)-f(x)g'(x)}{g^{2}(x)}\)
\end{itemize}
\end{itemize}
\subsection*{Example 1}
\label{sec:org579ddfe}
\[f(x)=x^{6}+2x^{4}-3x^{2}+2\]

\begin{itemize}
\item We have our addition and subtraction rule which says we can get the Derivative of each term in this Function separately, then add the Derivatives
\item As well, our Power Rule and Constant Rule tell us how to handle Derivatives of terms that are of the form \(x^{n}\):
\end{itemize}

\begin{align*}
\frac{d}{dx}f(x)&=\left(x^{6}\right)'+2\left(x^{4}\right)'-3\left(x^{2}\right)'+2\left(x^{0}\right)' \\
&=6x^{5}+8x^{3}-6x
\end{align*}
\subsection*{Example 2}
\label{sec:org996dee0}
\[f(x)=\left(x^{3}\right)(2x)\]

\begin{itemize}
\item Here, we'll need to utilize our product rule:
\end{itemize}

\begin{align*}
\frac{d}{dx}f(x)&=(2x)\left(x^{3}\right)'+\left(x^{3}\right)(2x)' \\
&=(2x)\left(3x^{2}\right)+\left(x^{3}\right)(2) \\
&=8x^{3}
\end{align*}
\subsection*{Example 3}
\label{sec:org7401c0e}
\[f(x)=\frac{x^{2}+1}{x+2}\]

\begin{itemize}
\item Here, we'll need to utilize our quotient rule:
\end{itemize}

\begin{align*}
\frac{d}{dx}f(x)&=\frac{(x+2)(x^{2}+1)'-(x^{2}+1)(x+2)'}{(x+2)^{2}} \\
&=\frac{(x+2)(2x)-(x^{2}+1)}{x^{2}+4x+4} \\
&=\frac{2x^{2}+4x-x^{2}-1}{x^{2}+4x+4} \\
&=\frac{x^{2}+4x-1}{x^{2}+4x+4}
\end{align*}
\section*{Derivatives of Trigonometric Functions}
\label{sec:orga4b3bfb}
\begin{itemize}
\item If you try to create a Derivative graph for \(f(x)=\sin(x)\), it turns out you'd actually end up with a graph for \(\cos(x)\), meaning \(\frac{d}{dx}\sin(x)=\cos(x)\)
\item As well, if you tried to create a Derivative graph for \(f(x)=\cos(x)\), it turns out that \(\frac{d}{dx}\cos(x)=-\sin(x)\) which also has the implication that \(\frac{d}{dx}-\cos(x)=\sin(x)\)
\item Using just these two definitions, we can find the Derivative of all other Trigonometric Functions
\end{itemize}
\subsection*{Derivative of Tangent}
\label{sec:org3a8b485}
\begin{align*}
\tan(x)&=\frac{\sin(x)}{\cos(x)} \\
\frac{d}{dx}\tan(x)&=\frac{d}{dx}\left(\frac{\sin(x)}{\cos(x)}\right) \\
&=\frac{\cos(x)\sin'(x)-\sin(x)\cos'(x)}{\cos^{2}(x)} \\
&=\frac{\cos^{2}(x)+\sin^{2}(x)}{\cos^{2}(x)} \\
&=\frac{1}{\cos^{2}(x)} \\
&=\sec^{2}(x)
\end{align*}
\subsection*{Derivative of Cosecant}
\label{sec:org01f1872}
\begin{align*}
\csc(x)&=\frac{1}{\sin(x)} \\
\frac{d}{dx}\csc(x)&=\frac{d}{dx}\left(\frac{1}{\sin(x)}\right) \\
&=\frac{\sin(x)(1)'-(1)\sin'(x)}{\sin^{2}(x)} \\
&=-\frac{\cos(x)}{\sin^{2}(x)} \lor =-\cot(x)\csc(x)
\end{align*}
\subsection*{Derivative of Secant}
\label{sec:orge82d809}
\begin{align*}
\sec(x)&=\frac{1}{\cos(x)} \\
\frac{d}{dx}\sec(x)&=\frac{d}{dx}\left(\frac{1}{\cos(x)}\right) \\
&=\frac{\cos(x)(1)'-(1)\cos'(x)}{\cos^{2}(x)} \\
&=\frac{\sin(x)}{\cos^{2}(x)} \lor =\tan(x)\sec(x)
\end{align*}
\subsection*{Derivative of Cotangent}
\label{sec:org335a521}
\begin{align*}
\cot(x)&=\frac{\cos(x)}{\sin(x)} \\
\frac{d}{dx}\cot(x)&=\frac{d}{dx}\left(\frac{\cos(x)}{\sin(x)}\right) \\
&=\frac{\sin(x)\cos'(x)-\cos(x)\sin'(x)}{\sin^{2}(x)} \\
&=\frac{-\sin^{2}(x)-\cos^{2}(x)}{\sin^{2}(x)} \\
&=\frac{-1(\sin^{2}(x)+\cos^{2}(x))}{\sin^{2}(x)} \\
&=\frac{-1}{\sin^{2}(x)} \\
&=-\csc^{2}(x)
\end{align*}
\subsection*{Derivatives of Inverse Trigonometric Functions}
\label{sec:org66918e4}
\begin{align*}
\frac{d}{dx}\arcsin(x)&=(1-x^{2})^{-\frac{1}{2}} \\
\frac{d}{dx}\arccos(x)&=-(1-x^{2})^{-\frac{1}{2}} \lor =-\frac{d}{dx}\arcsin(x) \\
\frac{d}{dx}\arctan(x)&=(1+x^{2})^{-1}
\end{align*}
\section*{Derivatives of Composite Functions}
\label{sec:org00f9892}
\begin{itemize}
\item If you wanted to find something like \(\frac{d}{dx}\sqrt{x^{2}+1}\) or \(\frac{d}{dx}\sin\left(x^{2}\right)\), you'd quickly find that this is not something that's possible within our current set of Derivative Rules
\item For more complex Functions like these that combine various Function types, we can use the \textbf{Chain Rule} to derive them as long as we can find a way to express these Functions as Composite Functions
\item The Chain Rule states that if you wanna derive a Composite Function, get the derivative of the outer Function while leaving the inner one untouched, and then multiply that by the derivative of the inner Function
\begin{itemize}
\item \(\frac{d}{dx}f(g(x))=f'(g(x))\cdot g'(x)\)
\end{itemize}
\item So, for instance, we could rewrite the Function \(\sqrt{x^{2}+1}\) as \(f(x)=x^{\frac{1}{2}},\ g(x)=x^{2}+1\), and our total Function is now going to be \(f(g(x))\), meaning we can now derive this using the Chain Rule
\item What that would look like, using our Chain Rule formula, is \(\frac{d}{dx}f(g(x))=\left(x^{2}+1\right)^{\frac{1}{2}\prime}\cdot\left(x^{2}+1\right)'\)
\item Remember that for the first half of this Derivative, we're \emph{only} taking the Derivative of the \(x^{\frac{1}{2}}\) Function; what's inside those brackets gets left alone
\item This means we end up with \(\frac{d}{dx}f(g(x))=\frac{1}{2}\left(x^{2}+1\right)^{-\frac{1}{2}}\cdot2x\) which then becomes \(\frac{d}{dx}f(g(x))=\frac{x}{\sqrt{x^{2}+1}}\)
\end{itemize}
\subsection*{Example 1}
\label{sec:org151fe69}
\[\frac{d}{dx}\left(\sin(\cos(\tan(x)))\right)\]

\begin{itemize}
\item Here, we can say that \(f(x)=\sin(x)\) and \(g(x)=\cos(\tan(x))\) to get \(f(g(x))\):
\end{itemize}

\begin{align*}
\frac{d}{dx}f(g(x))&=\sin(\cos(\tan(x)))'\cdot(\cos(\tan(x)))' \\
&=\cos(\cos(\tan(x)))\cdot(\cos(\tan(x)))'
\end{align*}

\begin{itemize}
\item Now, that second Derivative requires us to use Chain Rule \emph{again}; this necessity of nesting operations to do certain things is where a lot of the difficulty in Calculus comes from
\end{itemize}

\begin{align*}
\frac{d}{dx}f(g(x))&=\cos(\cos(\tan(x)))\cdot\cos(\tan(x))'\cdot\tan(x)' \\
&=\cos(\cos(\tan(x)))\cdot-\sin(\tan(x))\cdot\sec^{2}(x) \\
&=-\cos(\cos(\tan(x)))\cdot\sin(\tan(x))\cdot\sec^{2}(x)
\end{align*}
\subsection*{Example 2}
\label{sec:org9a5fee9}
\[\frac{d}{dx}\left(x^{2}\sin\left(\sin\left(x^{5}\right)\right)\right)\]

\begin{itemize}
\item Here, our outside function is going to turn out to be \(x^{2}\cdot\sin(x)\), and so we're going to need to utilize Product Rule to derive the outside Function while keeping the \(\sin\left(x^{5}\right)\) where it's supposed to be
\item Then, at the end, we're going to multiply by the Derivative of \(\sin\left(x^{5}\right)\) which itself is going to need Chain Rule
\end{itemize}

\begin{align*}
f(x)&=x^{2}\cdot\sin(x) \\
g(x)&=\sin\left(x^{5}\right) \\
\frac{d}{dx}f(g(x))&=2x\sin\left(\sin\left(x^{5}\right)\right)+x^{2}\cos\left(\sin\left(x^{5}\right)\right)\cdot\left(\sin(x^{5})\right)' \\
&=2x\sin\left(\sin\left(x^{5}\right)\right)+x^{2}\cos\left(\sin\left(x^{5}\right)\right)\cdot\cos\left(x^{5}\right)\cdot5x^{4} \\
&=5x^{6}\cos\left(x^{5}\right)\cos\left(\sin\left(x^{5}\right)\right)+2x\sin\left(\sin\left(x^{5}\right)\right)
\end{align*}
\section*{Derivatives of Logarithmic and Exponential Functions}
\label{sec:orgc4adfbb}
\begin{itemize}
\item Recall that \(\log_{e}(x)=\ln(x)\) where \(e\) is what is referred to as the Natural Base, where it can roughly be said \(e\approx2.7182818\ldots\)
\item It turns out that in every case, \(\frac{d}{dx}\ln(x)=\frac{1}{x}\)
\item Because of this, alongside the change-of-base formula which states \(\log_{b}(x)=\frac{\ln(x)}{\ln(b)}\), we can simply say \(\frac{d}{dx}\log_{b}(x)=\left(\frac{\ln(x)}{\ln(b)}\right)'\)
\item And so, simplifying this with the derivative of \(\ln(x)\), we know that \(\frac{d}{dx}\log_{b}(x)=\frac{1}{x\ln(b)}\)
\item As well, finally, to differentiate Exponential Functions, we can simply follow the formula \(\frac{d}{dx}\left(b^{x}\right)=(\ln(b))\left(b^{x}\right)\)
\end{itemize}
\subsection*{Why the exponential differentiation formula works}
\label{sec:org8295651}
\begin{itemize}
\item The defining feature of \(e\) is that it can be used as a base in the function \(e^{x}\), and it turns out, \(\frac{d}{dx}e^{x}=e^{x}\)
\begin{itemize}
\item \(e^{x}\) is the only function which is it's own derivative, and this is the main thing that makes \(e\) as a whole so special
\end{itemize}
\item Additionally, as we explained, \(\ln(x)=\log_{e}(x)\) such that \(e^{\ln(x)}=x\)
\item Finally, the chain rule explains that \(\frac{d}{dx}f(g(x))=f'(g(x))\cdot g'(x)\)
\item So, because \(e^{\ln(x)}=x\), we can express something like \(2^{x}\) as \(e^{\ln(2)^{x}}\) which then becomes \(e^{\ln(2)x}\)
\item That would also mean that \(\frac{d}{dx}2^{x}=\frac{d}{dx}e^{\ln(2)x}\)
\item We can differentiate \(e^{\ln(2)x}\) using the chain rule; the derivative of any function \(e^{n}\) is \(e^{n}\) and the derivative of \(\ln(2)x\) is simply \(\ln(2)\), so \(\frac{d}{dx}2^{x}=e^{\ln(2)x}\ln(2)\)
\item However, \(e^{\ln(2)x}\) simplifies into \(2^{x}\) and so finally, \(\frac{d}{dx}2^{x}=2^{x}\ln(2)\), and there's nothing special about 2 as the base here; we could in general state that \(\frac{d}{dx}n^{x}=n^{x}\ln(n)\)
\end{itemize}
\subsection*{Example 1}
\label{sec:org9613dff}
\[\frac{d}{dx}\log_{5}\left(x^{2}\right)\]

\begin{itemize}
\item Here, we're going to need to utilize Chain Rule alongside what we learned about the Derivative of Logarithms
\end{itemize}

\begin{align*}
f(x)&=\log_{5}(x) \\
g(x)&=x^{2} \\
\frac{d}{dx}f(g(x))&=\log_{5}'\left(x^{2}\right)\cdot2x \\
&=\frac{2x}{x^{2}\ln(5)} \\
&=\frac{2}{x\ln(5)}
\end{align*}
\subsection*{Example 2}
\label{sec:org58734a9}
\[\frac{d}{dx}x^{2}\ln\left(x^{5}\right)\]

\begin{itemize}
\item Again, we must utilize Chain Rule, except now the derivative of the outer Function requires us to use Product Rule as well
\end{itemize}

\begin{align*}
f(x)&=x^{2}\ln(x) \\
g(x)&=x^{5} \\
\frac{d}{dx}f(g(x))&=2x\ln\left(x^{5}\right)+\frac{x^{2}}{x^{5}}\cdot5x^{4} \\
&=2x\ln\left(x^{5}\right)+\frac{5x^{6}}{x^{5}} \\
&=2x\ln\left(x^{5}\right)+5x
\end{align*}
\subsection*{Example 3}
\label{sec:org6e7a110}
\[\frac{d}{dx}2^{x}+12\]

\begin{itemize}
\item For this Derivative, just remember that \(\frac{d}{dx}b^{x}=(\ln(b))\left(b^{x}\right)\):
\end{itemize}

\begin{align*}
\frac{d}{dx}2^{x}+12&=\ln(2)2^{x}+(12)' \\
&=2^{x}\ln(2)
\end{align*}
\subsection*{Example 4}
\label{sec:org460d36c}
\[\frac{d}{dx}2^{3x+1}\]

\begin{itemize}
\item This is going to be \emph{slightly} more complicated as we now need to use Chain Rule as well
\end{itemize}

\begin{align*}
f(x)&=2^{x} \\
g(x)&=3x+1 \\
\frac{d}{dx}f(g(x))&=\ln(2)2^{3x+1}\cdot3 \\
&=3\ln(2)2^{3x+1}
\end{align*}
\section*{Implicit Differentiation}
\label{sec:orgca1f141}
\begin{itemize}
\item In the Functions we've been taking the Derivatives of so far, \(y\) has acted as a Function of \(x\), meaning we were always able to say \(f(x)=y\)
\item What about Functions were there is more than 1 variable, however, such as \(x^{2}+y^{2}=9\)?
\item In some cases, such as that one, we are simply able to rearrange to form our traditional Function structure, such as \(y=\sqrt{9-x^{2}}\) in the above, but this will not always be possible
\item For this reason, \textbf{Implicit Differentiation} allows us to take the Derivative of \(y\) in these Functions using Chain Rule, since \(y=f(x)\), and then we isolate for the Derivative of \(y\)
\end{itemize}
\subsection*{Example 1}
\label{sec:org415f3e0}
\[\frac{d}{dx}x^{2}+y^{2}=9\]

\begin{itemize}
\item Here, our notation for the Derivative we are taking tells us that this Derivative will be \textbf{with respect to} \(x\), and so we are able to take the Derivative of the \(x\) term as we always would, since that is just our input variable
\item For \(y^{2}\), however, we must act as if instead of \(y\), we have written \(f(x)\), since \(y\) is a Function of \(x\), meaning for that term, we must use Chain Rule
\item That will look like the following:
\end{itemize}

\begin{align*}
2x+2y\frac{dy}{dx}&=0 \\
\frac{dy}{dx}&=\frac{-2x}{2y} \\
\frac{dy}{dx}&=-\frac{x}{y}
\end{align*}

\begin{itemize}
\item And since \(f(x)=y\), this is equivalent to writing \(\frac{d}{dx}f(x)=-\frac{x}{y}\)
\item In the above, for the \(y^{2}\) term, we separated the term into the Derivative of the Outer Function (being the power of 2) and the inner Function (being just \(y\) itself)
\item By doing this, we were able to use the Chain Rule to multiply the Derivative of the Outer Function with the Inner Function left untouched by the Derivative of the Inner Function
\item The only thing we did different to any other instance of using the Chain Rule, in fact, is that we did not evaluate the Derivative of \(y\) itself, for that is what we were isolating for
\end{itemize}
\subsection*{Example 2}
\label{sec:org2911491}
\[\frac{d}{dx}\sin(xy)=\frac{1}{2}\]

\begin{itemize}
\item Here we are going to try the same approach; evaluate the Derivative of \(x\)'s as we always would, and evaluate the Derivative of \(y\)'s as if it were a Function:
\end{itemize}

\begin{align*}
0&=\cos(xy)\cdot(xy)' \\
&=\cos(xy)\cdot(y(x)'+x(y)') \\
&=\cos(xy)\cdot(y+xy') \\
&=\cos(xy)y+\cos(xy)xy' \\
\frac{-\cos(xy)y}{\cos(xy)x}&=y' \\
-\frac{y}{x}&=\frac{dy}{dx}
\end{align*}
\section*{Higher Derivatives and finding Extrema}
\label{sec:org602b8f2}
\begin{itemize}
\item What stops us from getting the Derivative of a Derivative? As it turns out, nothing, and in fact, we have very good reason for doing so sometimes
\item The Derivative of a Derivative is called a \emph{Second} Derivative, quite simply and it's denoted as \(f^{\prime\prime}(x)\) where a regular Derivative would be denoted as \(f'(x)\)
\item You can go even higher than this, though the applications we will speak about only revolve around the first and second Derivatives
\item In Physics, a really big application of Derivatives is when you have plotted the Displacement of an object as a Function of elapsed time
\begin{itemize}
\item This will mean that time is along the x-axis and displacement is along the y-axis
\end{itemize}
\item For such a situation, the first Derivative of the graph will give you a graph of the \emph{Velocity} of the object
\begin{itemize}
\item This is because, since time is along the x-axis, and displacement is along the y-axis, the first Derivative gives you a graph of \(\frac{\Delta\vec{d}}{\Delta t}\) which is what Velocity is defined as
\end{itemize}
\item As well, the \emph{second} Derivative of this graph will give you a graph of the \emph{Acceleration} of the object
\begin{itemize}
\item That's because this will give you a graph of \(\frac{\frac{\Delta\vec{d}}{\Delta t}}{\Delta t}\), or more simply, \(\frac{\Delta\vec{d}}{\Delta t^{2}}\), which is what Acceleration is defined as
\end{itemize}
\item More generally, however, the First Derivative of a Function will tell you where the \textbf{Extremas} of the Function are (the local highest and lowest points of the Function)
\item And then, to gain further context, the Second Derivative can tell you whether these Extremas are Maximas or Minimas (high or low points)
\end{itemize}
\subsection*{Example 1}
\label{sec:orgcd4fe6a}
\[f(x)=x^{3}-3x^{2}+1\]

\begin{itemize}
\item Here, we're going to try and find where the Extremas of the Function are by using the First Derivative, and then find if they are Minima or Maxima points using the Second Derivative
\item Let's begin by finding the First Derivative; once we do, we are going to solve for it's x-intercepts
\end{itemize}

\begin{align*}
f'(x)&=3x^{2}-6x \\
&=3x(x-2) \\
f'(0)=0 &\land f'(2)=0
\end{align*}

\begin{itemize}
\item So, what do these x-intercepts tell us?
\item These are points along the Derivative where we get a value of 0
\item Given that the Derivative describes the \emph{rate of change} of the original Function, that means that the x-intercepts are where the vertical direction the Function is going changes
\item That also means that along the roots of the first Derivative are where we can find the Extremas of the Function, but we do not know if these Extremas are Minimas or Maximas so let's get the Second Derivative
\item Once we get the second Derivative, we simply see what output value it gives at the input values of the Extremas in the original Function
\item If the output it gives is \emph{positive} then that means the Function is increasing at that point and we have found a Minima
\item If the output it gives is \emph{negative} however, then that means the Function is decreasing at that point and we have found a Maxima
\end{itemize}

\begin{align*}
f^{\prime\prime}(x)&=\left(3x^{2}-6x\right)' \\
&=6x-6 \\
f^{\prime\prime}(0)&=-6 \\
f^{\prime\prime}(2)&=6
\end{align*}

\begin{itemize}
\item And so we know that at \(f(0)\) we have a Maxima and at \(f(2)\) we have a Minima
\end{itemize}
\section*{l'Hopital's Rule}
\label{sec:org69a097b}
\begin{itemize}
\item When we want to get the Limit of something indeterminate such as \(\lim_{x\to0}\frac{\sin(x)}{x}\), l'Hopital's Rule says we can simply get the limit of the derivatives of the numerator and denominator to evaluate this
\item To more so formalize this Law, in the expression \(\lim_{x\to a}\frac{f(x)}{g(x)}\), if both Functions are differentiable and approach some indeterminate form as \(x\) approaches \(a\), then \(\lim_{x\to a}\frac{f(x)}{g(x)}=\lim_{x\to a}\frac{f'(x)}{g'(x)}\)
\item l'Hopital's Rule also works for the product of two Functions which become Indeterminate with our Limit, though it takes on the form \(\lim_{x\to a}f(x)\cdot g(x)=\lim_{x\to a}\frac{f'(x)}{g(x)^{-1\prime}}=\lim_{x\to a}\frac{g'(x)}{f(x)^{-1\prime}}\)
\end{itemize}
\subsection*{Example 1}
\label{sec:org7dbc4b7}
\[\lim_{x\to0}\frac{\sin(x)}{x}\]

\begin{itemize}
\item Here, unfortunately, if we try to evaluate this as it is, we will simply end up with \(\frac{0}{0}\) which is indeterminable
\item However, l'Hopital's Rule can tell us a bit more about what this Function is doing as \(x\) approaches 0:
\end{itemize}

\begin{align*}
\lim_{x\to0}\frac{\sin(x)}{x}&=\lim_{x\to0}\frac{\sin'(x)}{x'} \\
&=\lim_{x\to0}\frac{\cos(x)}{1} \\
&=1
\end{align*}
\subsection*{Example 2}
\label{sec:org5380a25}
\[\lim_{x\to0^{+}}x\cdot\ln(x)\]

\begin{itemize}
\item Here, \(x\) will become 0 and \(\ln(x)\) will approach \(-\infty\), meaning we can use the product version of the rule:
\end{itemize}

\begin{align*}
\lim_{x\to0^{+}}x\cdot\ln(x)&=\lim_{x\to0^{+}}\frac{\ln'(x)}{x^{-1\prime}} \\
&=\lim_{x\to0^{+}}\frac{\frac{1}{x}}{-x^{-2}} \\
&=\lim_{x\to0^{+}}\frac{-x^{2}}{x} \\
&=\lim_{x\to0^{+}}-x \\
&=0
\end{align*}
\section*{Taylor Series}
\label{sec:org6b76225}
\begin{itemize}
\item Oftentimes, transcendental functions such as the trigonometric functions and \(e^{x}\) can be rather unwieldy; they're no problem with a calculator, but, well, how do calculators calculate the outputs to these functions?
\item It turns out, differentiation gives us a way to express these transcendental functions as \emph{approximations} of polynomials
\item The form of these polynomials is that of \(c_{0}x^{0}+c_{1}x^{1}+c_{2}x^{2}+\ldots\) where we have a choice for these \(c\) numbers
\end{itemize}
\subsection*{Sine}
\label{sec:org469ebff}
\begin{itemize}
\item The term \(c_{0}x^{0}\) is simply going to determine the y-intercept for the function, which in the case of \(\sin(x)\) is going to be 0, and so \(c_{0}=0\)
\item When taking the first order derivative of this polynomial, the term \(c_{1}x^{1}\) is going to simply become \(1\cdot c_{1}\), which means it will determine the y-intercept of the first order derivative
\begin{itemize}
\item \(\frac{d}{dx}\sin(x)=\cos(x)\) and so \(\sin'(0)=1\) meaning that we can simply make \(c_{1}=1\) so that the first order derivative of our polynomial matches that of Sine
\end{itemize}
\item Now, \(c_{2}x^{2}\) is simply going to become \(c_{2}\) once we take the second order derivative, and all lower degree terms will cancel out by that point, so this term determines the y-intercept of the second order derivative
\begin{itemize}
\item \(\sin^{\prime\prime}(x)=-\sin(x)\) which will be 0 at input 0, so we can make \(c_{2}=0\) to make our function's second derivative correspond to that of Sine
\end{itemize}
\item \(\sin^{\prime\prime\prime}(x)=-\cos(x)\) which will output -1 at input 0
\begin{itemize}
\item Remember that the power rule means that the term \(c_{3}x^{3}\) is going to become \(1\cdot2\cdot3\cdot c_{3} \lor 3!\cdot c_{3}\) after the third order derivative of the function is taken
\item This means that in order to make this term become -1 after the third order derivative, we must make it so \(c_{3}=\frac{-1}{3!}\)
\end{itemize}
\item \(\sin^{\prime\prime\prime\prime}(x)=\sin(x)\) which will output 0 at input 0, so we can make \(c_{4}=0\)
\item \(\sin^{\prime\prime\prime\prime\prime}(x)=\cos(x)\) which outputs 1 at input 0
\begin{itemize}
\item Remember that at the fifth order derivative, the fifth term becomes \(5\cdot4\cdot3\cdot2\cdot1\cdot c_{5} \lor 5!\cdot c_{5}\), so to make this term become 1 like Sine does, we make it so that \(c_{5}=\frac{1}{5!}\)
\end{itemize}
\item Now, in total, we have \(\frac{x^{1}}{1!}-\frac{x^{3}}{3!}+\frac{x^{5}}{5!}-\frac{x^{7}}{7!}+\ldots\) which continues on like this forever because the derivatives of Sine are periodic
\item This means we can make a Taylor Series for Sine that takes the form of \(\sum_{n=1}^{\infty}\frac{x^{2n-1}}{(-1)^{n+1}(2n-1)!}\)
\end{itemize}
\subsection*{Cosine}
\label{sec:orgd3bea55}
\begin{itemize}
\item The term \(c_{0}x^{0}\) will determine the y-intercept of the function, which for \(\cos(x)\) will be 1, so let's make it so \(c_{0}=1\)
\item The term \(c_{1}x^{1}\) determines the y-intercept of the first derivative, which in the case of \(\cos(x)\) is \(-\sin(0)=0\), so let's just make \(c_{1}=0\)
\item The term \(c_{2}x^{2}\) determines the y-intercept of the second order derivative, which in the case of \(\cos(x)\) is \(-\cos(0)=-1\), so since this term becomes \(2!\cdot c_{2}\) after the derivative, let's make \(c_{2}=\frac{-1}{2!}\)
\item The term \(c_{3}x^{3}\) determines the y-intercept of the third order derivative, which in the case of \(\cos(x)\) is \(\sin(0)=0\), so let's just make \(c_{3}=0\)
\item The term \(c_{4}x^{4}\) determines the y-intercept of the fourth order derivative, which in the case of \(\cos(x)\) is \(\cos(0)=1\), so since the term becomes \(4!\cdot c_{4}\) after the derivative, let's make \(c_{4}=\frac{1}{4!}\)
\item Now, in total, we have \(\frac{x^{0}}{0!}-\frac{x^{2}}{2!}+\frac{x^{4}}{4!}-\frac{x^{6}}{6!}+\ldots\) which continues on like this forever because the derivatives of Cosine are periodic
\item This means we can make a Taylor Series for Cosine that takes the form of \(\sum_{n=0}^{\infty}\frac{x^{2n}}{(-1)^{n}(2n)!}\)
\end{itemize}
\subsection*{Graphs of Sine and Cosine's Taylor Series'}
\label{sec:orge1e0d33}
\begin{tikzpicture}
\begin{axis}[
    axis lines=middle,
    xlabel=\(\theta\),
    ylabel=\(y\),
    ymin=-1.5,
    ymax=1.5,
    xmin=-2*pi,
    xmax=2*pi,
    xtick={-2*pi,-3*pi/2,-pi,-pi/2,0,pi/2,pi,3*pi/2,2*pi},
    xticklabels={\(-2\pi\),\(\frac{-3\pi}{2}\),\(-\pi\),\(-\frac{\pi}{2}\),\(0\),\(\frac{\pi}{2}\),\(\pi\),\(\frac{3\pi}{2}\),\(2\pi\)},
    xticklabel style={anchor=north east},
    xmajorgrids=true,
    grid style=dashed
]
\addplot[
    samples=300,
    color=red,
    domain=-2*pi:2*pi
]
{x-(x^3/6)+(x^5/120)-(x^7/5040)+(x^9/362880)};

\addplot[
    samples=300,
    color=blue,
    domain=-2*pi:2*pi
]
{1-(x^2/2)+(x^4/24)-(x^6/720)+(x^8/40320)-(x^10/3628800)};
\end{axis}
\end{tikzpicture}

\begin{itemize}
\item Here is a graph of the Taylor Series' of Sine and Cosine both with an upper limit of 5
\item I just think that they look beautiful
\end{itemize}
\subsection*{The Natural Exponential}
\label{sec:org8d9132b}
\begin{itemize}
\item \(e^{x}\) is of course a much more interesting function because it is it's own derivative, meaning that all of it's higher order derivatives are also just \(e^{x}\)
\item That means that all of it's derivative's y-intercepts are also going to just be 1, so we simply need to make it so all of the coefficients of our terms go to 1 after differentiation occurs
\item For a term of degree \(n\), after the \(n\)th derivative, that term is going to look like \(n!\cdot c_{n}\), so we can just make \(c_{n}=\frac{1}{n!}\)
\item This essentially means that the Taylor Series for \(e^{x}\) is going to look like \(\frac{x^{0}}{0!}+\frac{x^{1}}{1!}+\frac{x^{2}}{2!}+\frac{x^{3}}{3!}+\ldots\)
\item This also means that we can summarize the Taylor Series as \(\sum_{n=0}^{\infty}\frac{x^{n}}{n!} \lor \sum_{n=0}^{\infty}(n!)^{-1}x^{n}\)
\end{itemize}
\subsection*{Infinite Products for Sine and Cosine}
\label{sec:org07c4e57}
\begin{itemize}
\item There are also infinite \emph{products} that can be used to represent Sine and Cosine
\item For Sine, there is \(\sin(x)=x\prod_{n=1}^{\infty}\left(1-\frac{x^{2}}{\pi^{2}n^{2}}\right)\)
\item For Cosine, there is \(\cos(x)=\prod_{n=1}^{\infty}\left(1-\frac{x^{2}}{\pi^{2}\left(n-\frac{1}{2}\right)^{2}}\right)\)
\item You'll notice that these representations of Sine and Cosine require a lot more terms to become accurate over a given range than the Series' do
\item Nonetheless, to summarize, we now have all of the following representations of Sine and Cosine:
\end{itemize}

\begin{align*}
\sin(x)&=\frac{\text{Opposite}}{\text{Hypotenuse}} \\
&=\sum_{n=1}^{\infty}\frac{x^{2n-1}}{(-1)^{n+1}(2n-1)!} \\
&=x\prod_{n=1}^{\infty}\left(1-\frac{x^{2}}{\pi^{2}n^{2}}\right) \\
\cos(x)&=\frac{\text{Adjacent}}{\text{Hypotenuse}} \\
&=\sum_{n=0}^{\infty}\frac{x^{2n}}{(-1)^{n}(2n)!} \\
&=\prod_{n=1}^{\infty}\left(1-\frac{x^{2}}{\pi^{2}\left(n-\frac{1}{2}\right)^{2}}\right)
\end{align*}
\end{document}
