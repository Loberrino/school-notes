%& /home/arad/.config/emacs/.local/cache/org/persist/2e/3dcef2-4c63-475b-b426-cbfa756620c1-7a0d0a73484fd1332eb24a4fe610ddb7
% Created 2025-03-03 Mon 23:17
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage{capt-of}
\usepackage{esdiff}
\usepackage[fixamsmath]{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{longtable}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{modiagram}
\usepackage[version=4]{mhchem}
\usepackage{chemfig}
\usepackage{gensymb}
\usepackage[product-units=bracket-power, sticky-per]{siunitx}
\usepackage{cancel}
\usepackage{fourier}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{quotes,angles,decorations.pathreplacing} \DeclareMathOperator{\atantwo}{atan2} \newcommand*\intd{\mathop{}\!\mathrm{d}} \newcommand*\intD[1]{\mathop{}\!\mathrm{d^#1}} \DeclareMathOperator{\arctantwo}{arctan2} \everymath{\displaystyle}

%% ox-latex features:
%   !announce-start, !guess-pollyglossia, !guess-babel, !guess-inputenc, maths,
%   !announce-end.

\usepackage{amsmath}
\usepackage{amssymb}

%% end ox-latex features


% end precompiled preamble
\ifcsname endofdump\endcsname\endofdump\fi

\author{Arad Fathalian}
\date{\today}
\title{Parametric Equations, Polar Coordinates, and Vector-Valued Functions}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Parametric Equations, Polar Coordinates, and Vector-Valued Functions},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8-pre)},
 pdflang={English}}
\begin{document}

\maketitle
\section*{Parametric Equations}
\label{sec:org512f38f}
\begin{itemize}
\item Parametric Equations are a set of equations wherein two variables, \(x\) and \(y\), are defined in terms of a third variable, usually \(t\)
\begin{itemize}
\item The equations are typically of the form \(x=x(t)\) and \(y=y(t)\); \(t\) is called the \emph{paramter} of the equations
\end{itemize}
\item Both equations are said to be defined over the same domain \(I\)
\item The plane over which the points \(\left(x(t),y(t)\right)\) are defined is called the \emph{Plane Curve}
\item The way that these equations are graphed is usually by taking select values of \(t\) and finding what the corresponding \(x\) and \(y\) values are
\begin{itemize}
\item This is perhaps most efficiently done in a table
\end{itemize}
\item For instance, here is a table of select parameter values for the equations \(x(t)=t^{2}\) and \(y(t)=t^{3}-2t\):
\end{itemize}

\begin{center}
\begin{tabular}{rrr}
Value of \(t\) & Value of \(x\) & Value of \(y\)\\
\hline
-3 & 9 & -21\\
-2 & 4 & -4\\
-1 & 1 & 1\\
0 & 0 & 0\\
1 & 1 & -1\\
2 & 4 & 4\\
3 & 9 & 21\\
\hline
\end{tabular}
\end{center}

\begin{itemize}
\item To graph this, just take the values of \(x\) and \(y\) and plot them as points \((x,y)\)
\item Parametric Equations are usually interpreted as the paramter being a representation of time
\begin{itemize}
\item So, think of \(x\) and \(y\) as being coordinates along a 2D plane and \(t\) represents the time at which an object is at the corresponding coordinates \(\left(x(t),y(t)\right)\)
\end{itemize}
\item For simpler Parametric Equations, they can be converted into relations between \(x\) and \(y\) by substituting \(t\) out
\item For instance, for the Parametric Equations \(x=t^{2}\) and \(y=t^{3}-2t\) from above, we can isolate \(t\) in \(x=t^{2}\) as \(t=\pm\sqrt{x}\)
\begin{itemize}
\item Then we can substitute this into the equation for \(y\) to get \(y=\left(\pm\sqrt{x}\right)^{3}-2\left(\pm\sqrt{x}\right)\)
\end{itemize}
\item This is \emph{not} a function, but it \emph{is} a relation between \(y\) and \(x\) like the ones we've been graphing for years
\item Now suppose some Plane Curve defined by \(x(t)\) and \(y(t)\) with the interval \(a\leq t\leq b\) in the domain \(I\)
\item If each Parametric Function is continuous in \([a,b]\), and it is differentiable in \((a,b)\), with continuous \(\diff{y}{t}\) and \(\diff{x}{t}\) that are never both 0, this is a \textbf{smooth curve}
\begin{itemize}
\item So essentially, \(x(t)\) and \(y(t)\) must both be continuous in \([a,b]\) and differentiable in \((a,b)\), and across \((a,b)\), \(\diff{y}{x}\neq\frac{0}{0}\)
\end{itemize}
\end{itemize}
\subsection*{Differentiating Parametric Equations}
\label{sec:orgfaae5de}
\begin{itemize}
\item We can go through the effort of converting parametric equations into a typical relation in order to differentiate, but there is a better way
\item We have functions of \(t\) defining \(y\) and \(x\) separately, so we can find \(\diff{y}{t}\) and \(\diff{x}{t}\) separately
\item Notice what happens when we do \(\diff{y}{t}\div\diff{x}{t}\); this is equivalent to \(\diff{y}{t}\cdot\diff{t}{x}\) which becomes \(\diff{y}{x}\); the derivative we're looking for
\item So indeed, we differentiate \(y(t)\) and \(x(t)\) and then do \(\frac{y'(t)}{x'(t)}\) to find the derivative of the Parametric Equation
\begin{itemize}
\item Note that this only works if \(\diff{x}{t}\) or \(x'(t)\) is \emph{not} equal to 0 since it's in the denominator
\end{itemize}
\end{itemize}
\subsubsection*{Example 1}
\label{sec:org3e9c48d}
Differentiate the Parametric Equations \(x(t)=\cos(t)\) and \(y(t)=\ln(t)\)

\begin{itemize}
\item So then, to do this, we can just do \(\frac{y'(t)}{x'(t)}=\frac{t^{-1}}{-\sin(t)}=-\csc(t)t^{-1}\)
\end{itemize}
\subsubsection*{Example 2}
\label{sec:org1dac0da}
Consider the Plane Curve defined by the equations \(x(t)=t^{3}-12t+8\) and \(y(t)=t^{2}-4t+5\) with the domain \(t\in[-10,10]\).
Where can we find vertical and horizontal tangent lines?

\begin{itemize}
\item Your first instinct may be to find the derivative of this Plane Curve by doing \(\frac{y'(t)}{x'(t)}\) as we did before
\begin{itemize}
\item This produce \(\diff{y}{x}=\frac{2t-4}{3t^{2}-12}\); horizontal tangent lines occur when \(y'(t)=0\) and vertical tangent lines occur when \(x'(t)=0\) then
\end{itemize}
\item We can quite clearly see that \(y'(t)=0\) when \(t=2\) and \(x'(t)=0\) when \(t=\pm2\)
\begin{itemize}
\item Wait now we have a problem\ldots{} How can we have a vertical \emph{and} horizontal tangent line at the same time?
\end{itemize}
\item Turns out at \(t=2\), which is the intersection of the roots of \(y'(t)\) and \(x'(t)\), we actually have \emph{neither} a horizontal or a vertical tangent line
\item Since \(t=2\) is a root of the numerator and denominator of the derivative, the derivative becomes indeterminate form here
\item So, we re-phrase the derivative; \(\frac{2t-4}{3t^{2}-12}=\frac{2(t-2)}{3(t-2)(t+2)}=\frac{2}{3(t+2)}\)
\begin{itemize}
\item So as we can see then, the derivative of this plane curve \emph{actually} does not have any roots, but it has one vertical asymptote
\end{itemize}
\item We can interpret this as meaning there are no points at which this plane curve has horizontal tangent lines, but at \(t=-2\) there is a vertical one
\end{itemize}
\subsection*{Second Order Derivatives of Parametric Equations}
\label{sec:orge37c7dd}
\begin{itemize}
\item There is this notation used for standard second order derivatives which looks like \(\diff[2]{y}{x}\)
\begin{itemize}
\item This comes from \(\diff{\left(\diff{y}{x}\right)}{x}\) essentially; the \(\intd\)'s along the top collect together to make \(\intD{2}\), and along the bottom is \(\intd x\cdot\intd x\) expressed as \(\intd x^{2}\)
\end{itemize}
\item We use this notation to determine how we should deal with second order derivatives of parametric equations
\begin{itemize}
\item When we get the first order derivative of a parametric equation, it's with respect to \(t\) so we cannot differentiate it normally
\end{itemize}
\item Instead, we do \(\diff{\left(\diff{y}{x}\right)}{t}\div\diff{x}{t}\), which becomes \(\diff{\left(\diff{y}{x}\right)}{t}\cdot\diff{t}{x}\), or \(\diff[2]{y}{x}\)
\begin{itemize}
\item So essentially, we differentiate \(\diff{y}{x}\) with respect to \(t\), and then divide that by \(\diff{x}{t}\)
\end{itemize}
\end{itemize}
\subsection*{Example 1}
\label{sec:orgc09bb57}
Find the second order derivative of the Plane Curve defined by \(x(t)=3t^{2}\) and \(y(t)=2t^{4}-5\)

\begin{itemize}
\item Firstly, we get the first order derivative \(\diff{y}{x}\) by doing \(\frac{y'(t)}{x'(t)}\) which gets us \(\frac{8t^{3}}{6t}=\frac{4}{3}t^{2}\)
\item Then, we can differentiate this with respect to \(t\) again and divide \emph{again} by \(x'(t)\) to get \(\frac{\frac{8}{3}t}{6t}=\frac{8}{18}=\frac{4}{9}\)
\end{itemize}
\section*{Arc-Lengths of Curves Given By Parametric Equations}
\label{sec:org378129c}
\begin{itemize}
\item Suppose we had a curve given by the Equations \(x=x(t)\) and \(y=y(t)\), and we wanted to find the length of the curve from the point where \(t=a\) to where \(t=b\)
\item Well one thing we could try is to segment the curve into a bunch of really small straight lines; as the number of these lines increases, the approximation gets better
\item Maybe we can think of this series of straight lines as being composed of a horizontal component \(\intd x\) and a vertical component \(\intd y\)
\begin{itemize}
\item That means the length of each of these line segments \emph{would} be \(\sqrt{\left(\intd x\right)^{2}+\left(\intd y\right)^{2}}\) according to the Pythagorean Theorem
\end{itemize}
\item The problem with this is that our equations are with respect to \(t\) and this model is with respect to \(x\) and \(y\)
\begin{itemize}
\item We know how to find \(\diff{x}{t}\) and \(\diff{y}{t}\) however, so we can think of \(\intd x\) as \(\diff{x}{t}\cdot\intd t\) and \(\intd y\) as \(\diff{y}{t}\cdot\intd t\)
\end{itemize}
\item Thus, what we can describe the length of each of these line segments as is \(\sqrt{\left(\diff{x}{t}\cdot\intd t\right)^{2}+\left(\diff{y}{t}\cdot\intd t\right)^{2}}\)
\item And of course, if we wanna approximate the length of the function's curve, we add up a continuous series of such lines by doing \(\int\sqrt{\left(\diff{x}{t}\cdot\intd t\right)^{2}+\left(\diff{y}{t}\cdot\intd t\right)^{2}}\)
\item Now to make this an Integral we can actually compute, we factor \(\intd t^{2}\) out to get \(\int\sqrt{\left(\intd t\right)^{2}\left(\left(\diff{x}{t}\right)^{2}+\left(\diff{y}{t}\right)^{2}\right)}\)
\item By the way that radicals work, we can now split this root of a product up into a product of roots: \(\int\sqrt{\left(\intd t\right)^{2}}\sqrt{\left(\diff{x}{t}\right)^{2}+\left(\diff{y}{t}\right)^{2}}\)
\item So, we can find the length of an arc given by Parametric Equations \(x=x(t)\) and \(y=y(t)\) from \(t=a\) to \(t=b\) as \(\int_{a}^{b}\sqrt{\left(\diff{x}{t}\right)^{2}+\left(\diff{y}{t}\right)^{2}}\intd t\)
\end{itemize}
\section*{Vector-Valued Functions}
\label{sec:orgb10773c}
\begin{itemize}
\item Vectors are often denoted as \(\vec{r}=x\hat{\imath}+y\hat{\jmath}\) for some Scalar Values \(x\) and \(y\)
\begin{itemize}
\item Here, \(\hat{\imath}\) (read as ``i-hat'') is the Unit Vector in the horizontal direction, and \(\hat{\jmath}\) (read as ``j-hat'') is the Unit Vector in the vertical direction
\end{itemize}
\item They are drawn graphically essentially as arrows/rays emanating from the origin of the Cartesian Plane \(\left(\mathbb{R}^{2}\right)\) to the coordinate \((x,y)\)
\begin{itemize}
\item They are \emph{thought} of as the sum of the unit vectors \(\hat{\imath}\) and \(\hat{\jmath}\) after they have been \emph{scaled} a certain quantity, hence why \(x\) and \(y\) are Scalars
\end{itemize}
\item A Vector \(\vec{r}=x\hat{\imath}+y\hat{\jmath}\) is also often alternatively written as \(\vec{r}=\begin{bmatrix}x\\y\end{bmatrix}\) or \(\vec{r}=\left\langle x,y\right\rangle\)
\item We can have some function \(\vec{r}(t)\) whose input is a parameter \(t\in\mathbb{R}\) and whose output is a Vector \(\vec{r}\in\mathbb{R}^{2}\) defined as \(\vec{r}(t)=x(t)\hat{\imath}+y(t)\hat{\jmath}\) or \(\vec{r}(t)=\begin{bmatrix}x(t)\\y(t)\end{bmatrix}\)
\item This makes Vector-Valued Functions \emph{very} similar to Plane Curves, just that we're working in terms of Vectors, not points
\item So, for an example, suppose the function \(\vec{r}(t)=\begin{bmatrix}0.5t^{2}+t\\t^{3}-5t\end{bmatrix}\); we then know that \(\vec{r}(2)=\begin{bmatrix}4\\-2\end{bmatrix}\)
\begin{itemize}
\item So, for input 2, the output is \(\begin{bmatrix}4\\-2\end{bmatrix}\), or \(4\hat{\imath}-2\hat{\jmath}\); it is a Vector with a horizontal component of 4 and a vertical component of -2
\end{itemize}
\item You can make a graph of all outputs for a certain interval of the parameter \(t\), mapping the horizontal and vertical components of output Vectors as \((x,y)\)
\begin{itemize}
\item Then you'll just be making the graph of the Plane Curve \(\left(x(t),y(t)\right)\)
\end{itemize}
\item \emph{Limits} of Vector-Valued functions \(\vec{r}(t)=\begin{bmatrix}x(t)\\y(t)\end{bmatrix}\) can be found as \(\lim_{t\to a}\vec{r}(t)=\begin{bmatrix}\lim_{t\to a}x(t)\\\lim_{t\to a}y(t)\end{bmatrix}\), provided the limits of each component exist
\begin{itemize}
\item Note, then, that the limit of a Vector-Valued function is itself a Vector
\end{itemize}
\item Similarly, a Vector-Valued function is \emph{continuous} at some input \(a\) if \(\vec{r}(a)=\lim_{t\to a}\vec{r}(t)\)
\end{itemize}
\subsection*{A Note On the Notation Used Here}
\label{sec:org9fa19d6}
\begin{itemize}
\item It is likely that you are unfamiliar with the usage of \(\mathbb{R}^{2}\) used to denote 2-Dimensional Euclidean Space (I.E. The Cartesian Plane)
\item This comes from an operation that can be performed upon two sets \(A\) and \(B\) called the \emph{Cartesian Product} of \(A\) and \(B\), denoted as \(A\times B\)
\item \(A\times B\) outputs a set composed of ordered pairs \((x,y)\) such that \(x\in A\) and \(y\in B\); in proper form, then, \(A\times B=\left\{(x,y)\mid x\in A\land y\in B\right\}\)
\item So, the Cartesian Product of the Real Numbers with themselves, \(\mathbb{R}\times\mathbb{R}\), denoted also as \(\mathbb{R}^{2}\), gives a set of ordered pairs \((x,y)\) such that \(x\in\mathbb{R}\) and \(y\in\mathbb{R}\)
\item This is referred to as 2-Dimensional Euclidean Space, or the Cartesian Plane! 2D Vectors are said to be elements of \(\mathbb{R}^{2}\)
\end{itemize}
\section*{Differentiating Vector-Valued Functions}
\label{sec:org22c0ef1}
\begin{itemize}
\item Some Vector-Valued Function \(\vec{r}(t)=\begin{bmatrix}x(t)\\y(t)\end{bmatrix}\) can be differentiated as the form \(\vec{r}'(t)=\begin{bmatrix}x'(t)\\y'(t)\end{bmatrix}\) or \(\diff{\vec{r}(t)}{t}=\left\langle\diff{x(t)}{t},\diff{y(t)}{t}\right\rangle\)
\begin{itemize}
\item So, notice, \(\vec{r}:\mathbb{R}\mapsto\mathbb{R}^{2}\), and so too is \(\diff{\vec{r}}{t}:\mathbb{R}\mapsto\mathbb{R}^{2}\); they're both Vector-Valued Functions
\end{itemize}
\item There exist rules when it comes to differentiating Vector-Valued Functions similar to differentiating regular Functions
\item Suppose the Vector-Valued Function \(\vec{r}(t)\) with a specific input \(t=a\) meaning we have \(\vec{r}(a)\)
\begin{itemize}
\item At this point, \(\vec{r}'(a)\) will give another vector which when rooted at the tip of \(\vec{r}(a)\), will show the trajectory of the function's rate of change at \(\vec{r}(a)\)
\end{itemize}
\end{itemize}
\subsection*{Rules For Differentiating Vector-Valued Functions}
\label{sec:org851fcec}
\begin{itemize}
\item Suppose we have functions \(\vec{u}(t),\ \vec{v}(t),\ f(t)\) such that \(\vec{u}:\mathbb{R}\mapsto\mathbb{R}^{2},\ \vec{v}:\mathbb{R}\mapsto\mathbb{R}^{2},\ f:\mathbb{R}\mapsto\mathbb{R}\), and \(k\) is just some scalar value
\item If we have a Vector-Valued Function multiplied by a Scalar, then \(\diff{k\vec{u}(t)}{t}=k\diff{\vec{u}(t)}{t}\)
\item Similarly, \(f(t)\) can be thought of as what's called a \emph{Scalar-Valued} Function; it takes a Scalar value as an input, and it outputs a Scalar value for a given \(t\)
\begin{itemize}
\item Hence, the same rule can be applied, but since \(f(t)\) is still a function with respect to \(t\), we must differentiate it too
\end{itemize}
\item So, we can just use the product rule for something like \(\diff{f(t)\vec{u}(t)}{t}\) to get that \(\diff{f(t)\vec{u}(t)}{t}=f'(t)\vec{u}(t)+\vec{u}'(t)f(t)\)
\item As well, the derivative of a sum or difference of Vector-Valued Functions is exactly what you'd think: \(\diff{\vec{u}(t)\pm\vec{v}(t)}{t}=\vec{u}'(t)\pm\vec{v}'(t)\)
\item Finally, the product rule applies in exactly the same way with the derivative of a product of Vector-Valued Functions: \(\diff{\vec{u}(t)\cdot\vec{v}(t)}{t}=\vec{u}'(t)\vec{v}(t)+\vec{v}'(t)\vec{u}(t)\)
\begin{itemize}
\item If you knew the dot product of the two Vector-Valued Functions as \(g(t)=\vec{u}(t)\cdot\vec{v}(t)\), then \(\diff{g(t)}{t}=\diff{\vec{u}(t)\cdot\vec{v}(t)}{t}\)
\end{itemize}
\end{itemize}
\section*{Integrating Vector-Valued Functions}
\label{sec:org2916db3}
\begin{itemize}
\item Suppose some Vector-Valued Function \(\vec{r}(t)=\begin{bmatrix}x(t)\\y(t)\end{bmatrix}\); to take its Indefinite Integral, we do \(\int\vec{r}(t)=\left\langle\int x(t)\intd t,\int y(t)\intd t\right\rangle\)
\begin{itemize}
\item This produces \(\int\vec{r}(t)=\left\langle X(t)+C_{1},Y(t)+C_{2}\right\rangle\), written perhaps better as \(\int\vec{r}(t)=X(t)\hat{\imath}+Y(t)\hat{\jmath}+C_{1}\hat{\imath}+C_{2}\hat{\jmath}\)
\end{itemize}
\item The Definite Integral is very similar; \(\int_{a}^{b}\vec{r}(t)=\left\langle\int_{a}^{b}x(t)\intd t,\int_{a}^{b}y(t)\intd t\right\rangle=\left\langle X(b)-X(a),Y(b)-Y(a)\right\rangle\)
\begin{itemize}
\item So, written another way, \(\int_{a}^{b}\vec{r}(t)=\left(\int_{a}^{b}x(t)\intd t\right)\hat{\imath}+\left(\int_{a}^{b}y(t)\intd t\right)\hat{\jmath}\)
\end{itemize}
\end{itemize}
\end{document}
