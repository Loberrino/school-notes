#+title: Rational Functions
#+author: Arad Fathalian
#+startup: latexpreview
#+latex_class: article
#+latex_class_options: [letterpaper]
#+options: num:nil toc:nil
#+latex_header: \usetikzlibrary{quotes,angles,decorations.pathreplacing} \DeclareMathOperator{\atantwo}{atan2} \DeclareMathOperator{\arctantwo}{arctan2} \everymath{\displaystyle}

* Reciprocals of Linear Functions
+ For some degree 1 Function \(f(x)\), the reciprocal will be \(f(x)^{-1}\); not to be confused with \(f^{-1}(x)\)
+ When \(f(x)>0\), \(f(x)^{-1}>0\) as well, and when \(f(x)<0\), so will \(f(x)^{-1}<0\)
+ If \(\lim_{x\to\infty}f(x)=\infty\), then \(f(x)^{-1}\) will be a decreasing Function always, and if \(\lim_{x\to\infty}f(x)=-\infty\), then \(f(x)^{-1}\) will be an increasing Function
  + For some number \(n\) such that \(f(n)=0\), \(\lim_{x\to n^{+}}f(x)^{-1}=\infty\) and \(\lim_{x\to n^{-}}f(x)^{-1}=-\infty\)
  + Negative Linear Functions are the reverse: \(\lim_{x\to n^{+}}-f(x)^{-1}=-\infty\) and \(\lim_{x\to n^{-}}-f(x)^{-1}=\infty\)
+ Since for \(f(x)=0\), \(f(x)^{-1}=\frac{1}{0}\), there is a vertical asymptote at the root of the original linear Function
+ As well, linear reciprocals will always have a horizontal asymptote at \(y=0\); \(\lim_{x\to\pm\infty}f(x)^{-1}=0\)
+ The domain of a Linear reciprocal Function will be \(\{x\in\mathbb{R}\mid x\neq n\}\) and the range will be \(\{y\in\mathbb{R}\mid y\neq0\}\)
+ Of course, for every point \(\left(x,f(x)\right)\) in the original Function, it will be \(\left(x,\frac{1}{f(x)}\right)\)
+ The points where \(f(x)=\pm1\) will be /invariant points/ since \(f(x)^{-1}=\pm1\) as well (meaning \(y=\pm1\)) are points shared by \(f(x)\) and \(f(x)^{-1}\)

* Reciprocals of Quadratic Functions
+ There are three general cases of what the Reciprocal of a Quadratic Function may look like
+ You can utilize the fact that \(y=\pm1\) are invariant points and that the x-intercepts of the original Function are now vertical asymptotes to know how to graph it
+ If your Quadratic Function has exactly one x-intercept, the graph will look something like:

\begin{tikzpicture}
\begin{axis}[
    grid=major,
    axis lines=middle,
    ylabel=\(y\),
    xlabel=\(x\),
    ymin=-5,
    ymax=5,
    xmin=-10,
    xmax=10,
    xtick distance=2,
    xticklabel style={anchor=north east}
]
\addplot[
    domain=-100:100,
    restrict y to domain=-500:500,
    samples=5000,
    color=blue
]
{x^-2};
\end{axis}
\end{tikzpicture}

+ This is the graph of \(x^{-2}\), and as you can see, it's exclusively positive, which makes sense, since the output of the denominator can never be negative this way
+ If your Quadratic Function has two x-intercepts, the graph will look something like:

\begin{tikzpicture}
\begin{axis}[
    grid=major,
    axis lines=middle,
    ylabel=\(y\),
    xlabel=\(x\),
    ymin=-5,
    ymax=5,
    xmin=-10,
    xmax=10,
    xtick distance=2,
    xticklabel style={anchor=north east}
]
\addplot[
    domain=-100:100,
    restrict y to domain=-500:500,
    samples=5000,
    color=blue
]
{(x^2-1)^-1};
\end{axis}
\end{tikzpicture}

+ (Ignore the lines at where the vertical asymptotes are supposed to be)
+ The original Function is decreasing and then increasing going from left to right
+ The part connecting the two positive vertical asymptotes which is below the x-axis is where the original function is negative, and where the vertex is
+ If your Quadratic Function has no x-intercepts, the graph will look something like:

\begin{tikzpicture}
\begin{axis}[
    grid=major,
    axis lines=middle,
    ylabel=\(y\),
    xlabel=\(x\),
    ymin=-5,
    ymax=5,
    xmin=-10,
    xmax=10,
    xtick distance=2,
    xticklabel style={anchor=north east}
]
\addplot[
    domain=-100:100,
    restrict y to domain=-500:500,
    samples=5000,
    color=blue
]
{(x^2+1)^-1};
\end{axis}
\end{tikzpicture}

+ Here, there are no vertical asymptotes, because the original function has no zeroes, and instead there's just a bump where the vertex is

* Rational Functions of Like Degree Numerators and Denominators
+ Say we have the function \(f(x)=\frac{-x+3}{2x-5}\); we don't know that the horizontal asymptote is at \(y=0\) any longer, so how do we find it?
+ We can try multiplying it by \(\frac{x^{-1}}{x^{-1}}\) and see what we get:

\begin{align*}
f(x)&=\frac{-x+3}{2x-5}\cdot\frac{x^{-1}}{x^{-1}} \\
&=\frac{-1+3x^{-1}}{2-5x^{-1}}
\end{align*}

+ Now, the horizontal asymptote is what the end behaviour of the function is, so let's find the end behaviour of the function in this form
+ As it turns out, now, \(\lim_{x\to\pm\infty}f(x)=-\frac{1}{2}\)
+ We can generally say for some function \(f(x)=\frac{a_{n}x^{n}+a_{n-1}x^{n-1}+\ldots}{b_{n}x^{n}+b_{n-1}x^{n-1}+\ldots}\), \(\lim_{x\to\pm\infty}f(x)=\frac{a_{n}}{b_{n}}\)
+ We can also find the roots of the function by finding the roots of the numerator and the vertical asymptotes are the roots of the denominator

* Finding the Behaviours of Horizontal and Oblique Asymptotes
+ Generally for some Function \(f(x)=\frac{g(x)}{h(x)}\), we can determine the position and type of non-vertical Asymptote present based on the degrees of \(g(x)\) and \(h(x)\)
+ If \(h(x)\) has a higher degree than \(g(x)\), the Function will have a Horizontal Asymptote which will always be at \(y=0\)
+ Horizontal and Oblique Asymptotes /can/ be crossed unlike Vertical Asymptotes - these Asymptotes are simply a way to define the end behaviour of the Function

** Same Degree
+ If \(g(x)\) and \(h(x)\) are the exact same degree, such as with \(\frac{-x+3}{2x-5}\), the Function will have a Horizontal Asymptote still
  + The Horizontal Asymptote in this case can be found as \(y=\frac{a}{c}\), where \(a\) is the leading coefficient of \(g(x)\) and \(c\) is the leading coefficient of \(h(x)\)
+ The Horizontal Asymptote of a Rational Function is what its End Behaviours are, so we can find it by evaluating \(\lim_{x\to\pm\infty}f(x)\)
+ However, as it's currently written, a Function like \(\frac{-x+3}{2x-5}\) doesn't tell us much about its end behaviour - \(\lim_{x\to\pm\infty}\frac{-x+3}{2x-5}=-\infty\)
+ So we can try multiplying it by \(\frac{x^{-1}}{x^{-1}}\) - \(\frac{-x+3}{2x-5}\cdot\frac{x^{-1}}{x^{-1}}=\frac{-1+\frac{3}{x}}{2-\frac{5}{x}}\)
+ Now, written like this, we can see that \(\lim_{x\to\pm\infty}\frac{-1+\frac{3}{x}}{2-\frac{5}{x}}=\frac{-1}{2}\), or, again, \(\frac{a}{c}\)

** Higher Degree Numerator
+ If \(g(x)\) has a higher degree than \(h(x)\), and with the assumption that the degree of \(h(x)\) is non-zero, the Function will not have a Horizontal Asymptote
  + It will instead have an /Oblique/ or Diagonal Asymptote whose equation can be found simply by evaluating the division
+ For some Function of this type such as \(\frac{(x-3)(x-1)}{x+2}\), for instance, which can be expanded into \(\frac{x^{2}-4x+3}{x+2}\):

\[\begin{array}{c|rrr}
-2 & 1 & -4 & 3 \\
   &   & -2 & 12 \\
\hline
\div1 & 1 & -6 & 15
\end{array}\]

+ Meaning that in this case, the Function is also able to be written as \(x-6+\frac{15}{x+2}\)
+ Written like this, the end behaviour makes the remainder term /negligibly/ small and so the Oblique Asymptote ends up being \(y=x-6\)

* Holes
+ We've already covered the most common type of discontinuity found in Rational Functions, being Vertical Asymptotes, but there is another
+ For a function such as \(\frac{(x-3)(x-2)}{(x)(x-2)}\), we can simplify towards \(\frac{x-3}{x}\), meaning we know these Functions are behaviourally identical
  + The problem is that our function is not defined at \(x=2\) as we can tell from the original form, but this simplified form does not imply that in any way
+ So, we instead write this as \(\frac{x-3}{x}\ \{x\in\mathbb{R}\mid x\neq0,2\}\), and at \(x=2\), there is no vertical asymptote, but rather there is just a "hole"
  + What is meant by a "hole" is that indeed, \(\lim_{x\to2^{\pm}}\frac{(x-3)(x-2)}{(x)(x-2)}=\frac{2-3}{2}\), but the function is not actually /defined/ at \(x=2\)
+ Graphically, at \(x=2\), we show how the Function behaves around that point as normal, but at \(x=2\) itself, we place a hollow point

* The General Process of Graphing Rational Functions
+ A way to graph Rational Functions is to identify 4 traits about them firstly - x-intercepts, y-intercept, horizontal/oblique asymptote, and vertical asymptotes
+ After we've done this, the only big question mark that remains is how the Function behaves relative to the horizontal/oblique asymptote
  + Does the Function ever cross it? Where is it above and below it? If it crosses, at what point does it cross?
+ To answer these questions, we simply set the Function to be greater than or equal to this asymptote and we find the intervals that are solutions to it

** Example 1
\[f(x)=\frac{x^{2}+x-12}{x+2}\]

+ First thing we should do is factor the numerator to get \(f(x)=\frac{(x+4)(x-3)}{x+2}\), and since the numerator is a higher degree, we know we have an Oblique Asymptote
+ Nonetheless, we can now see that the Roots of this Function are at \(x=-4\) and \(x=3\), and there exists a Vertical Asymptote at \(x=-2\)
+ We can also find the y-intercept by finding \(f(0)\): \(\frac{(0+4)(0-3)}{0+2}=-6\)
+ Now, we can find the Oblique Asymptote by doing the division:

\[\begin{array}{c|rrr}
-2 & 1 & 1 & -12 \\
   &   & -2& 2 \\
\hline
\div1 & 1 & -1& -10
\end{array}\]

+ So, \(f(x)=x-1-\frac{10}{x+2}\), meaning that \(\lim_{x\to\pm\infty}f(x)=x-1\), and so the Oblique Asymptote is at \(y=x-1\)

+ But, again, we don't know if the Function ever crosses this Asymptote and we don't know where the Function is above and below it

+ So, to find that, we create an inequality between the original Function and the Asymptote: \(\frac{x^{2}+x-12}{x+2}\geq x-1\):

\begin{align*}
\frac{x^{2}+x-12}{x+2}&\geq x-1 \\
\frac{x^{2}+x-12}{x+2}-x+1&\geq0 \\
\frac{x^{2}+x-12+(-x+1)(x+2)}{x+2}&\geq0 \\
\frac{x^{2}+x-12-x^{2}-x+2}{x+2}&\geq0 \\
\frac{-10}{x+2}&\geq0
\end{align*}

+ This inequality doesn't have any roots, meaning that the Function never /crosses/ the Asymptote
+ Now lets segment the original Function by roots and vertical asymptotes and see which intervals are solutions to this Inequality:

| Factors                | \((-\infty,-4)\) | \((-4,-2)\) | \((-2,3)\) | \((3,\infty)\) |
|------------------------+-------------+-------------+------------+-----------|
| \(-10\)                | -           | -           | -          | -         |
| \(x+2\)                | -           | -           | +          | +         |
| Product of the Factors | +           | +           | -          | -         |
|------------------------+-------------+-------------+------------+-----------|

+ So therefore, the Function is above the Oblique Asymptote at intervals \(x\in(-\infty,-4)\cup(-4,-2)\)
+ And now, with that in mind, we essentially have everything we need to create at least a rough sketch of the Function!
