% Created 2024-01-25 Thu 21:36
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{gensymb}
\usepackage{fourier}
\usepackage{tikz}
\usetikzlibrary{quotes,angles}
\author{Arad Fathalian}
\date{\today}
\title{Trigonometry}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Trigonometry},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.2 (Org mode 9.7)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Grade 9 Review}
\label{sec:org633e4ec}
\subsection*{Angle Relationships}
\label{sec:orgd6c825b}
\subsubsection*{Complementary Angles}
\label{sec:orgdcca80e}
\begin{itemize}
\item Instances in which you have a set of angles which add to \(90\degree\) are called \emph{Complementary} Angles
\item When you have a part of a right angle, for instance say that you know it is \(30\degree\), then you would know that the other part of it \emph{has} to be \(60\degree\)
\end{itemize}

\begin{tikzpicture}
\draw[thick] (0,0) coordinate (A)
    -- (2,0) coordinate (B)
    -- (2,2) coordinate (C);
\draw (B) -- (0,2) coordinate (D)
    pic["\(40\degree\)", draw, ->, angle eccentricity=1.2, angle radius=10mm]
    {angle=D--B--A}
    pic["\(\theta\)", draw, <-, angle eccentricity=1.2, angle radius=10mm]
    {angle=C--B--D};
\end{tikzpicture}

\begin{itemize}
\item So in this instance, we know that Angle \(\theta\) \emph{must} be \(50\degree\) because \(50+40=90\)
\end{itemize}
\subsubsection*{Supplementary Angles}
\label{sec:orgd31909c}
\begin{itemize}
\item Similarly, every flat surface \emph{will} have a total angle of \(180\degree\)
\item If you have a part that lies on a flat surface which is \(75\degree\) you know that the other side \emph{must} be \(105\degree\)
\end{itemize}

\begin{tikzpicture}
\draw (0,0) coordinate (A) -- (1,0) coordinate (B) -- (2,0) coordinate (C);
\draw (B) -- (0,1) coordinate (D);
\draw pic["\(50\degree\)", draw, ->, angle eccentricity=1.5, angle radius=5mm]
    {angle=D--B--A};
\draw pic["\(\theta\)", draw, <-, angle eccentricity=1.5, angle radius=5mm]
    {angle=C--B--D};
\end{tikzpicture}

\begin{itemize}
\item So in this instance, we know that Angle \(\theta\) must be \(130\degree\) because \(50+130=180\)
\end{itemize}
\subsubsection*{The Sum of a Triangle}
\label{sec:orgf613ae6}
\begin{itemize}
\item The 3 Angles of all triangles \textbf{will always add to a total of \(180\degree\)}
\item Say you have \(\triangle XYZ\) and you know that \(\angle XYZ = 65\degree\) and \(\angle XZY = 70\degree\), well, then you know that \(\angle YXZ = 45\degree\)
\end{itemize}
\subsubsection*{Opposing Angles}
\label{sec:org0b48648}
\begin{itemize}
\item If you have an x-shape and know that one angle is \(40\degree\) then you know the one opposite to it should be \(40\degree\) also
\item So, if you have 2 straight lines converging at a point, the angles it forms will be the same as the angles on the opposite sides of the shape formed
\end{itemize}

\begin{tikzpicture}
\draw (0,0) coordinate (A) -- (1,1) coordinate (B) -- (2,0) coordinate (C);
\draw (0,2) coordinate (D) -- (B) -- (2,2) coordinate (E);
\draw pic["\(40\degree\)", draw, <->, angle eccentricity=1.5, angle radius=5mm]
    {angle=A--B--C};
\draw pic["\(\theta\)", draw, <->, angle eccentricity=1.5, angle radius=5mm]
    {angle=E--B--D};
\end{tikzpicture}

\begin{itemize}
\item Here, since these converging lines are perpendicular, we know that the angles they make which are opposite to one another must be equal
\item Therefore, we know that \(\theta=40\degree\)
\end{itemize}
\subsubsection*{Z Pattern (alternate angles)}
\label{sec:org56b7f98}
\begin{itemize}
\item When you have a pair of parallel lines with a line going through it such that it forms a z-shape, the 2 concave angles will be identical
\end{itemize}

\begin{tikzpicture}
\draw (2,0) coordinate (A) -- (0,0) coordinate (B) -- (2,2) coordinate (C) -- (0,2) coordinate (D);
\draw pic["\(30\degree\)", draw, <->, angle eccentricity=1.6, angle radius=5mm]
    {angle=D--C--B};
\draw pic["\(\theta\)", draw, <->, angle eccentricity=1.6, angle radius=5mm]
    {angle=A--B--C};
\end{tikzpicture}

\begin{itemize}
\item The top and bottom lines here are parallel which means the angles that the intersecting line makes are equal on both sides
\item That means that we know \(\theta=30\degree\)
\end{itemize}
\subsubsection*{C Pattern}
\label{sec:org27d539b}
\begin{itemize}
\item If you have a pair of parallel lines with a line perpendicular to both of them, then the two inner angles will be \emph{supplementary} to one another
\end{itemize}

\begin{tikzpicture}
\draw (2,0) coordinate (A) -- (0,0) coordinate (B) -- (0,2) coordinate (C) -- (2,2) coordinate (D);
\end{tikzpicture}

\begin{itemize}
\item This is most evident with this very blocky C wherein both inner angles are \(90\degree\) and are thus supplementary to one another as they both add up to \(180\degree\)
\end{itemize}
\subsection*{Similar Triangles}
\label{sec:org11bd9dc}
\begin{itemize}
\item Similar triangles have the same shape but with different sizes
\item One triangle is a reduction or an enlargement of the other
\item If we have \(\triangle ABC\) and \(\triangle XYZ\), then we know of a few ratios already
\begin{itemize}
\item Corresponding sides must be the same; \(\frac{AB}{XY}=\frac{BC}{YZ}=\frac{AC}{XZ}\)
\item Corresponding angles must be the same as well; \(\angle BAC = \angle YXZ\), \(\angle ABC = \angle XYZ\), and \(\angle BCA = \angle YZX\)
\end{itemize}
\end{itemize}
\subsubsection*{Conditions for Similarity}
\label{sec:orgf9fa8b8}
\begin{itemize}
\item In order to prove that 2 triangles are similar, there are 3 different types of conditions that can and must be met
\item Only one type must be met for 2 triangles to be proven as similar
\end{itemize}
\begin{itemize}
\item Side-Side-Side Similarity (SSS\textasciitilde{})
\label{sec:org7598748}
\begin{itemize}
\item If 3 corresponding sides are proportional, then we have proved that our triangles are similar
\item In other words, for \(\triangle ABC\) and \(\triangle XYZ\), when we can prove \(\frac{AB}{XY}=\frac{BC}{YZ}=\frac{AC}{XZ}\), then we can write \(\therefore \triangle ABC \sim \triangle XYZ (sss\sim)\)
\item This of course however means that all sides of both triangles need to be known
\end{itemize}
\item Side-Angle-Side Similarity (SAS\textasciitilde{})
\label{sec:org8212121}
\begin{itemize}
\item If 2 pairs of corresponding sides are proportional \emph{and} their contained angles are equal, then we have proven that our triangles are similar
\item For \(\triangle ABC\) and \(\triangle XYZ\), when we can prove \(\frac{AB}{XY}=\frac{BC}{YZ}\) \emph{and} \(\angle ABC = \angle XYZ\), then we can write \(\therefore \triangle ABC \sim \triangle XYZ (sas\sim)\)
\item This requires us to know 2 sides and their angles and so will be very rarely used
\end{itemize}
\item Angle-Angle Similarity (AA\textasciitilde{})
\label{sec:org610170c}
\begin{itemize}
\item If 2 pairs of corresponding angles are equal, then we have proven that our triangles are similar
\item For \(\triangle ABC\) and \(\triangle XYZ\), when we can prove \(\angle ABC = \angle XYZ\) and \(\angle BAC = \angle YXZ\) then we can write \(\therefore \triangle ABC \sim \triangle XYZ (aa\sim)\)
\end{itemize}
\end{itemize}
\section*{Sine, Cosine, and Tangent}
\label{sec:org9660186}
\subsection*{Right Angle Triangle Review}
\label{sec:org7acc177}
\begin{itemize}
\item Right angle triangles have 3 sides to them which depend based on which angle you're focusing on
\begin{itemize}
\item The \textbf{Hypotenuse} never changes and is always static; it's the side that never touches the right angle of the triangle and is the longest side of the triangle
\item The \textbf{Adjacent} side helps shape the Hypotenuse; it's the side that touches the angle we're measuring alongside the Hypotenuse
\item The \textbf{Opposite} side is, well, on the opposing side of the angle we're measuring; it's the side that touches the right angle which is on the opposite side of the angle of focus
\end{itemize}
\end{itemize}
\subsection*{Similar Right Angle Triangles}
\label{sec:orgf2bc582}
\begin{itemize}
\item If we had a right angle triangle with angle \(x\) of \(30\degree\) then regardless of whatever the measurements are of the 3 sides, there are a few different ratios we will \emph{know} to be true
\begin{itemize}
\item We will know that if we do \(\frac{\text{Opposite}}{\text{Hypotenuse}}\) we \emph{will} get a Ratio of about 1:2 or \(\frac{1}{2}\) or 0.5

\item We will know that if we do \(\frac{\text{Adjacent}}{\text{Hypotenuse}}\) we \emph{will} get a Ratio of about \(\approx0.866\)

\item We will know that if we do \(\frac{\text{Opposite}}{\text{Adjacent}}\) we \emph{will} get a Ratio of about \(\approx5.77\)
\end{itemize}
\end{itemize}
\subsubsection*{Soh-Cah-Toa}
\label{sec:orgc59efad}
You might be asking yourself how we simply know these Ratios; the answer is because they're a set of sort of \textbf{Golden Ratios} for right angle triangles:

\(\frac{\text{Opposite}}{\text{Hypotenuse}}\) is called the \uline{sine} of the angle in question

\(\frac{\text{Adjacent}}{\text{Hypotenuse}}\) is called the \uline{cosine} of the angle in question

\(\frac{\text{Opposite}}{\text{Adjacent}}\) is called the \uline{tangent} of the angle in question

In order to memorize what ratios sine, cosine, and tangent represent, the phrase Soh-Cah-Toa exists; Sine, Cosine, and Tangent are called the \textbf{Primary Trigonometric Ratios}
\subsection*{Working with the Primary Trigonometric Functions}
\label{sec:orge690f32}
\begin{itemize}
\item While we could always calculate the values of Sine, Cosine, and Tangent for our angles ourselves, this is tedious and sometimes impossible since we often know the value of an angle but not the measurements of the sides
\item Because of this, \textbf{all scientific calculators} have these 3 functions already programmed into them
\item This is why this unit and all of Math going forward \textbf{absolutely 100\% needs} a Scientific Calculator

\item We can do \(\sin(\theta)\) \(\cos(\theta)\) and \(\tan(\theta)\) in any calculator (where the Greek letter \(\theta\) pronounced ``theta'' represents our given angle) to get our specified ratio
\item There is also \(\arcsin\), \(\arccos\), and \(\arctan\) (which will often look like \(\sin^{-1}\), \(\cos^{-1}\), and \(\tan^{-1}\)) which will allow you to plug in a ratio in order to get the angles; \(\arcsin\left(\frac{x}{y}\right)=\theta\), \(\arccos\left(\frac{x}{y}\right)=\theta\), and \(\arctan\left(\frac{x}{y}\right)=\theta\)

\item As a small side note, as you just saw, you will often see characters in the Greek alphabet represent unknown angles
\begin{itemize}
\item They work in the exact same way as typical variables like \(x,y,z\), they just look different essentially
\end{itemize}
\item The characters you will see most often used to represent angles are \(\theta,\alpha,\beta\), which are lowercase theta, alpha, and beta respectively
\end{itemize}
\subsection*{The Reciprocals of the Primary Trigonometric Functions}
\label{sec:org3f6227f}
\begin{itemize}
\item 3 more Trigonometric Functions exist as a variation of Sine, Cosine, and Tangent
\item They are called \textbf{Cosecant, Secant, and Cotangent}
\end{itemize}

\begin{align*}
\csc(\theta)=\frac{1}{\sin(\theta)} &\lor \csc(\theta)=\frac{\text{Hypotenuse}}{\text{Opposite}} \\
\sec(\theta)=\frac{1}{\cos(\theta)} &\lor \sec(\theta)=\frac{\text{Hypotenuse}}{\text{Adjacent}} \\
\cot(\theta)=\frac{1}{\tan(\theta)} &\lor \cot(\theta)=\frac{\text{Adjacent}}{\text{Opposite}}
\end{align*}

And so, Cosecant is equivalent to Sine, Secant is equivalent to Cosine, and Cotangent is equivalent to Tangent
\subsection*{A Note on the Inverse Trigonometry Notation}
\label{sec:org85750c3}
\begin{itemize}
\item As mentioned, you will see on your calculator and in many resources that the inverse of, for instance, Sine, is written as \(\sin^{-1}()\)
\item I personally disagree with this Notation style, for two reasons:
\item As will be explored in the Grade 11 Trigonometry Notes, Notation such as \(\sin^{2}()\) is also prevalent and means the same thing as \(\left(\sin()\right)^{2}\)
\begin{itemize}
\item This makes it difficult to tell whether the -1 is supposed to indicate an exponent or an Inverse function
\end{itemize}
\item As well, because of the last reason, one may misread \(\sin^{-1}()\) and think that, based upon exponent rules learned in Grade 11, it is equivalent to \(\frac{1}{\sin()}\), which is then equal to \(\csc()\)
\item For this reason, I much prefer the Notation where Inverse Trigonometric Functions are instead denoted by an ``arc'' prefix
\item While it's 3 extra letters to write out, it completely eliminates this confusion!
\item For this reason, you'll see that across all my notes that instead of something like \(\sin^{-1}()\), I use \(\arcsin()\) instead
\end{itemize}
\section*{Solving with the Primary Trigonometric Functions}
\label{sec:org20f0178}
With our Primary Trigonometric Functions as well as their inverse equivalents, we can find unknown side measurements and unknown angle measurements very easily
\subsection*{Solving for Unknown Sides}
\label{sec:orgdb2c7c3}
\subsubsection*{Example 1}
\label{sec:org09711b0}
A Right Angle Triangle with a Hypotenuse of the length 11.3cm and an angle of \(18.6\degree\) where all other sides and the other angle are unknown. Determine the Opposite side's length.

Okay so the two sides we know the measurements of are the Hypotenuse and Opposite side and Sine is the only ratio that uses both of these so we plug in our values into an equation with the function and the ratio:

\[\sin(18.6)=\frac{x}{11.3}\]

Okay so this is a very easy solve; we just rearrange the 11.3 to isolate x getting us \((11.3)\sin(18.6)=x\) which when evaluated with a calculator yields \(x\approx3.6042cm\) (round to 4th decimal place as good practice)
\subsubsection*{Example 2}
\label{sec:org51f71f0}
A Right Angle Triangle has an Angle of \(65\degree\) and an Adjacent side of length 40m; determine the length of the Opposite side.

The only function out of the three which utilizes both the Adjacent and Opposite sides is Tangent so this time we use that:

\[\tan(65)=\frac{x}{40}\]

Once again this is an easily solve; we simply multiply 40 by tan(65):

\[x\approx85.7803m\]
\subsection*{Solving for Unknown Angles}
\label{sec:org1dd52e8}
\subsubsection*{Example 1}
\label{sec:org6fb8dd1}
A Right Angle Triangle has an Adjacent side of length 4 and a Hypotenuse of length 5; find the angle.

In this case we know the Adjacent and Hypotenuse which means we'd use Cosine so let's plug in what values we have:

\[\cos(\theta)=\frac{4}{5}\]

For cases like this we use the \emph{inverse} of the Functions which essentially just flips the roles of the angle and the ratio in the formula to get us:

\[\arccos\left(\frac{4}{5}\right)=\theta\]

When we plug this into a calculator we get \(\theta\approx36.8699\degree\)
\subsubsection*{Example 2}
\label{sec:org794feed}
A Right Angle Triangle has a Hypotenuse of 13 and an Opposite side of 5; find the angle.

So following the previous example, we know that Arcsine uses the Opposite and Hypotenuse, and so we make:

\begin{align*}
\arcsin\left(\frac{5}{13}\right)&=\theta \\
\therefore\theta&\approx22.6199\degree
\end{align*}
\section*{The Sine Law}
\label{sec:orgea67ced}
Given \emph{any} triangle with sides a, b, and c and corresponding angles on opposing ends A, B, and C we know:

\[\frac{a}{\sin(A)}=\frac{b}{\sin(B)}=\frac{c}{\sin(C)}\]

We also can flip the fractions to get

\[\frac{\sin(A)}{a}=\frac{\sin(B)}{b}=\frac{\sin(C)}{c}\]
\subsection*{Example 1}
\label{sec:org55665c4}
\(\triangle ABC\) with corresponding angles \(abc\) with values of \(A=17, \angle a=42\degree, B=22\), so find \(\angle b\)

The Sine law in this case tells us the ratio \(\frac{\sin(42)}{17}\) \emph{will} be equal to \(\frac{\sin(b)}{22}\) and so we can use this to solve for b:

\[\frac{\sin(b)}{22}=\frac{\sin(42)}{17}\]

as always we can start by cancelling out the denominator by multiplying it by the other side:

\[\sin(b)=\frac{\sin(42)}{17}\times22\]

But notice that this gets us the \emph{sine} of our b value; in order to isolate b completely we need to get it's arcsin:

\begin{align*}
b&=\arcsin\left(\frac{\sin(42)}{17}\times22\right) \\
\therefore \angle b &\approx 59.99\degree
\end{align*}
\section*{The Cosine Law}
\label{sec:orgff5f1b8}
\begin{itemize}
\item The Cosine law is a variation on the Pythagorean Theorem for Right Angle Triangles that states \(A^{2}+B^{2}=C^{2}\) (A, B, and C represent the side measurements)
\item The Cosine Law makes the Pythagorean Theorem accessible to \emph{all} types of Triangles like the Sine Law is:
\end{itemize}

In any triangle \(\triangle ABC\), \(A^{2}=B^{2}+C^{2}-2BC\times\cos(a)\)
As you can see, it essentially just adds a term at the end
\subsection*{Example 1}
\label{sec:org9be24ee}
\(\triangle ABC\) with side A=10, side B=5, and angle c=\(30\degree\); find side C

In this case we don't have a single complete pairing of side and angle to be able to use Sine law so we'll have to go back to our Cosine formula and fill in our blanks:

\[C^{2}=10^{2}+5^{2}-2(10)(5)\cos(30)\]

\begin{itemize}
\item As you can see, the order in which we substitute in our side measurements does not matter as long as we remain consistent, and the angle we're getting the Cosine of \emph{has} to correspond with the side we're trying to find
\item Another thing is that this gets us C \emph{squared} so in order to isolate C we must do:
\end{itemize}

\[C=\sqrt{10^{2}+5^{2}-2(10)(5)\cos(30)}\]

Plugging this into a calculator, we get \(C\approx6.19\)
\subsection*{Example 2}
\label{sec:org87d2cfd}
\(\triangle ABC\) with side A=9, B=10, and C=3; find angle a

Okay so this time we must find an \emph{angle} which means our formula needs some changing; for now let's plug in values:

\[9^{2}=10^{2}+3^{2}-2(10)(3)\cos(a)\]

We can begin isolating a by moving the first 2 terms to the other side as they are addition terms:

\[9^{2}-10^{2}-3^{2}=-2(10)(3)\cos(a)\]

Now to move the remaining terms to the other side, we divide them as they're multiplication terms:

\[\cos(a)=\frac{9^{2}-10^{2}-3^{2}}{-2(10)(3)}\]

And of course to isolate a we get it's arccos:

\begin{align*}
a&=\arccos\left(\frac{9^{2}-10^{2}-3^{2}}{-2(10)(3)}\right) \\
\therefore a&\approx62.18\degree
\end{align*}
\end{document}
