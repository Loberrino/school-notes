#+title: Classification, Nomenclature, and Reactions
#+author: Arad Fathalian
#+startup: latexpreview
#+latex_class: article
#+latex_class_options: [letterpaper]
#+options: num:nil toc:nil
#+latex_header: \everymath{\displaystyle}

* Nomenclature
** Covalent Bonds
+ The element that is /more/ of a Cation tends to come first in the Chemical Formula for Molecules
  + The one that is less Electronegative, or the one that is more to the left or bottom on the Periodic Table
+ If the Compound contains only Carbon and Hydrogen it's a Hydrocarbon and has it's own Nomenclature
  + The suffixes of Hydrocarbons are ane, ene, or yne
+ Remember: The reason we specify the number of each Atom in the name of Molecules is because they come in various ratios unlike with Ionic Bonds

** Polyatomic Ions
+ the Nick the Camel acronym gives you the versions of the Polyatomics that are most common and have the 'ate' suffix
+ Adding 1 more oxygen leaves the charge unaffected but now it also has a 'per' prefix
+ Removing 1 oxygen leaves the charge unaffected but now it has an 'ite' suffix
+ Removing 2 oxygen leaves the charge unaffected but now it also has a 'hypo' prefix
+ Adding 1 Hydrogen to a Polyatomic increases it's charge by 1, increases the charge by 2 if adding 2 Hydrogen

** Hydrates
+ Hydrates are Compounds with water in their structure
+ Name the Ionic Compound normally and then suffix with "hydrate" with a Greek prefix to it to indicate the number of water molecules
+ Anhydrous means water /was/ in the structure but has been removed; typically through applying heat

* Synthesis and Decomposition
** Synthesis
+ Synthesis is the process of 2 different Elements forming a Binary Compound
+ It can also be an Element and a Compound forming another Compound
+ Finally, it can also be 2 different Compounds forming a new Compound

*** Metal+Non-metal
+ A Monovalent Metal and a Non-metal forming an Ionic Compound can also form 1 type of Compound
  + A Multivalent Metal and a Non-metal can form various Ionic Compounds, however
  + For instance, Copper reacting with Chlorine can produce either \(\ce{CuCl_{(s)}}\) or \(\ce{CuCl2_{(s)}}\) Depending on if it's Copper (I) or Copper (II)

*** Non-metal + Non-metal
+ Non-metals synthesize a Molecular Compound
  + Be careful here as there exist many ratios between 2 Non-Metals typically
  + For instance, Carbon and Oxygen can synthesize other Carbon Dioxide or Carbon Monoxide
  + In ambiguous cases like these, *it will be specified* what the correct ratio will be

*** Non-metal + Compound Patterns
+ For Non-Metals Synthesizing with compounds, there are some patterns and formulas you simply must memorize
+ Phosphorus Trichloride + Chlorine Gas \rightarrow Phosphorus Pentachloride
  + \(\ce{PCl3_{(l)} + Cl2_{(g)} -> PCl5_{(s)}}\)
+ Sulphur Dioxide + Oxygen \rightarrow Sulfur Trioxide
  + \(\ce{2SO2_{(g)} + O2_{(g)} -> 2SO3_{(g)}}\)
+ Metal Halide + Oxygen gas \rightarrow Metal Flourate/Chlorate/Bromate/Iodate
  + \(\ce{NaCl_{(s)} + O2_{(g)} -> NaClO3_{(s)}}\)
+ Metal Nitrite + Oxygen \rightarrow Metal Nitrate
  + \(\ce{2NaNO2_{(aq)} + O2_{(g)} -> 2NaNO3_{(aq)}}\)

*** Compound + Compound Patterns
+ Again, these are just patterns and formulas you must memorize and we have no other way to predict the results of these reactions yet
+ A Metal Oxide + Water \rightarrow Metal Hydroxide
  + \(\ce{Na2O_{(s)} + H2O_{(l)} -> 2NaOH_{(aq)}}\)
+ A Non-metal oxide + Water \rightarrow Non-metal Oxyacid
  + \(\ce{SO3_{(g)} + H2O_{(l)} -> H2SO4_{(aq)}}\)
+ A Metal Oxide + Carbon Dioxide \rightarrow Metal Carbonate
  + \(\ce{Na2O_{(s)} + CO2_{(g)} -> Na2CO3_{(aq)}}\)

** Decomposition
+ This is when a Binary Compound decomposes into it's Elements
+ All of the Patterns of Decomposition are identical to Synthesis just in reverse
  + So for instance a Metal Chlorate will decompose into a Metal Chloride and Oxygen
+ Elecrolysis is the main way a Decomposition reaction is triggered (process of using Electricity to cause a Chemical Reaction)
  + For this, the Reactants must be in an Aqueous or a Liquid state

* Single and Double Displacement
** Double Displacement
+ Double Displacement reactions can form solids, gases, water (neutralization reactions for instance) or nothing because both products and reactants are all aqueous
  + When we have the Reaction \(\ce{NaNO3_{(aq)} + CaCl2_{(aq)} -> NaCl_{(aq)} + Ca(NO3)_{(aq)}}\), since everything is aqeuous it all exists in the same environment in free-floating Ionic form
  + All of these Units exist separately in this aqueous substance and so they do not react in any different way and so there is no reaction
+ When one product is aqueous and the other is solid, the solid is a precipitate
  + So in the reaction \(\ce{NaCl_{(aq)} + AgNO3_{(aq)} -> NaNO3_{(aq)} + AgCl_{(s)}}\), Argenous Chloride is the precipitate

*** Solubility of Double Displacement Reactions
+ To determine the Solubility of an Ionic Compound, you must use a Solubility Chart
+ Typically, you must find the Anion of your Compound in the top row, then the corresponding Cation in the corresponding Column
+ From here, if the Cation is present in the "high-solubility" or top boxes, this will form an aqueous solution
  + If not, it will form a precipitate
+ The Periodic Table sheet included with this course includes a Solubility Table on the back which is easy to use
  + There are 2 tables; 1 for Ions that form Soluble Compounds and 1 for Ions that form Insoluble Compounds
  + Look up your Anion; if it exists in table 1 then also look up your Cation
  + If your Anion is listed as Insoluble, check to see if the Cation it's paired with is listed as an exception to this rule

*** Reactions that form Gases
+ These reactions happen in 2 reactions that occur in rapid succession where an intermediate product is produced
+ Here, Double Displacement occurs but one product quickly decomposes into water and a gas

| Intermediate Product           | Gas formed from Decomposition of Intermediate Product |
|--------------------------------+-------------------------------------------------------|
| Carbonic Acid \(\ce{H2CO3}\)       | \(\ce{CO2}\)                                           |
| Ammonium Hydroxide \(\ce{NH4OH}\)    | \(\ce{NH3}\)                                           |
| Sulphurous Acid \(\ce{H2SO3}\)       | \(\ce{SO2}\)                                           |
| Other Gases                    | Other gas: possibly direct formation of \(\ce{H2S}\)    |
|--------------------------------+-------------------------------------------------------|

** Single Displacement
+ A metal can displace another metal in an aqueous Ionic Compound
+ A metal can displace Hydrogen in an Acid
+ A metal can displace Hydrogen in Water
+ A Non-metal (usually a Halogen) can displace another Non-metal form an aqueous Ionic Compound
+ Simply use the Reactivity Series to see when Single Displacements can happen and what special cases apply

* Combustion Reactions
+ Combustion always releases energy in the form of heat and light
+ Combustion is always indicated with a triangle over the yield arrow \(\left(\ce{->[\Delta]}\right)\)
  + The triangle indicates a *Spark* is occurring to yield the products
+ In order to be able to tell if a Hydrocarbon Reaction is Complete or Incomplete, it will either have to be told to you or you will need to know the products instead of predicting them
  + Otherwise, *we know of no way to predict Complete vs. Incomplete Combustion*

** Complete Combustion
+ Complete Combustion always happens because enough Oxygen is present in the reaction
+ A blue flame colour indicates that Complete and efficient Combustion is occurring
+ The products of Complete Combustion will always be Water Vapour and Carbon Dioxide (as well as energy technically)

** Incomplete Combustion
+ Incomplete Combustion always occurs because not enough Oxygen is present in the reaction for Complete Combustion
+ A yellow flame colour indicates that Incomplete Combustion is occurring and that the flame is inefficient
+ The products of Incomplete Combustion will always be Carbon Dioxide, Water Vapour, Carbon Monoxide, Carbon (soot), and technically Energy

** Combustion of Non-Hydrocarbons
+ Substances aside from Hydrocarbons can also combust with Oxygen
  + Examples may include Hydrogen, Magnesium, and Sulphur
+ These essentially boil down to Synthesis Reactions
