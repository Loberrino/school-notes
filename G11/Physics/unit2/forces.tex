%& /home/arad/.emacs.d/.local/cache/org/persist/65/3e2db7-8cec-47d2-b973-00b233709ad7-f5d059d8c2bbcf8c260e6653e0c37ca3
% Created 2024-09-10 Tue 21:50
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage{capt-of}
\usepackage[fixamsmath]{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{longtable}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage[version=4]{mhchem}
\usepackage{chemfig}
\usepackage{gensymb}
\usepackage[product-units=bracket-power, sticky-per]{siunitx}
\usepackage{cancel}
\usepackage{fourier}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{quotes,angles,decorations.pathreplacing} \everymath{\displaystyle}

%% ox-latex features:
%   !announce-start, !guess-pollyglossia, !guess-babel, !guess-inputenc, maths,
%   !announce-end.

\usepackage{amsmath}
\usepackage{amssymb}

%% end ox-latex features


% end precompiled preamble
\ifcsname endofdump\endcsname\endofdump\fi

\author{Arad Fathalian}
\date{\today}
\title{Forces}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Forces},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Types of Forces}
\label{sec:orgc2af1b1}
\begin{itemize}
\item A Force \(\left(\vec{F}\right)\) in it's simplest terms is a push or a pull
\begin{itemize}
\item Therefore, Forces are Vectors
\end{itemize}
\item Forces can cause objects to change their motion or shape
\item The unit of force is called the \textbf{Newton} \(\left(\vec{\unit{\N}}\right)\)
\begin{itemize}
\item This is a Standard International unit equal to \(\unit{\kg\m\per\s\squared} \lor \unit[per-mode=symbol]{\kg\m\per\s\squared}\) (Kilogram-meters per seconds squared)
\item This essentially means \(\vec{F}=m\vec{a}\) where \(m\) is mass and \(\vec{a}\) is, of course, acceleration
\end{itemize}
\item Forces are generally classified into 4 categories:
\end{itemize}
\subsection*{Gravitational Force}
\label{sec:orgcddac7d}
\begin{itemize}
\item This is the force of attraction between all the objects in the universe
\item This is a weak but a long-range force
\item It is \emph{purely} attraction
\item There are two different definitions of Gravity; there is the \textbf{Force} of Gravity and there is the \textbf{Gravitational Field Constant}
\begin{itemize}
\item The \textbf{Force} of Gravity is the force that acts upon objects, but the \textbf{Gravitational Field Constant} is what acts upon objects \emph{per unit mass}
\item So essentially, the Force of Gravity is a formula \((\vec{F_{g}}=m\vec{g})\) whereas the Gravitational Field Constant is, well, a Constant \(\left(G=\qty{6.674e-11}{\N\m\squared\per\kg\squared}\right)\)
\item Sometimes this constant is also expressed as \(G=\qty{6.674e-2}{\mm\cubed\per\kg\s\squared}\) but we will not be using this
\end{itemize}
\item The Gravitational Field Constant - also called the Universal Gravity Constant - is the force of attraction all objects in the universe feel essentially
\item There is also the formula \(\vec{F_{g}}=\frac{Gm_{1}m_{2}}{r^{2}}\) where \(\vec{F_{g}}\) is the attractive force between two massive bodies
\begin{itemize}
\item The two masses are \emph{always} in Kilograms while the radius is always in metres
\item The radius is the measure of the distance between the two massive bodies
\end{itemize}
\end{itemize}
\subsubsection*{An Example of the Gravitational Force Formula}
\label{sec:org85d7a26}
Some object that weighs \(\qty{75}{\kg}\) orbits the Earth at an altitude of \(\qty{450}{\km}\) above the surface. What is the magnitude of the force of gravity acting upon it if we assume that the mass of the Earth is about \(\qty{5.97e24}{\kg}\) and that it's radius to the surface is about \(\qty{6.37e6}{\m}\)?

Here, our givens are:
\begin{align*}
m_{1}&=\qty{75}{\kg} \\
m_{2}&=\qty{5.97e24}{\kg} \\
r&=\qty{6.37e6}{\m} \\
d&=\qty{450}{\km}
\end{align*}

\begin{itemize}
\item While we know that the radius of the Earth's origin to it's \emph{surface}, in order to get the total radius, we must add that distance with the distance from the surface to the object
\item To do that, we convert everything into metres, since that's what we're going to be putting into the formula anyways
\item Then, we have everything we need to find the total force of gravity:
\end{itemize}

\begin{align*}
\vec{F_{g}}&=\frac{Gm_{1}m_{2}}{r^{2}} \\
&=\frac{\left(\qty{6.674e-11}{\N\m\squared\per\cancel\kg\squared}\right)\qtyproduct{75 x 5.97e24}{\cancel\kg}}{\left(\qty{6.37e6}{\m}+\qty{450e3}{\m}\right)^{2}} \\
&=\frac{\qty{2.9882835e16}{\N\cancel\m\squared}}{\qty{4.65124e13}{\cancel\m\squared}} \\
&\approx\qty{6.4e2}{\N}[down]
\end{align*}
\subsubsection*{Differentiating Weight and Mass}
\label{sec:orgb315dcd}
\begin{itemize}
\item \emph{Mass} is the measure of the matter that takes up a certain portion of space
\item \emph{Weight} is now the measure of how strongly gravity influences an object, which is therefore impacted by the Mass since \(\vec{F_{g}}=m\left(\qty{9.81}{\m\per\s\squared}\right)\)
\item Because of this, Mass is measured in \textbf{grams}, while Weight is measured as a \textbf{Force} (specifically, \(\text{Weight}=\vec{F_{g}}=m\vec{g}\))
\item And so because of that, the Mass you feel on Earth is not your \emph{true} mass in a sense
\item For many instances, the Weight that you're given can be assumed to also be your Mass; while they are different, they are used interchangeably in many instances
\end{itemize}
\subsubsection*{Air Resistance and Terminal Velocity}
\label{sec:orgc2f759c}
\begin{itemize}
\item \textbf{Free Fall} occurs when an object is only being impacted by the force of gravity, and air resistance is either nonexistent or negligible
\item As an object has greater surface area, air resistance impacts it more, while as it has less surface area, air resistance impacts it less
\item As well, as the Mass of an object increases, the force of gravity has a greater effect on the object, but so does the force of air resistance
\item Finally, when the speed an object is going at increases, so does Air Resistance
\item As an object falls through the Atmosphere, Gravity makes that object accelerate but Air Resistance pushes back against the acceleration of Gravity
\begin{itemize}
\item Eventually, the two colliding forces balance out at a point called the \textbf{Terminal Velocity} of the object
\end{itemize}
\item This object has reached a constant Velocity at this point; it cannot go any faster as long as there are no other Forces introduced to it
\end{itemize}
\subsection*{Electromagnetic Force}
\label{sec:org71db048}
\begin{itemize}
\item These are essentially your dipole forces from Chemistry
\item Electromagnetic Forces can exert \emph{either} an attraction or a repulsion
\begin{itemize}
\item They therefore tend to cancel each other out
\end{itemize}
\item These are the forces that hold atoms and molecules together
\item A majority of the forces experienced are Electromagnetic
\item This is a strong \emph{and} long-range force
\end{itemize}
\subsection*{Strong Nuclear Force}
\label{sec:org7efb2da}
\begin{itemize}
\item This is the force that holds Protons and Neutrons together in a Nucleus
\item This force is \emph{very} strong (the strongest of the categories) but short-range
\end{itemize}
\subsection*{Weak Nuclear Force}
\label{sec:orgb9f418c}
\begin{itemize}
\item There are many other ``elementary'' particles (such as the Electron) which are unstable and break up
\item The Weak Nuclear Force is responsible for this \textbf{radioactive decay}
\item It is a strong force but incredibly short-range
\end{itemize}
\section*{Free-Body Diagrams}
\label{sec:orgd4616f6}
\begin{itemize}
\item A Free-Body Diagrams are simple drawings to represent the forces acting on an object at a given moment
\item Each force is a Vector originating from a central origin (representing the centre of gravity) labelled as \(\vec{F}\) with some corresponding subscript
\end{itemize}
\subsection*{Example 1: Still Object}
\label{sec:orgc238bb9}
\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)} node[midway] {\(/\)};
\end{tikzpicture}

\begin{itemize}
\item This is the Free-Body Diagram for an object that is laying on some flat surface without any horizontal forces acting upon it
\item There is a force that is dragging the object's centre of gravity downwards (being gravity of course) and a force dragging the object's centre of gravity upwards (This is called the \textbf{Normal} force)
\begin{itemize}
\item The Normal force always exists to counteract some other force when your object is on a surface, and a force is pushing the object into the surface
\item So in this case, the object sits on some ground, and gravity is pushing the object further into the surface, so the Normal force keeps gravity from completely crushing it
\item The Normal force is always going to be \textbf{perpendicular} to the surface in question; not always aligned with the force it counteracts
\end{itemize}
\item There are ticks going through these two Vectors which indicates that they are \emph{exactly equal} to each other in magnitude; this means that they \emph{cancel out}
\item Because of this, the \textbf{Net Force} \(\left(\vec{F_{Net}}\right)\) is equal to 0
\end{itemize}
\subsection*{Example 2: An Object Being Dragged Rightward Across a Surface}
\label{sec:org5367eff}
\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)};
\draw[->] (0,0) -- (-0.75,0) node[left,below] {\(\vec{F_{fk}}\)};
\end{tikzpicture}

\begin{itemize}
\item As you can see, the vertical (y-component) forces are still present and still cancel each other out, but now there are two horizontal (x-component forces)
\item The greater of the two forces is the \textbf{Applied Force}; this is the force that is actually pushing the object rightward
\item There is a counter-force presenting resistance, which is \textbf{Friction}
\item Because the y-component forces cancel out, and because the Applied Force is greater than it's friction, \(\vec{F_{Net}}\) shall be a purely x-component Vector going to the right
\end{itemize}
\subsection*{Example 3: An Object Being Pushed Against a Wall on a Level Surface}
\label{sec:org4889a2e}
\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (-1.5,0) node[left,below] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\end{tikzpicture}

\begin{itemize}
\item So remember, since the object is being pushed against a surface, a Normal force is there to counteract it
\item The y and the x components cancel out here; \(\vec{F_{Nety}}=0 \land \vec{F_{Netx}}=0\)
\end{itemize}
\section*{Newton's Laws of Motion}
\label{sec:orgdf22a0a}
\subsection*{Law 1}
\label{sec:org7389df8}
\begin{itemize}
\item If an object is in motion, it will stay in motion at a constant velocity unless an external unbalanced force acts upon it
\item For instance, in space, a planet moving along a line will continue to move along that line until and unless gravity or some applied force acts upon it
\item This tendency to stay in motion is also called \textbf{Inertia} which is why this law is sometimes called the \emph{Law of Inertia}
\item As well, and inversely, this law states that an object that stays it rest will stay at rest unless and until an external unbalanced force acts upon it
\end{itemize}
\subsection*{Law 2}
\label{sec:orge6f0bc3}
\begin{itemize}
\item Law 2 now defines the \(\vec{F}=m\vec{a}\) formula from before
\item It also states that \(\vec{F}\propto\vec{a}\) (Force is \textbf{proportional} to Acceleration; as Acceleration increases, so does Force, and vice versa)
\item As well, this law states \(\vec{a}\propto m^{-1}\) (Acceleration is \textbf{proportional} to the reciprocal of Mass; as Mass increases, Acceleration decreases and vice versa)
\item Finally, Newton's Second Law states that when there is no Force acting on an object, there is also no Acceleration, which will mean that the object either isn't moving or is moving at a constant Velocity
\end{itemize}
\subsubsection*{Pulleys}
\label{sec:orgaac55c6}
\begin{itemize}
\item Example 1
\label{sec:org206ede4}
Say that a cart weighing \(\qty{0.80}{\kg}\) is placed on a table, and one end is tied to a light string over a pulley, and at the other end of the string lies a \(\qty{0.20}{\kg}\) hanging weight. Assume no friction acts upon either objects.
Find the magnitude of the acceleration of the cart as well as the hanging object, then the magnitude of the tension

\begin{itemize}
\item So firstly, let's draw Free Body Diagrams for the Cart \((m_{1})\) and for the Weight \((m_{2})\):
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{T}}\)};
\end{tikzpicture}

\begin{itemize}
\item So the cart has y-components that cancel out, and it has \(\vec{F_{net}}=\vec{F_{T}}\)
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.25) node[right] {\(\vec{F_{T}}\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)};
\end{tikzpicture}

\begin{itemize}
\item So the weight has no x-components, but it has the x-components that give \(\vec{F_{net}}=\vec{F_{g}}-\vec{F_{T}}\)
\item The \(\vec{F_{T}}\) in both the cart and the weight are the same force since it's the same force since they both are affected by the same rope, so you can substitute them into one another
\item This gives \(\vec{F_{m_{2}}}=\vec{F_{g}}-\vec{F_{m_{1}}}\) which can also be written as \(m_{2}\vec{a}=m_{2}\vec{g}-m_{1}\vec{a}\) since both objects feel the same acceleration given they are both affected by the same rope
\item Now we can move both acceleration terms to one side and isolate for \(\vec{a}\):
\end{itemize}

\begin{align*}
m_{2}\vec{a}+m_{1}\vec{a}&=m_{2}\vec{g} \\
\vec{a}(m_{2}+m_{1})&=m_{2}\vec{g} \\
\vec{a}&=\frac{m_{2}\vec{g}}{m_{2}+m_{1}} \\
&=\frac{0.2(9.81)}{0.2+0.8} \\
&=\qty{1.962}{\m\per\s\squared}
\end{align*}

\begin{itemize}
\item Now that we have this, we can also find the magnitude of the tension using the formula \(m_{1}\vec{a}=\vec{F_{T}}\) from before to get \(\vec{F_{T}}=\qty{1.5696}{\N}\)
\end{itemize}
\item Example 2
\label{sec:orgae7a4cb}
A cart \((\qty{1.20}{\kg})\) is being pulled forward on a surface by a rope going through a pulley attached on the other end to a weight. \((\qty{0.60}{\kg})\) Find the acceleration of the Cart if there's a \(\qty{0.50}{\N}\) force of friction.

\begin{itemize}
\item So here, again, let's make Free Body Diagrams for the Cart \((m_{1})\) and the Weight \((m_{2})\) again:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{T}}\)};
\draw[->] (0,0) -- (-0.75,0) node[left,below] {\(\vec{F_{f}}\)};
\end{tikzpicture}

\begin{itemize}
\item So here, \(\vec{F_{m_{1}}}=\vec{F_{T}}-0.5\)
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.25) node[right] {\(\vec{F_{T}}\)};
\draw[->] (0,0) -- (0,-1.5) node[right] {\(\vec{F_{g}}\)};
\end{tikzpicture}

\begin{itemize}
\item And here, \(\vec{F_{m_{2}}}=\vec{F_{g}}-\vec{F_{T}}\)
\item So again, we do our substitution to get \(\vec{F_{m_{2}}}=\vec{F_{g}}-(\vec{F_{m_{1}}}+0.5)\) which expands further into \(m_{2}\vec{a}=m_{2}\vec{g}-(m_{1}\vec{a}+0.5)\)
\item You know the rest:
\end{itemize}

\begin{align*}
m_{2}\vec{a}+m_{1}\vec{a}&=m_{2}\vec{g}-0.5 \\
\vec{a}(m_{2}+m_{1})&=m_{2}\vec{g}-0.5 \\
\vec{a}&=\frac{m_{2}\vec{g}-0.5}{m_{2}+m_{1}} \\
&=\frac{0.6(9.81)-0.5}{0.6+1.2} \\
&\approx\qty{3}{\m\per\s\squared}
\end{align*}
\end{itemize}
\subsection*{Law 3}
\label{sec:orgf12a052}
\begin{itemize}
\item ``For every action there is an equal and opposite reaction''
\item If two objects interact, they forces they impart upon each other will be equal and go in opposing directions
\item This means that forces \emph{always} come in pairs; there can never be a lone singular force without some kind of complementary force
\item Think of a hammer hitting a nail; the force from your hand makes the hammer accelerate and impact the nail with a force
\item The nail then hits the hammer back with the same force, however, since the mass of the nail is far less than the mass of the hammer, this force makes the nail accelerate forward a lot more than it makes the hammer bounce back
\item That is the reason why objects still accelerate even when their forces are always met with equal and opposite forces; there is a difference in masses between objects
\begin{itemize}
\item This difference in objects means that forces make certain objects accelerate a lot more or a lot less than objects they impact with, since \(\vec{a}\propto m^{-1}\) as the second law states
\end{itemize}
\end{itemize}
\subsubsection*{Example 1}
\label{sec:org0bc913a}
Two buckets of water are lifted, one above the other. The top bucket weighs \(\qty{3.0}{\kg}\) and the bottom bucket weighs \(\qty{4.0}{\kg}\). Their action-reaction force is \(\qty{50.0}{\N}\). Calculate the force of Tension lifting them up.

\begin{itemize}
\item As always, our first step should typically be to draw free body diagrams and state whichever equations we can
\item For the top Bucket (which we shall call Bucket A):
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[above] {\(\vec{F_{T}}\)};
\draw[->] (0,0) -- (0,-1.5) node[below] {\(\vec{F_{B\rightarrow A}}+\vec{F_{g_{A}}}\)};
\end{tikzpicture}

\begin{itemize}
\item Well, we know that \(\vec{F_{B\rightarrow A}}=\qty{50.0}{\N}[Down]\) and because we know that \(m_{A}=\qty{3.0}{\kg}\), we can determine \(\vec{F_{g_{A}}}=(3)(9.81)=\qty{29.43}{\N}[Down]\)
\item And so, \(\vec{F_{A}}=\vec{F_{T}}-\qty{79.43}{\N}\)
\item Now, for the bottom Bucket (which we shall call Bucket B):
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[above] {\(\vec{F_{A\rightarrow B}}\)};
\draw[->] (0,0) -- (0,-1.5) node[below] {\(\vec{F_{g_{B}}}\)};
\end{tikzpicture}

\begin{itemize}
\item Again, since Law 3 states that action-reaction forces are equal and opposite, \(\vec{F_{B\rightarrow A}}=\vec{F_{A\rightarrow B}}=\qty{50.0}{\N}\)
\item As well, \(\vec{F_{g_{B}}}=(4)(9.81)=\qty{39.24}{\N}[Down]\)
\item And so, \(\vec{F_{B}}=\qty{10.76}{\N}[Up]\)
\item Finally, for the whole System:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (0,1.5) node[above] {\(\vec{F_{T}}\)};
\draw[->] (0,0) -- (0,-1.5) node[below] {\(\vec{F_{g}}\)};
\end{tikzpicture}

\begin{itemize}
\item Since we know the total mass to be \(\qty{3.0}{\kg}+\qty{4.0}{\kg}=\qty{7}{\kg}\), we can determine \(\vec{F_{g}}=(7)(9.81)=\qty{68.67}{\N}[Down]\)
\item And so in total we have:
\begin{itemize}
\item \(\vec{F_{Net}}=\vec{F_{T}}-\qty{68.67}{\N}\)
\item \(\vec{F_{A}}=\vec{F_{T}}-\qty{79.43}{\N}\)
\item \(\vec{F_{B}}=\qty{10.76}{\N}[Up]\)
\end{itemize}
\item Now, we must make the assumption that the Acceleration felt across both buckets is the same
\item This means we can calculate the Acceleration felt by all buckets by doing \(\vec{a}=\frac{\vec{F}}{m}\)
\item In the case of Bucket B, we know completely what it's net force is so that's the most optimal one to use: \(\vec{a_{Net}}=\frac{\qty{10.76}{\N}}{\qty{4}{\kg}}=\qty{2.69}{\m\per\s\squared}\)
\item Now, again, because we said that the net Acceleration is consistent across both Buckets, we can use this in our System equation to get \(\vec{F_{Net}}=(\qty{7}{\kg})\left(\qty{2.69}{\m\per\s\squared}\right)\)
\item That means we can write \((7)(2.69)=\vec{F_{T}}-\qty{68.67}{\N}\) which means that \(\vec{F_{T}}=\qty{87.5}{\N}[Up]\) (\(\qty{88}{\N}[Up]\) with consideration for Significant Figures)
\end{itemize}
\subsubsection*{Example 2}
\label{sec:org3a5eaf3}
Two objects are being pulled Eastward. The front object has a mass of \(\qty{4200}{\kg}\) and the second has a mass of \(\qty{3300}{\kg}\). There is an applied force of \(\qty{25.9}{\kN}\) pulling them forward. Find the action-reaction forces acting between the objects if the first object experiences \(\qty{1500}{\N}\) of friction and the second object experiences \(\qty{2100}{\N}\) of friction.

\begin{itemize}
\item Let's call the front object A and the back object B; for object A (gravity and normal shall be excluded as they simply cancel out and thus are redundant here):
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f_{a}}}+\vec{F_{B\rightarrow A}}\)};
\end{tikzpicture}

\begin{itemize}
\item Now for object B:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{A\rightarrow B}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f_{B}}}\)};
\end{tikzpicture}

\begin{itemize}
\item And for the whole system:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f}}\)};
\end{tikzpicture}

\begin{itemize}
\item And so, our net equations are:
\begin{itemize}
\item \(\vec{F_{A}}=\qty{25900}{\N}-\qty{1500}{\N}-\vec{F_{B\rightarrow A}}=\qty{24400}{\N}-\vec{F_{B\rightarrow A}}\)
\item \(\vec{F_{B}}=\vec{F_{A\rightarrow B}}-\qty{2100}{\N}\)
\item \(\vec{F_{Net}}=\qty{25900}{\N}-(\qty{1500}{\N}+\qty{2100}{\N})=\qty{22300}{\N}\)
\end{itemize}
\item In both the equations of \(\vec{F_{A}} \land \vec{F_{B}}\) all that we need to isolate the action-reaction forces are a value for \(\vec{F_{A}} \land \vec{F_{B}}\) themselves, which we could get if we had \(\vec{a_{Net}}\)
\item We can find \(\vec{a_{Net}}\) by using the equation \(\qty{22300}{\N}=(\qty{4200}{\kg}+\qty{3300}{\kg})\vec{a_{Net}}\) which gets \(\vec{a_{Net}}=\frac{22300}{7500}\unit{\m\per\s\squared}\)
\item Now we can use either the net equation for object A or B; let's use object A:
\end{itemize}

\begin{align*}
\vec{F_{A}}&=\qty{24400}{\N}-\vec{F_{B\rightarrow A}} \\
(\qty{4200}{\kg})\left(\frac{22300}{7500}\unit{\m\per\s\squared}\right)&=\qty{24400}{\N}-\vec{F_{B\rightarrow A}} \\
-\left(\frac{4200\cdot22300}{7500}-24400\right)&=\vec{F_{B\rightarrow A}} \\
\vec{F_{B\rightarrow A}}&=\qty{11912}{\N}\approx\qty{12000}{\N}[W]\approx\qty{12}{\kN}[W] \\
\therefore \vec{F_{B\rightarrow A}}\approx\qty{12}{\kN}[W] &\iff \vec{F_{A\rightarrow B}}\approx\qty{12}{\kN}[E]
\end{align*}
\section*{Friction}
\label{sec:org4cd42fd}
\begin{itemize}
\item Whenever an object is in motion along a surface, the surface exerts forces upon the object
\begin{itemize}
\item One component is the Normal force
\end{itemize}
\item The force of \textbf{Friction} is like the Normal Force but it remains \emph{parallel} to the surface instead of \emph{perpendicular} to it
\item The \textbf{Coefficient of Friction} \(\left(\mu\right)\) is less on smoother surfaces and more on rougher surfaces
\item The Coefficient of Friction is a scalar that is equal to the ratio of the magnitude of \(\vec{F_{f}}\) and the magnitude of \(\vec{F_{N}}\); \(\mu=\frac{|\vec{F_{f}}|}{|\vec{F_{N}}|}\)
\begin{itemize}
\item Notice the Notation here; the Absolute Value of a Vector is equivalent to it's Magnitude/length
\item This is because the Absolute Value of something outputs it's distance from 0; in the case of a Vector, you will get it's distance from the Origin, which is simply it's length, or Magnitude
\item Keep in mind that this Coefficient is a \textbf{Scalar}; it can only be calculated using Scalar Values and it cannot ever imply direction
\item Generally, you're gonna have to simply entrust that the value of the Friction Coefficient for the surface will be \emph{given to you} if you are not given the value of the Friction and the Normal Forces
\item A way that you may indirectly be given the Normal force is by being given a way to calculate the Gravity force for an object laying still on a level surface; in such a case, \(\vec{F_{g}}=-\vec{F_{N}} \lor \vec{F_{g}}[Down]=\vec{F_{N}}[Up]\)
\end{itemize}
\item There are two types of Friction
\item \textbf{Static Friction} resists the \emph{initiation} of movement across a surface; when you struggle to start sliding an object across a surface, Static Friction is responsible
\begin{itemize}
\item Static Friction is always directionally opposed to the Applied Force in the x-component, and will prevent the object from moving until \(\vec{F_{a}}>\vec{F_{f}}\)
\item \(\vec{F_{f_{s}}}\propto\vec{F_{N}}\), so when the Normal Force is greater, the force of Static Friction is also greater
\item Static Friction has a maximum magnitude which can be calculated as \(|\vec{F_{f_{smax}}}|=\mu_{s}|\vec{F_{N}}|\) (Maximum Static Friction is the product of the magnitude of the Normal force with the Frictional Coefficient)
\item This formula also means \(\frac{|\vec{F_{f_{smax}}}|}{|\vec{F_{N}}|}=\mu_{s}\), meaning that the Coefficient of Friction is indeed the ratio of the Force of Friction to the Normal
\item The values of \(\mu_{s}\) can vary depending on the surface, for which the values are usually:
\item When the Applied Force exceeds this maximum Static Friction force, the object can move
\end{itemize}
\item \textbf{Kinetic Friction} resists the movement of an object across the surface once it is \emph{already moving}
\begin{itemize}
\item \(\vec{F_{f_{k}}}<\vec{F_{f_{s}}}\) always; it always takes more force to \emph{start} an object's movement along a surface than it takes to continue it
\item The equation \(|\vec{F_{f_{k}}}|=\mu_{k}|\vec{F_{N}}|\) can be used, very similarly to with Static Friction, and again this also means \(\frac{|\vec{F_{f_{k}}}|}{|\vec{F_{N}}|}=\mu_{k}\)
\end{itemize}
\end{itemize}
\subsection*{The Values of the Friction Coefficient}
\label{sec:org2f85bf7}
\begin{center}
\begin{tabular}{lrr}
Surfaces & \(\mu_{s}\) & \(\mu_{k}\)\\
\hline
Glass on Glass & 0.94 & 0.4\\
Steel on Steel & 0.74 & 0.57\\
Copper on Steel & 0.53 & 0.36\\
Ice on Ice & 0.1 & 0.03\\
Teflon on Steel & 0.04 & 0.04\\
\hline
\end{tabular}
\end{center}
\subsection*{Example 1}
\label{sec:org05fa2ae}
Two objects are being pulled horizontally by a string. The front object has a mass of \(\qty{5.0}{\kg}\) and the back object has a mass of \(\qty{2.0}{\kg}\). They're being pulled via a force of \(\qty{10.0}{\N}\). Both objects have a friction Coefficient of \(0.10\). Calculate the net Acceleration the objects experience and the tension of the rope between them.

\begin{itemize}
\item We know the mass of both objects which means we can calculate the Force of gravity acting upon both objects, and then by changing their signs, we have found the Normal force
\end{itemize}

\begin{align*}
\vec{F_{g_{A}}}&=(\qty{5.0}{\kg})\left(\qty{-9.81}{\m\per\s\squared}\right) \\
&=\qty{-49.05}{\N} [Up] \\
\therefore \vec{F_{N_{A}}}&=\qty{49.05}{\N} [Up] \\
\vec{F_{f_{A}}}&=(0.10)(\qty{49.05}{\N}) \\
&=\qty{4.905}{\N} [B] \\
\vec{F_{g_{B}}}&=(\qty{2.0}{\kg})\left(\qty{-9.81}{\m\per\s\squared}\right) \\
&=\qty{-19.62}{\N} [Up] \\
\therefore \vec{F_{N_{B}}}&=\qty{19.62}{\N} [Up] \\
\vec{F_{f_{B}}}&=(0.10)(\qty{19.62}{\N}) \\
&=\qty{1.962}{\N} [B]
\end{align*}

\begin{itemize}
\item Now we can draw our free body diagrams, starting with the one for object A (front):
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f_{A}}}+\vec{F_{B\rightarrow A}}\)};
\end{tikzpicture}

\begin{itemize}
\item Now for object B:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{A\rightarrow B}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f_{B}}}\)};
\end{tikzpicture}

\begin{itemize}
\item And now for the whole system:
\end{itemize}

\begin{tikzpicture}
\draw (-1,-1) -- (1,-1) -- (1,1) -- (-1,1) -- cycle;
\filldraw (0,0) circle (2pt);
\draw[->] (0,0) -- (1.5,0) node[right] {\(\vec{F_{a}}\)};
\draw[->] (0,0) -- (-1.5,0) node[left] {\(\vec{F_{f}}\)};
\end{tikzpicture}

\begin{itemize}
\item And so we have the equations:
\begin{itemize}
\item \(\vec{F_{A}}=\qty{10}{\N}-\qty{4.905}{\N}-\vec{F_{B\rightarrow A}}=\qty{5.095}{\N}-\vec{F_{B\rightarrow A}}\)
\item \(\vec{F_{B}}=\vec{F_{A\rightarrow B}}-\qty{1.962}{\N}\)
\item \(\vec{F_{Net}}=\qty{10}{\N}-(\qty{4.905}{\N}+\qty{1.962}{\N})=\qty{3.133}{\N}[F]\)
\end{itemize}
\item Now we can calculate Acceleration using the net Force: \(\vec{a_{Net}}=\frac{3.133}{7}\unit{\m\per\s\squared}\)
\item And now that we have this we can calculate the tension of the rope between the objects as well:
\end{itemize}

\begin{align*}
(\qty{5.0}{\kg})\left(\frac{3.133}{7}\unit{\m\per\s\squared}\right)&=\qty{5.095}{\N}-\vec{F_{B\rightarrow A}} \\
-\left(\frac{\qty{5}{\kg}\cdot\qty{3.133}{\m}}{\qty{7}{\s\squared}}-\qty{5.095}{\N}\right)&=\vec{F_{B\rightarrow A}} \\
\vec{F_{B\rightarrow A}}&\approx\qty{-2.9}{\N}[F] \\
&\therefore \vec{a_{Net}}\approx\qty{0.45}{\m\per\s\squared}[F] \land \vec{F_{T_{A\leftrightarrow B}}}\approx\pm\qty{2.9}{\N}
\end{align*}
\section*{Forces on an Inclined Plane}
\label{sec:orgf2c4a9b}
\begin{itemize}
\item Say that you have an object that is on an Inclined Plane such as in the following diagram:
\end{itemize}

\begin{tikzpicture}
\filldraw (0,0) circle (2pt);
\draw[very thick] (-1,0) -- (0,1) -- (1,0) -- (0,-1);
\draw[very thick] (-2,1) coordinate (A) -- (1,-2) coordinate (B) -- (-2,-2) coordinate (C) -- cycle;
\draw[->] (0,0) -- (2.5,2.5) node[right] {\(\vec{F_{N}}\)} node[midway] {\(/\)};
\draw[->] (0,0) -- (-2.5,-2.5) coordinate (F) node[midway] {\(/\)} node[below,left] {\(\vec{F_{g}}\cos\left(\theta\right)\)};
\draw[->] (0,0) coordinate (E) -- (0,-5) node[below] {\(\vec{F_{g}}\)} coordinate (D);
\draw[dashed] (-2.5,-2.5) -- (0,-5);
\draw[->] (0,0) -- (1,-1) node[midway] {\(/\)} node[below,right] {\(\vec{F_{g}}\sin\left(\theta\right)\)};
\draw[->] (0,0) -- (-1,1) node[left] {\(\vec{F_{f}}\)} node[midway] {\(/\)};
\draw pic["\(\theta\)", draw, <->, angle eccentricity=1.25, angle radius=0.75cm]
    {angle=A--B--C};
\draw pic["\(\theta\)", draw, <->, angle eccentricity=1.25, angle radius=1.15cm]
    {angle=F--E--D};
\end{tikzpicture}

\begin{itemize}
\item Here, we can see that the force of Gravity continues to face down as purely a y-component while the force of the Normal continues to stay \textbf{perpendicular to the surface}
\item There is a force that balances out the Normal and there is a force that keeps the object moving  parallel to the surface in opposition to the friction
\item These forces are calculated as \(\vec{F_{g}}\cos\left(\theta\right) \land \vec{F_{g}}\sin\left(\theta\right)\) respectively which can also be written as \(m\vec{g}\cos\left(\theta\right) \land m\vec{g}\sin\left(\theta\right)\)
\item The reason for this is that the angle of how raised the incline is from the horizontal is the same as the angle in between the gravity vector and the one that opposes the normal force
\begin{itemize}
\item These angles are equal because both of these angles are contained in smaller right angles which have opposing angles, making \emph{all} of their angles therefore equal
\end{itemize}
\item The reason that the force which opposes the friction force is now equal to \(\vec{F_{g}}\sin\left(\theta\right)\) is that the connecting line between the gravity vector and the one opposing the normal force is equal to \(\vec{F_{g}}\sin\left(\theta\right)\)
\begin{itemize}
\item We can make the assumption that these two magnitudes are equal
\end{itemize}
\item Remember: along an incline, the formula \(|\vec{F_{g}}|\neq|\vec{F_{N}}|\); now it's \(|\vec{F_{N}}|=|\vec{F_{g}}\cos\left(\theta\right)|\)
\end{itemize}
\end{document}
