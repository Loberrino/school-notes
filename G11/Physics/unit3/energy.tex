%& /home/arad/.emacs.d/.local/cache/org/persist/f5/2e5679-c51f-48f6-9f5e-8de4fe60d642-1c7bdd8f043e5d4e9bf6ae4ff0d20036
% Created 2024-09-09 Mon 01:07
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage{capt-of}
\usepackage[fixamsmath]{mathtools}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage{rotating}
\usepackage{wrapfig}
\usepackage{longtable}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}
\usepackage{mhchem}
\usepackage{chemfig}
\usepackage{gensymb}
\usepackage[product-units=bracket-power, sticky-per]{siunitx}
\usepackage{cancel}
\usepackage{fourier}
\usepackage{pgfplots}
\usepackage{tikz}
\usetikzlibrary{quotes,angles,decorations.pathreplacing} \everymath{\displaystyle}

%% ox-latex features:
%   !announce-start, !guess-pollyglossia, !guess-babel, !guess-inputenc, maths,
%   !announce-end.

\usepackage{amsmath}
\usepackage{amssymb}

%% end ox-latex features


% end precompiled preamble
\ifcsname endofdump\endcsname\endofdump\fi

\author{Arad Fathalian}
\date{\today}
\title{Energy}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Energy},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.4 (Org mode 9.8-pre)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Work and Energy}
\label{sec:org032a79f}
\begin{itemize}
\item Work is an action performed on an object such that a force \emph{displaces} the object
\begin{itemize}
\item To find Work, use the formula \(W=|\vec{F}||\Delta\vec{d}|\cos(\theta)\) where theta is the angle at which you exert your force if the displacement vector of the object is the horizontal
\item You'll notice that Work is a Scalar; it's positive when it's in the same direction as the displacement, and negative when it's in the opposite direction
\end{itemize}
\item This means that Work has a unit of \(\unit{\N\m}\) which is equivalent to the unit \textbf{Joules} \((\unit{\J})\)
\begin{itemize}
\item If we expand the Newton unit, we can see that \(\unit{\J}=\left(\unit{\kg\m\per\s\squared}\right)(\unit{\m}) \lor \unit{J}=\unit{\kg\m\squared\per\s\squared}\)
\end{itemize}
\item Only the Forces that cause displacement are considered Work; if you push an object at an angle while it is on a level surface, only the x-component of the applied force is work
\item The Cosine of the angle you're pushing at will be equal to 1 if you're pushing perfectly along the surface, and it will be 0 if you're pushing perpendicular to the surface
\begin{itemize}
\item This is because of how Cosine behaves on the Unit Circle/Vector; \(\cos(0\degree)=\cos(180\degree)=\pm1 \land \cos(90\degree)=\cos(270\degree)=0\)
\item The formula \(W=|\vec{F}||\Delta\vec{d}|\) will work for any Work being done \emph{parallel to the surface}; when your angle is perfectly parallel to the surface, Work is just the force multiplied by the displacement
\item when it's perpendicular, on the other hand, there is no work being done at all because the object cannot be displaced
\end{itemize}
\end{itemize}
\subsection*{Example: Why This Formula Works}
\label{sec:orgd3c6e44}
There is an applied Force of \(\qty{50.0}{\N}\) placed upon an object at \(30\degree\) to the \(\qty{3.00}{\m}\) of displacement being done on it. Find the amount of Work being performed

\begin{itemize}
\item Generally, we could use the formula \(W=|\vec{F}||\Delta\vec{d}|\) as the formula for the Joules unit tells us, so why do we need to also consider the Cosine of the angle?
\item Well, if we draw these Vectors, we'll see the following:
\end{itemize}

\begin{tikzpicture}
\draw[->] (0,0) coordinate (B) -- (2,2) coordinate (A) node[midway,sloped,above] {\(\vec{F_{a}}=\qty{50.0}{\N}\)};
\draw[->] (0,0) -- (2,0) coordinate (C) node[midway,below] {\(\Delta\vec{d}=\qty{3.00}{\m}\)};
\draw pic["\(30\degree\)", draw, <->, angle eccentricity=1.5, angle radius=0.75cm]
    {angle=C--B--A};
\end{tikzpicture}

\begin{itemize}
\item Now, in terms of the work being done, we're only concerned on the Force we're applying that actually causes displacement
\item That means anything that isn't an x-component of \(\vec{F_{a}}\) does not interest us in this case
\item So, let's try and find \(\vec{F_{ax}}\):
\end{itemize}

\begin{tikzpicture}
\draw[->] (0,0) coordinate (B) -- (2,2) coordinate (A) node[midway,sloped,above] {\(\vec{F_{a}}=\qty{50.0}{\N}\)};
\draw[->,dashed] (0,0) -- (2,0) coordinate (C) node[midway,below] {\(\vec{F_{ax}}\)};
\draw[->,dashed] (2,0) -- (2,2) node[midway,right] {\(\vec{F_{ay}}\)};
\draw pic["\(30\degree\)", draw, <->, angle eccentricity=1.5, angle radius=0.75cm]
    {angle=C--B--A};
\end{tikzpicture}

\begin{itemize}
\item This means that \(\vec{F_{ax}}=\cos(30\degree)\vec{F_{a}}\)
\item And so, this finally means \(W=|\vec{F_{ax}}||\Delta\vec{d}| \lor W=|\vec{F_{a}}|\cos(30\degree)|\Delta\vec{d}|\)
\item So the Cosine in the equation comes from us getting just the x-component of the Applied Force when it isn't parallel to the displacement Vector
\end{itemize}
\section*{Kinetic and Potential Energy}
\label{sec:orgfc798a5}
\subsection*{Kinetic Energy}
\label{sec:orgc1010d6}
\begin{itemize}
\item Kinetic Energy is the Energy an object possesses because of it's \emph{motion}
\item We know that \(W=|\vec{F}||\Delta\vec{d}|\) quite simply, but since \(\vec{F}=m\vec{a}\), we can also write this as \(W=m\vec{a}\Delta\vec{d}\)
\item The Kinematic equation \(\vec{v_{f}}^{2}=\vec{v_{i}}^{2}+2\vec{a}\Delta\vec{d}\) might come in handy here because it can be rearranged into \(\frac{\vec{v_{f}}^{2}-\vec{v_{i}}^{2}}{2}=\vec{a}\Delta\vec{d}\)
\item This can then be integrated back into the Work equation to get \(W=\frac{m\left(\vec{v_{f}}^{2}-\vec{v_{i}}^{2}\right)}{2}\)
\begin{itemize}
\item This can then be written in the form \(W=\frac{m}{2}\vec{v_{f}}^{2}-\frac{m}{2}\vec{v_{i}}^{2}\)
\item The terms \(\frac{m}{2}\vec{v}^{2}\) are each equal to the Kinetic Energy that an object possesses, so we could say \(W=KE_{f}-KE_{i} \lor W=\Delta KE\)
\end{itemize}
\end{itemize}
\subsection*{Potential Energy}
\label{sec:org6ec6155}
\begin{itemize}
\item Potential Energy is the Energy that an object possess simply because of it's \emph{position} in a field
\begin{itemize}
\item So for instance, by increasing the height of some object by holding it, you're increasing it's Potential Energy in the Gravitational Field of the Earth
\item When you drop the object, all this Potential Energy gets converted into Kinetic Energy
\end{itemize}
\item Since for most situations, Potential Energy simply takes into account an object's height, we can make the formula \(PE=|\vec{F_{g}}| h \lor PE_{g}=m|\vec{g}| h\) out of our original \(W=|\vec{F}||\Delta\vec{d}|\)
\end{itemize}
\section*{Types of Energy}
\label{sec:orgd3b25ac}
\subsection*{Mechanical Energy}
\label{sec:orgf3e7a29}
\begin{itemize}
\item This is a form of energy that can \emph{either} be Potential or Kinetic
\item Mechanical Energy is possessed by objects that are affected by the forces of gravity and friction
\end{itemize}
\subsection*{Gravitational Energy}
\label{sec:org60fab2f}
\begin{itemize}
\item Gravitational Energy can \emph{only} be Potential
\item This is energy possessed by objects affected by the force of Gravity
\end{itemize}
\subsection*{Radiant Energy}
\label{sec:orgfe28863}
\begin{itemize}
\item This is a form of energy that can \emph{either} be Potential or Kinetic
\item This is energy that is possessed by oscillating electric and magnetic fields
\item This includes things like light and electromagnetic radiation
\end{itemize}
\subsection*{Electrical Energy}
\label{sec:org0400b6d}
\subsubsection*{Static Electricity}
\label{sec:orgd69db9e}
\begin{itemize}
\item Static Electricity can \emph{only} be Potential
\item This is energy possessed by static charges that have gathered over time upon an object
\end{itemize}
\subsubsection*{Current Electricity}
\label{sec:org4fd3d91}
\begin{itemize}
\item This is a form of energy that can \emph{either} be Potential or Kinetic
\item This is energy possessed by flowing charges
\end{itemize}
\subsection*{Thermal Energy}
\label{sec:org51e8583}
\begin{itemize}
\item This is a form of energy that can \emph{either} be Potential or Kinetic
\item This is energy possessed by randomly moving atoms and molecules
\end{itemize}
\subsection*{Sound Energy}
\label{sec:orgaca53fb}
\begin{itemize}
\item This is a form of energy that can \emph{either} be Potential or Kinetic
\item This is energy possessed by large groups of oscillating atoms and molecules
\end{itemize}
\section*{Conservation of Energy}
\label{sec:org68f0c62}
\begin{itemize}
\item In a Closed System, total Energy is conserved
\begin{itemize}
\item As an object falls, it's Potential Energy gets converted gradually into Kinetic Energy but none is ever lost so the sum of the two types of energy remains constant at any given point
\end{itemize}
\item This sum while an object is in free-fall is called the \textbf{Mechanical Energy}; \(ME=KE+PE\)
\begin{itemize}
\item This also means \(ME=\frac{m}{2}\vec{v}^{2}+m\vec{g}h\)
\end{itemize}
\item Because Mechanical Energy is conserved, \(\frac{m}{2}\vec{v_{i}}^{2}+m\vec{g}h_{i}=\frac{m}{2}\vec{v_{f}}^{2}+m\vec{g}h_{f}\)
\item However, it's important to assume that these systems do not have to deal with Friction
\end{itemize}
\section*{Power}
\label{sec:org7aa5a1a}
\begin{itemize}
\item Power is the measurement of average Work over an amount of time
\item Power is measured in \textbf{Watts}; \(\unit{\W}=\unit{\J\per\s}\) meaning that if we expand the Joules unit, we can see that since \(\unit{\J}=\unit{\kg\m\squared\per\s\squared}\), \(\unit{W}=\unit{\kg\m\squared\per\s\cubed}\)
\item Because \(P_{avg}=\frac{W}{t}\) where \(t\) is some unit of time and \(W\) is some unit of Work, we could also say \(P_{avg}=\frac{\vec{F}\Delta\vec{d}}{t} \lor P_{avg}=\vec{F}\cdot\vec{v_{avg}}\)
\end{itemize}
\subsection*{Efficiency}
\label{sec:org3a7543d}
\begin{itemize}
\item Efficiency is the ratio of the amount of useful energy that is output from Work to the amount that is input in total
\item The formula for calculating Efficiency is \(Eff=\frac{E_{out}}{E_{in}}\cdot100\%\)
\end{itemize}
\section*{Thermal Energy}
\label{sec:orgf13479a}
\begin{itemize}
\item Naturally, heat passively that already exists in an area will spread over a larger surface area in a way similar to how fluids transfer
\item The primary force that heat utilizes in order to transfer is a difference in temperature; without temperature differences, heat simply cannot be transferred
\item Heat will \emph{always} move from a place of higher temperature to a place of lower temperature unless things such as an engine are employed
\item Nonconductors are materials that reduce the flow of heat, thus keeping heat in one place to some variety of effectiveness
\end{itemize}
\subsection*{Conduction}
\label{sec:org020ae7a}
\begin{itemize}
\item Conduction is the process of heat energy being transferred across surfaces through the \textbf{Collision of Molecules or other Particles}
\item When something hot touches something cold, the particles of the hotter object will be moving faster and will collide a lot with the particles of the colder object, thus transferring heat
\item Some materials have a higher capability to conduct heat, and some have more of a resistance to heat conductivity
\item Typically, metals have high thermal conductivity while insulating materials such as mineral wool and Styrofoam have low thermal conductivity
\begin{itemize}
\item A good way to preserve the temperature in one place, then, is to surround it with insulators
\end{itemize}
\item \textbf{Thermal Resistance} defines how good a material is at insulating; the higher the Thermal Resistance, the lower the Thermal Conductivity of the material
\item To find Thermal Resistance, use \(R=\frac{L}{kA}\) where \(L\) is the thickness of the layer of material, \(k\) is it's thermal conductivity, and \(A\) is the area it takes up
\item Of course, by definition, \(R\propto k^{-1}\) then, but also \(R\propto A^{-1}\), and so the more area a material is stretched across, the less effective it is at Insulating, while \(R\propto L\)
\item Another way to define Thermal Resistance is \(R=\frac{\Delta Ts}{q} \lor R=\frac{Ts_{2}-Ts_{1}}{q}\)
\begin{itemize}
\item Here, \(\Delta Ts\) is the difference in Temperature, and \(q\) is the rate of heat transfer, also known as \textbf{heat flux}
\item This also of course means that \(R\propto q^{-1}\) which makes sense; the lower the rate of heat transfer, the higher the thermal resistance
\end{itemize}
\item Thermal Resistance is \textbf{additive}; the thermal resistance of several materials placed next to each other can simply be added together for the net Thermal Resistance; \(R_{net}=\sum_{n=1}^{a}R_{n}\) where \(a\) is the number of materials
\end{itemize}
\subsection*{Convection}
\label{sec:org9aebec7}
\begin{itemize}
\item Convection is the transfer of Energy through the \textbf{physical movement} of a fluid
\item As a fluid interacts with and moves across a surface, it can change the heat of the surface
\item For instance, hot water in a glass has a lower density, and so moves to the top, taking the thermal energy with it, and it's replaced by cooler water
\item You can measure how much heat is being transferred through means of Convection using the \textbf{Convective Heat Transfer Coefficient} \((h)\)
\begin{itemize}
\item \(h=\frac{k}{\delta}\) where \(k\) is once again the thermal conductivity of the fluid, and \(\delta\) is the thickness of the boundary layer with the surface it's transferring heat to
\end{itemize}
\end{itemize}
\subsection*{Radiation}
\label{sec:org5ebbb6a}
\begin{itemize}
\item Radiation is the transfer of Energy in the form of \textbf{Electromagnetic Waves}
\item This is the main form of long-distance heat transfer; it's how heat from the Sun and heat from fires transfers to other objects
\item Reflective Surfaces generally absorb less Electromagnetic Waves, and so feel the effect of heat Radiation less intensely
\end{itemize}
\section*{Principle of Heat Exchange}
\label{sec:org899c6ac}
\begin{itemize}
\item The Principle of Heat Exchange says that the amount of thermal energy transferred from the warmer object will be equal to the amount of energy absorbed by the cooler object
\begin{itemize}
\item \(Q_{Released}+Q_{Absorbed}=0\)
\end{itemize}
\item Different substances absorb heat at different heats, and the \textbf{Specific Heat Capacity} of a substance is the amount of energy required to raise \(\qty{1}{\g}\) of it's temperature by \(\qty{1}{\celsius}\)
\item The amount of heat exchanged in a reaction is \(Q=mc\Delta T\) where \(c\) is the Specific Heat Capacity of the substance, \(\Delta T\) is the difference in temperature, and \(m\) is mass
\begin{itemize}
\item To be exact, \(Q\) is called the \textbf{Quantity of Heat} and is the total amount of thermal energy transferred from warmer to colder substances
\item Specific Heat Capacity has a unit of \(\unit{\J\per\g\celsius} \lor \unit{\J\per\g\K}\)
\end{itemize}
\item \textbf{Calorimetry} is the study of the measure of a change in heat
\item The Specific Heat Capacity of Water is about \(c_{H_{2}O}=\qty{4.186}{\J\per\g\celsius}\)
\item Generally, Specific Heat Capacity of a substance is a way you can identify it, and if you cannot derive it with this formula, it will likely be given to you
\item For a rate of Heat transfer over time, use \(\frac{Q}{\Delta t}=\frac{kA\Delta T}{I}\) where \(k\) is the thermal conductivity, \(A\) is the cross-sectional area, and \(I\) is the distance between the two points
\end{itemize}
\subsection*{Heat vs. Temperature}
\label{sec:orgfe3e07b}
\begin{itemize}
\item Temperature is a measure of \textbf{heat energy} available for work in a system
\begin{itemize}
\item This will be the average Kinetic Energy of the system
\end{itemize}
\item \(T\propto \vec{v}\) for Particles
\item Heat, however, is a measure of the speed at which Particles are moving and the Work they do on other Particles when heat transfer occurs
\item Heat is measured in Joules while Temperature is transferred in degrees Celsius or in Kelvin
\item So, in simpler terms, Temperature measures the average kinetic energy of particles of a substance, while Heat measures the rate of transfer for thermal energy in a system
\end{itemize}
\subsection*{The Kelvin Scale}
\label{sec:orge17d2fb}
\begin{itemize}
\item Kelvin is the Standard International (SI) unit for measuring Temperature
\item The only difference between Kelvin and Celsius, really, is that while \(\qty{0}{\celsius}\) is set at the freezing point of Water, \(\qty{0}{\K}\) is set at the value at which Particles stop moving absolutely
\item It turns out that Particles stop moving absolutely at \(\qty{-273}{\celsius}\), and so \(\qty{0}{\K}=\qty{-273}{\celsius} \land \qty{0}{\celsius}=\qty{273}{\K}\)
\end{itemize}
\subsection*{Phase Changes}
\label{sec:org55fcf92}
\begin{itemize}
\item While some substance's state of matter is changing, temperature remains constant until the state \emph{completely} changes
\item For instance, as ice turns to water, it will stay at \(\qty{0}{\celsius}\) until it has all fully become water
\item During these phase changes, the formula \(Q=mc\Delta T\) no longer works as there \emph{is} no change in temperature
\item So, we use \(Q=mL\) during these times instead; here, \(L\) is the \textbf{Latent Heat} of the substance which again, has to be given to you
\item There is Fusion and Vaporization Latent Heat of substances; Fusion is for a phase change between liquid and solid, and Vaporization is for between Liquid and Gas
\item Each of these describe how much thermal energy is required for \(\qty{1}{\g}\) of a substance to change state
\end{itemize}
\subsection*{Example 1}
\label{sec:org94e15ec}
A metal bar with a mass of \(\qty{4.0}{\kg}\) is placed in boiling water till it reaches \(\qty{100}{\celsius}\); there is \(\qty{500.0}{\mL}\) of water with an initial temperature of \(\qty{20.0}{\celsius}\) and a final temperature of \(\qty{35.0}{\celsius}\). Find \(c\) for the metal bar

\begin{itemize}
\item For this problem, we need to understand the equation \(Q_{released}=-Q_{absorbed}\)
\begin{itemize}
\item This also can be written as \(Q_{hot}=-Q_{cold}\) or just \(mc\Delta T=-mc\Delta T\) when the substance in the left and right are part of the same energy system
\end{itemize}
\item This means that since we can find \(Q_{H_{2}O}\), we can just make it negative and use it as the \(Q\) value for the metal
\item Remember that \(\qty{1}{\mL}=\qty{1}{\g}\)
\end{itemize}

\begin{align*}
Q_{H_{2}O}&=mc\Delta T \\
&=(\qty{500}{\cancel\g})\left(\qty{4.186}{\J\per\cancel\g\celsius}\right)\left(\qty{20}{\celsius}-\qty{35}{\celsius}\right) \\
&=\left(\qty{2093}{\J\per\cancel\celsius}\right)\left(\qty{-15}{\cancel\celsius}\right) \\
&=\qty{-31395}{\J} \\
&=-Q_{metal} \\
\qty{31395}{\J}&=(\qty{4000}{\g})c\left(\qty{100}{\celsius}-\qty{35}{\celsius}\right) \\
\frac{\qty{31395}{\J}}{(\qty{4000}{\g})\left(\qty{65}{\celsius}\right)}&=c \\
\qty{0.1}{\J\per\g\celsius}&\approx c
\end{align*}
\section*{Heating and Cooling Graphs}
\label{sec:orgd61f487}
\begin{itemize}
\item You can describe how the temperature of an object changes as thermal energy is absorbed or released
\item Your y-axis will show the temperature at a given moment, and the x-axis will either show the thermal energy absorbed or released
\item At melting and boiling points, the temperature will remain at a constant slope of 0 for some time as the phase change occurs
\end{itemize}
\section*{Radiation and Nuclear Equations}
\label{sec:orgcfe46d1}
\begin{itemize}
\item The three types of Radioactive Decay are Alpha, Beta, and Gamma Decay
\begin{itemize}
\item \emph{Decay} means that the radiation is produced on the Reactant's side of the reaction
\end{itemize}
\item To describe Decay, you'll want to recall \textbf{Isotopic Notation}: \(_{\text{Atomic Number}}^{\text{Mass Number}}\text{Element Symbol}\); so for instance \(\ce{_{2}^{4}He}\) Since Helium's Atomic Number is 2 and it's Average Atomic Mass is 4
\end{itemize}
\subsection*{Alpha Decay}
\label{sec:orga57e993}
\begin{itemize}
\item This occurs when a Nucleus emits an \(\alpha-\text{Particle}\)
\item An Alpha Particle contains clusters of Protons and Neutrons, and so it occurs when an Atom has too many Protons \emph{and} too many Neutrons
\item After Alpha Decay, 4 is subtracted from the Mass Number of the element, and 2 is subtracted from the Atomic Number
\begin{itemize}
\item So 2 Protons and 2 Neutrons are lost making the Mass Number decrease by 4
\item At the same time, since 2 Protons are lost, the Atomic Number also decreases by 2, changing the Identity of the Atom entirely; if the element Scandium underwent Alpha Decay, it would turn into Potassium
\end{itemize}
\item Alpha Particles are quite large; they cannot travel far without losing energy and they can be blocked by most substances since they have a very low penetration power
\end{itemize}
\subsection*{Beta Decay}
\label{sec:orga8c41b8}
\begin{itemize}
\item Here, atoms emits \(\beta^{\pm}-\text{Particles}\)
\item The Mass of Beta Particles is considered so small that it's essentially negligible, so what's accounted for is mainly just the charge
\item This makes Beta Particles essentially just Electrons with high energy
\item The way you want to write Beta Particles is \(\ce{_{\pm1}^{0}\beta}\) where the charge goes at the bottom, the mass at the top, and the element symbol beside
\item Beta Decay occurs when an Atom has \emph{either} too many Protons \emph{or} too many Neutrons
\item Beta Particles have higher penetration power than Alpha Particles but they can still be blocked by things such as Clothing and Aluminum
\item However, because Beta Particles are smaller than Alpha Particles, their Ionizing Power is also smaller than that of Alpha Particles
\begin{itemize}
\item So, since the particle is smaller, it's less likely to collide with the important parts of other Atoms and cause damage
\end{itemize}
\end{itemize}
\subsubsection*{Beta-Plus Decay/Positron Emission}
\label{sec:org13b9b39}
\begin{itemize}
\item \(\beta^{+}\) Decay, also known as Positron Emission, occurs when a Proton turns into a Neutron with the release of a Beta-Plus Particle (called a \textbf{Positron}) alongside an uncharged and nearly massless particle called a \textbf{Neutrino}
\item Because of this, the resultant Atom will have one less Proton and one more Neutron
\item However, the mass number \emph{does not} change
\end{itemize}
\subsubsection*{Beta-Minus Decay/Beta Decay}
\label{sec:orgd20942c}
\begin{itemize}
\item \(\beta^{-}\) Decay, also simply referred to as Beta Decay, is now the opposite of Beta-Plus Decay; a Neutron turns into a Proton which releases a Beta-Minus particle (simply an Electron) along with an \textbf{Antineutrino}
\item Because of this, the resultant Atom will have one less Neutron and one more Proton
\item However, the mass number \emph{does not change}
\item This type of Beta Decay is \emph{far} more common than Beta-Plus Decay
\end{itemize}
\subsection*{Gamma Decay}
\label{sec:org91ce2b8}
\begin{itemize}
\item Gamma Decay is a process where \textbf{Gamma Ray Photons} \(\left(\gamma-\text{Rays}\right)\) are ejected, but these, unlike the previous two types of Decay, are not Particles
\begin{itemize}
\item Gamma Rays are \emph{Waves} with incredibly small Wavelengths
\end{itemize}
\item You're basically fucked if a \emph{lot} of Gamma Rays come your way; they have an incredibly high penetration power and it takes things such as Lead slabs or Concrete with a high thickness to block them
\item Gamma Rays are a part of the Electromagnetic Spectrum just like visible light
\begin{itemize}
\item In terms of the Electromagnetic Spectrum, Radio Waves are the largest Waves and Gamma Rays are the smallest; smaller than X-rays and Ultraviolet Rays
\end{itemize}
\item Gamma Rays do not change the structure or composition of the atom from which they originate
\end{itemize}
\subsection*{Nuclear Equations}
\label{sec:orga28b3cd}
\begin{itemize}
\item Using Isotope Notation, Nuclear Equations shows how the Nucleus of a radioactive Isotope breaks down over time into Isotopes containing less mass as well as different radioactive particles associated with the decay
\item For instance, for Alpha Decay of \(\ce{_{94}^{242}Pu}\), we know that a Particle of 2 Neutrons and 2 Protons will be expelled meaning we get \(\ce{_{94}^{242}Pu -> _{2}^{4}He + _{92}^{238}U}\)
\item For Beta-Plus Decay of \(\ce{_{8}^{15}O}\), we get \(\ce{_{8}^{15}O -> _{7}^{15}N + _{1}^{0}e}\); notice that the sum of the mass and atomic numbers of the products always add up to the reactant, which here, means a Positron gets expelled
\item For Beta-Minus Decay of \(\ce{_{15}^{32}P}\), we get \(\ce{_{15}^{32}P -> _{16}^{32}S + _{-1}^{0}e}\) where an Electron gets expelled
\end{itemize}
\subsection*{Isotopes of Hydrogen}
\label{sec:org8784500}
\begin{enumerate}
\item \(\ce{_{1}^{1}H}\) is of course called Hydrogen but it's also called \textbf{Protium}
\item \(\ce{_{1}^{2}H}\) is called \textbf{Deuterium}
\item \(\ce{_{1}^{3}H}\) is called \textbf{Tritium}
\end{enumerate}
\section*{Half Life}
\label{sec:org0e50f03}
\subsection*{Example 1}
\label{sec:orgc062d9d}
Iodine-131 has a half life of 8 days starting with 200 grams. How much will remain after 32 days?

\begin{itemize}
\item For these kinds of problems, we want to model them as \(A=A_{0}\left(\frac{1}{2}\right)^{\frac{t_{1}}{t_{2}}}\)
\begin{itemize}
\item Here, \(A_{0}\) is your starting quantity, \(t_{1}\) is the amount of time progressed, and \(t_{2}\) is the period of time of a half-life
\end{itemize}
\item So for this problem we would do \(A=(\qty{200}{\g})\left(\frac{1}{2}\right)^{\frac{\qty{32}{\day}}{\qty{8}{\day}}}\), meaning \(A=\qty{12.5}{\g}\)
\item Just remember that for this formula, while the units of time do not really matter, \(t_{1}\) and \(t_{2}\) must \textbf{always be in the same unit}
\end{itemize}
\subsection*{Example 2}
\label{sec:orga9f72ab}
Sodium-24 has a half life of 15 hours, starting with 800 grams. How long will it take for 750 grams to decay?

\begin{itemize}
\item Here, we know our initial amount, we know the total period of the half life, and we know how much we will be left with in order to solve for our time
\item This means we can model this as \(\qty{800}{\g}-\qty{750}{\g}=(\qty{800}{\g})\left(\frac{1}{2}\right)^{\frac{t_{1}}{\qty{15}{\hour}}}\)
\item So now we isolate for the exponent on the right side to get \(\frac{1}{16}=\frac{1}{2}^{\frac{t_{1}}{\qty{15}{\hour}}}\)
\item Now we just use Logarithms: \(\log_{\frac{1}{2}}\left(\frac{1}{16}\right)=\frac{t_{1}}{\qty{15}{\hour}}\) which means \(15\cdot\log_{\frac{1}{2}}\left(\frac{1}{16}\right)=t_{1}=\qty{60}{\hour}\)
\item The way you would evaluate this in a calculator is \(\left(\frac{\ln\left(\frac{1}{16}\right)}{\ln\left(\frac{1}{2}\right)}\right)\cdot15=t_{1}=\qty{60}{\hour}\) according to the Change-of-base Formula
\end{itemize}
\subsection*{Example 3}
\label{sec:org19622d4}
Oxygen-15 has a half life of 2 minutes. What fraction of some Oxygen-15 will remain after 5 half lives?

\begin{itemize}
\item We're not given a starting quantity, but we don't need to be given one
\item To model this equation, we would have \(A=A_{0}\left(\frac{1}{2}\right)^{\frac{\qty{10}{\min}}{\qty{2}{\min}}}\) which, when we simplify, we get \(A=A_{0}\left(\frac{1}{32}\right)\), and so \(\frac{1}{32}\) of the original quantity will be left
\end{itemize}
\subsection*{Example 4}
\label{sec:orgc7c53fc}
It takes 35 days for 512 grams of some element to decay to 4 grams. What was the half life?

\begin{itemize}
\item Here, we can set up our equation as \(\qty{4}{\g}=(\qty{512}{\g})\left(\frac{1}{2}\right)^{\frac{\qty{35}{\day}}{t_{2}}}\)
\item That means we can rearrange this as \(\frac{1}{128}=\frac{1}{2}^{\frac{\qty{35}{\day}}{t_{2}}}\), and so \(\log_{\frac{1}{2}}\left(\frac{1}{128}\right)=\frac{\qty{35}{\day}}{t_{2}}\) meaning \(t_{2}=\frac{35}{\log_{\frac{1}{2}}\left(\frac{1}{128}\right)}=\qty{5}{\day}\)
\end{itemize}
\section*{Mass-Energy Equivalence}
\label{sec:orgbb1d097}
\begin{itemize}
\item The Mass-Energy Equivalence defines the equation \(E=mc^{2}\) where \(E\) is the energy of an object while at rest, \(m\) is it's mass, and \(c\) is the speed of light \(\left(c\approx\qty{3e8}{\m\per\s}\right)\)
\begin{itemize}
\item This equation also means that \(E\propto m\), which brings us to the Law of Conservation of Mass-Energy
\end{itemize}
\item This law extends what the Law of Conservation of Energy says to also include the fact that mass and energy can freely transfer into becoming one another without a loss in mass \emph{or} energy when in an enclosed system
\item This Law also explains the \textbf{mass defect} seen in Nuclei sometimes; the actual mass of a Nucleus may be smaller than if we were to add the masses of the Protons, Neutrons, and Electrons that are supposed to compose the atom
\begin{itemize}
\item What's happening here is that a small portion of the mass has converted into \textbf{binding energy} which is energy used to keep the composition of the Atom
\end{itemize}
\item At such scales as the Atom, Kilograms and Joules are not practical units to use anymore; \textbf{Atomic Mass Units} \((\unit{\atomicmassunit})\) and \textbf{Mega Electron Volts} \((\unit{\MeV})\) are used instead
\begin{itemize}
\item \(\qty{1}{\atomicmassunit}\approx\qty{1.66e-27}{\kg} \land \qty{1}{\MeV}\approx\qty{1.602e-13}{\J}\)
\end{itemize}
\end{itemize}
\subsection*{Example 1}
\label{sec:orga56aaea}
The actual recorded mass of a Helium-4 atom is \(\qty{4.002603}{\atomicmassunit}\); if the theoretical mass of the atom should be \(\qty{4.03298}{\atomicmassunit}\) find the mass defect and Binding Energy of the atom

\begin{itemize}
\item First, in order to find the mass defect \((\Delta m)\) we simply find the difference between the theoretical and actual mass: \(\Delta m=\qty{4.03298}{\atomicmassunit}-\qty{4.002603}{\atomicmassunit}=\qty{0.030347}{\atomicmassunit}\)
\item Now, in order to use it in our \(E=mc^{2}\) formula, we must convert it into Kilograms which we can do by multiplying it with \(\frac{\qty{1.66e-27}{\kg}}{\unit{\atomicmassunit}}\) which results in \(\qty{5.037602e-29}{\kg}\)
\item Now, we simply plug it into our formula: \(E=\left(\qty{5.037602e-29}{\kg}\right)\left(\qty{3e8}{\m\per\s}\right)^{2}=\qty{4.5338418e-12}{\J}\)
\item Finally, we will convert this into Mega Electron Volts by multiplying it with \(\frac{\qty{1}{\MeV}}{\qty{1.602e-13}{\J}}\) to get about \(\qty{28}{\MeV}\)
\end{itemize}
\section*{Nuclear Fission and Fusion}
\label{sec:org9cb1f37}
\subsection*{Fission}
\label{sec:orga496029}
\begin{itemize}
\item Fission occurs when a Neutron collides with an atom of an Isotope that has nuclear instability
\begin{itemize}
\item Such atoms are often called \textbf{Fissionable}
\end{itemize}
\item For example, Uranium-235 can undergo Fission in the reaction \(\ce{_{92}^{235}U + _{0}^{1}n^{0} -> _{36}^{92}Kr + _{56}^{141}Ba + 3(_{0}^{1}n^{0}) + energy}\)
\item During such reactions, the Binding Energy of the reactant Atom is released making Fission a very ideal source of energy
\item As seen in the equation above, Neutrons are also produced by Fission reactions and these Neutrons can themselves be used to perform \emph{more} Fission reactions in a \emph{Chain Reaction}
\begin{itemize}
\item The amount of the Reactant substance needed for a Chain Reaction is called the \textbf{Critical Mass}
\end{itemize}
\item However, it's important to note that produced Neutrons often have too much energy to be absorbed by another Nucleus, and so they must undergo moderation
\begin{itemize}
\item This often involves the Neutrons being surrounded by \emph{Heavy Water} which utilizes Deuterium
\end{itemize}
\item It's possible to calculate the amount of energy produced in a Fission reaction
\end{itemize}
\subsubsection*{Example 1}
\label{sec:org26306c6}
Find the amount of energy released in the reaction \(\ce{_{92}^{235}U + _{0}^{1}n^{0} -> _{55}^{140}Cs + _{37}^{93}Rb + 3(_{0}^{1}n^{0})}\) if \(m_{\ce{U}}=\qty{235.044}{\atomicmassunit},\ m_{\ce{Cs}}=\qty{139.909}{\atomicmassunit}\ m_{\ce{Rb}}=\qty{92.922}{\atomicmassunit},\ m_{\ce{n^{0}}}=\qty{1.009}{\atomicmassunit}\)

\begin{itemize}
\item As we established, the amount of Energy produced here is equal to the Binding Energy of the reactant Uranium-235
\item To find the Binding Energy of the U-235, we simply find the difference between it's mass and the mass of the products; this will be our mass defect
\item From there it's just a matter of using \(E=mc^{2}\):
\item As for the Neutron in the reactant side, it cancels out with a Neutron in the products side to just leave us with 2 Product Neutrons
\end{itemize}

\begin{align*}
\Delta m&=m_{\ce{U}}-\left(m_{\ce{Cs}}+m_{\ce{Rb}}+2m_{\ce{n^{0}}}\right) \\
&=\qty{235.044}{\atomicmassunit}-(\qty{139.909}{\atomicmassunit}+\qty{92.922}{\atomicmassunit}+\qty{2.018}{\atomicmassunit}) \\
&=\qty{0.195}{\atomicmassunit} \\
&\approx\qty{3.237e-28}{\kg} \\
E&=\left(\qty{3.237e-28}{\kg}\right)\left(\qty{3e8}{\m\per\s}\right)^{2} \\
&\approx\qty{2.913e-11}{\J} \\
&\approx\qty{181.8}{\MeV}
\end{align*}
\subsection*{Fusion}
\label{sec:orgc028b0e}
\begin{itemize}
\item Fusion is now the inverse reaction of Fission; Fusion is the merging of two Nuclei into a new Element
\item Fusion can only occur if the Nuclei have enough Kinetic Energy to overpower the repulsive Electrostatic Forces the Nuclei have for one another
\item Generally, the higher the Binding Energy of a Nucleus, the more ``stable'' it can be considered
\begin{itemize}
\item This means that \(\ce{_{26}^{56}Fe}\) is actually the most stable Nucleus
\end{itemize}
\item Approaching Iron-56 from smaller Mass Numbers, Atoms have a large increase in their quantity of Binding Energy per Nucleon
\begin{itemize}
\item After Iron-56, as Mass Number increases further, Binding Energy per Nucleon sees a gradual decrease
\end{itemize}
\item Fission is more likely to occur with very heavy Nuclei while Fusion is more likely to occur with very light Nuclei
\end{itemize}
\end{document}
