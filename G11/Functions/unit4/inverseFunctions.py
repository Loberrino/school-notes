#!/usr/bin/env python3
import math
import sys
import os

def arcsin(x):
    return round(math.degrees(math.asin(x)),1)

def arccos(x):
    return round(math.degrees(math.acos(x)),1)

def arctan(x):
    return round(math.degrees(math.atan(x)),1)

def sin(x):
    return round(math.sin(math.radians(x)),1)

def cos(x):
    return round(math.cos(math.radians(x)),1)

def tan(x):
    return round(math.tan(math.radians(x)),1)

def iterator(ratio, iterationCountPos, iterationCountNeg, expressions):
    print("Here are the angles that would produce this ratio:")
    if ratio > 0:
        for n in range(iterationCountPos):
            print(360*n,"to",360*(n+1),":", expressions[0]+(360*n), "∧", expressions[1]+(360*n))
        for n in range(iterationCountNeg):
            n=n+1
            print(-360*(n-1),"to",-360*n,":", expressions[0]-(360*n), "∧", expressions[1]-(360*n))
    else:
        for n in range(iterationCountPos):
            print(360*n,"to",360*(n+1),":", expressions[2]+(360*n), "∧", expressions[3]+(360*n))
        for n in range(iterationCountNeg):
            n=n+1
            print(-360*(n-1),"to",-360*n,":", expressions[2]-(360*n), "∧", expressions[3]-(360*n))

angleDictionary = {
    "sin" : lambda ratio : [arcsin(ratio), 180-arcsin(ratio), arcsin(ratio*-1)+180, 360-arcsin(ratio*-1)],
    "cos" : lambda ratio : [arccos(ratio), 360-arccos(ratio), 180-arccos(ratio*-1), 180+arccos(ratio*-1)],
    "tan" : lambda ratio : [arctan(ratio), 180+arctan(ratio), 180-arctan(ratio*-1), 360-arctan(ratio*-1)]
}

try:
    while True:
        function = str(input("Would you like to work with Sine, Cosine, or Tangent?: ")).lower()[:3]
        while function == "sin" or function == "cos" or function == "tan":
            inverseCheck = str(input("Would you like to work with the inverse of this function? [Y/N]: ")).lower()[:1]
            if inverseCheck == "y":
                inverseCheck = True
                break
            elif inverseCheck == "n":
                inverseCheck = False
                break
            print("Please enter either 'Yes' or 'No'")
        else:
            print("Input not understood; please try typing 'Sine', 'Cosine', or 'Tangent'")
            continue
        break

    while inverseCheck == True:
        try:
            ratio = float(input("What Ratio would you like to use as your input?: "))
            if (ratio > 1 or ratio < -1) and (function == "sin" or function == "cos"):
                raise ValueError
            print("There are an infinite number of angles that give this Ratio, so we need to establish a range within which angles will be given.")
            iterationCountPos = int(input("Please give the number of iterations past a full 360 degrees you'd like this program to account for: "))
            iterationCountNeg = int(input("As well, please give the number of iterations below 0 degrees you'd like this program to account for: "))
            if (iterationCountPos < 0 or iterationCountNeg < 0) or (iterationCountPos == 0 and iterationCountNeg == 0):
                raise ValueError
            iterator(ratio, iterationCountPos, iterationCountNeg, angleDictionary[function](ratio))
            break
        except ValueError:
            print("Please enter a valid number.")

    while inverseCheck == False:
        try:
            angle = float(input("What angle would you like to use as your input?: "))
            if (function == "tan") and ((angle % 90) == 0 and (angle/90) % 2 != 0):
                raise ValueError
            print("The Ratio this angle produces can be achieved by an infinite number of angles, so we need to establish a range within which angles will be given.")
            iterationCountPos = int(input("Please give the number of iterations past a full 360 degrees you'd like this program to account for: "))
            iterationCountNeg = int(input("As well, please give the number of iterations below 0 degrees you'd like this program to account for: "))
            if (iterationCountPos < 0 or iterationCountNeg < 0) or (iterationCountPos == 0 and iterationCountNeg == 0):
                raise ValueError
            if function == "sin":
                iterator(sin(angle), iterationCountPos, iterationCountNeg, angleDictionary[function](sin(angle)))
            elif function == "cos":
                iterator(cos(angle), iterationCountPos, iterationCountNeg, angleDictionary[function](cos(angle)))
            else:
                iterator(tan(angle), iterationCountPos, iterationCountNeg, angleDictionary[function](tan(angle)))
            break
        except ValueError:
            print("Please enter a valid number.")

except KeyboardInterrupt:
    try:
        sys.exit(130)
    except SystemExit:
        os._exit(130)
except EOFError:
    try:
        sys.exit(130)
    except SystemExit:
        os._exit(130)
