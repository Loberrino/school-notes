#+title: Radical Expressions, Quadratics, and Complex Numbers
#+author: Arad Fathalian
#+startup: latexpreview
#+latex_class: article
#+latex_class_options: [letterpaper]
#+options: num:nil toc:nil
#+latex_header: \everymath{\displaystyle}

* Radical Operations and Simplifying
+ A Radical is a term written as \(\sqrt[n]{x}\) wherein \(\{n\in\mathbb{R} \mid n\geq2 \land x\in\mathbb{R} \mid x\geq0\}\)
+ A /mixed/ Radical is a Radical, except it has a Coefficient that isn't \(\pm1\) (i.e. \(3\sqrt{2},-5\sqrt{3}\))

** Radical Rules (Totally Tubular, /Brah/)
\begin{align*}
\sqrt{x}\sqrt{y}&=\sqrt{xy}\ \{x\in\mathbb{R} \mid x\geq0 \land y\in\mathbb{R} \mid y\geq0\} \\
\frac{\sqrt{x}}{\sqrt{y}}&=\sqrt{\frac{x}{y}}\ \{x\in\mathbb{R} \mid x\geq0 \land y\in\mathbb{R} \mid y>0\}
\end{align*}

+ So basically, anything under a radical multiplied by anything /else/ under a radical is equal to both of those 2 things multiplied under a Radical *(provided the Radicals are to the same degree)*
+ Additionally, anything under a radical divided by anything /else/ under a radical is equal to both of those 2 things divided under a Radical *(provided the Radicals are to the same degree)*

** Simplifying Radicals
*** Example 1
\[\sqrt{72}\]

+ 72 is not a *Perfect Square* meaning it's Square Root is /not/ an Integer, so is there a simpler way to express this, maybe as a Mixed Radical?
+ Well, let's see if 72 is divisible by any of the Perfect Squares
+ 72 can be factored into 36 and 2! Perfect!
+ That means that another way to express this is as \(\sqrt{36}\sqrt{2}\)

\[=6\sqrt{2}\]

+ Keep in mind that, whenever you can, you want to try dividing your numbers by the largest Perfect Square possible when simplifying them
  + they work very similarly to simplifying fractions in this way

*** Example 2
\[5\sqrt{27}\]

+ Here, using the same method as last time, we can see that 27 is divisible by 9 so let's use that to get \(5\sqrt{9}\sqrt{3}\)
+ \(\sqrt{9}\) just simplifies to 3 which then multiplies with the coefficient of our mixed Radical to get us our final answer

\[=15\sqrt{3}\]

** Radical Operations
*** Example 1
\[-4\sqrt{6}\cdot2\sqrt{6}\]

+ Here we start seeing some similarities to terms with variables because we can multiply these exactly as we would if the Radicals were x's to get \(-8\sqrt{36}\)
+ This is also where we see a sort of rule come into play: \(\sqrt{x}\sqrt{x}=x\) (Multiplying the Square Root of a number by the Square Root of the same number results in the Radical cancelling out)

\[=-48\]

*** Example 2
\[\left(3\sqrt{2}+4\sqrt{5}\right)\left(4\sqrt{2}-3\sqrt{5}\right)\]

+ Here we have a Binomial being multiplied by a Binomial, meaning we have to use FOIL

\[=12\sqrt{4}-9\sqrt{10}+16\sqrt{10}-12\sqrt{25}\]

+ \(\sqrt{4}\) and \(\sqrt{25}\) get simplified into 2 and 5, of course, so we do the operations that this allows:

\[=24-9\sqrt{10}+16\sqrt{10}-60\]

+ Now let's get into /adding and subtracting/ Radical expressions
  + This works very much like with variables; if you have "like" Radicals, then you can add/subtract their coefficients
+ In this case, our "like" Radicals are \(\sqrt{10}\) (The degree of the Radical matters too, like with Variables)

\[=7\sqrt{10}-36\]

*** Example 3
\[\frac{16\sqrt{50}}{-5\sqrt{10}}\]

+ Here, we have 2 regular Monomials being divided and 2 Radicals being divided; we can do these divisions separately actually!

\begin{align*}
&=\frac{16}{-5}\sqrt{\frac{50}{10}} \\
&=\frac{16\sqrt{5}}{-5}
\end{align*}

* Rationalizing The Denominator
+ When we have Radical terms inside the Denominator, it is considered improper form
  + This is similar to having negative exponents inside of a final simplified expression
+ The way we fix this is by /Rationalizing the Denominator/ (multiplying the Numerator and Denominator by that very same Radical)

** Example 1
\[\frac{3}{\sqrt{6}}\]

+ So, like mentioned before, we have a rule that states \(\sqrt{x}\sqrt{x}=x\), of course because this is the same as saying \(\sqrt{x^{2}}\)
+ This rule can be applied here! We can simply multiply the Denominator here by \(\sqrt{6}\) so remove the Radical! However, since this is a Fraction, we must also multiply the Numerator

\[=\frac{3\sqrt{6}}{6}\]

+ However, since we now have a regular Monomial over another Monomial, we can simplify them into \(\frac{1}{2}\) and leave the Radical alone

\[=\frac{\sqrt{6}}{2}\]

** Example 2
\[\frac{3\sqrt{2}-5}{\sqrt{2}}\]

+ Same as last time, here we multiply both the numerator the denominator by \(\sqrt{2}\), making sure to distribute it into the numerator

\begin{align*}
&=\frac{\left(3\sqrt{2}-5\right)\sqrt{2}}{2} \\
&=\frac{3\sqrt{4}-5\sqrt{2}}{2} \\
&=\frac{6-5\sqrt{2}}{2}
\end{align*}

** Example 3
\[\frac{14}{3+\sqrt{2}}\]

+ Okay now our Denominator is a Binomial and this is where we start having some problems with our first method
+ Let's try multiplying both the Numerator and Denominator by \(\sqrt{2}\) just to demonstrate what I mean:

\[\frac{14\sqrt{2}}{3\sqrt{2}+2}\]

+ Okay well now we still have our problem; there's a Radical in the Denominator... What do we do?
+ Well, we use a *Conjugate*
  + Recall that Conjugates are expressions with the exact same terms, however with opposite \pm signs
+ Conjugates always produce a Difference of Squares; a degree 2 term subtracted from another degree 2 term
  + If we have a degree 2 term as the end product, our Radical cancels out with it, so let's try that

\begin{align*}
&=\frac{14\left(3-\sqrt{2}\right)}{\left(3+\sqrt{2}\right)\left(3-\sqrt{2}\right)} \\
&=\frac{42-14\sqrt{2}}{9-2} \\
&=\frac{42-14\sqrt{2}}{7}
\end{align*}

+ Now, a common factor on the numerator is 7 giving us:

\begin{align*}
&=\frac{\underline{7}\left(6-2\sqrt{2}\right)}{\underline{7}} \\
&=6-2\sqrt{2}
\end{align*}

** Example 4
\[\frac{3\sqrt{2}-2\sqrt{5}}{2\sqrt{2}-\sqrt{5}}\]

+ So now using our method from before, we must multiply both the numerator and denominator by \(2\sqrt{2}+\sqrt{5}\):

\[=\frac{\left(3\sqrt{2}-2\sqrt{5}\right)\left(2\sqrt{2}+\sqrt{5}\right)}{\left(2\sqrt{2}-\sqrt{5}\right)\left(2\sqrt{2}+\sqrt{5}\right)}\]

+ A small thing to remember about Difference of Squares is that the middle 2 multiplications always cancel each other out; you can always only do the first and last multiplications to make things quicker

\begin{align*}
&=\frac{6\sqrt{4}+3\sqrt{10}-4\sqrt{10}-2\sqrt{25}}{8-5} \\
&=\frac{-\sqrt{10}+2}{3}
\end{align*}

* Complex and Imaginary Numbers
+ As has been discussed previously, getting the Square Root of a Negative Number isn't possible while staying within the Real Number System
+ This is because an expression such as \(\sqrt{-1}\) asks "What number can be multiplied by itself to get -1?"
  + Squaring a positive number gains a positive result, and squaring a negative number also gets a positive result making this an impossible question
  + Plugging \(\sqrt{-1}\) into a regular Scientific Calculator while likely give a "Math Error" or if it's /really/ fancy, it will print out the letter i

** Some Historical Background
+ Prior to the middle of the 17th Century, Mathematicians had a problem that they'd encounter with some Quadratic Functions
  + They would encounter Discriminants (the part of the Quadratic Formula that lies under the Radical) which would evaluate into a Negative number and thus couldn't be solved
+ These Quadratics were simply dismissed as impossible and absurd
  + They considered the Equation to entirely have no Roots/solutions because the Square Root of Negative Numbers was not defined in any way
+ What they didn't consider is that their problem could be solved by extending the Number System
+ This thinking was similar to the following examples:
  + \(x+3=0\) has no Roots in the set of Natural Numbers, but it does have the Solution \(-3\) in the set of Integers
  + \(2x+1=0\) has no Roots in the set of Integers, but it does have the solution \(-\frac{1}{2}\) in the set of Rational Numbers
+ A rather similar problem occurred with *Cubics* (\(ax^{3}+bx^{2}+cx+d\)) and Complex Numbers that ties into this as well, but that's a story for a [[https://www.youtube.com/watch?v=cUzklzVXJwo][different]] person to explain

** Defining Imaginary
+ So, how did Mathematicians work around this dilemma in the end? They created a unit called Imaginary and defined it as the following:

\[i=\sqrt{-1} \lor i^{2}=-1\]

+ And so with this unit, a Quadratic such as \(x^{2}+1=0\) suddenly has solutions i and -i
  + when \(x=i\), the left side of the equation becomes \(i^{2}+1\) or \(-1+1\) which evaluates into 0
  + when \(x=-i\), the left side of the equation becomes \((-i)^{2}+1\), the negative squares away, again giving us \(-1+1\)
+ Now, since there is no Real number with a negative Square Root, i simply isn't a Real number
  + Because of this, i cannot be expressed as a decimal nor can it be placed on a Number line

** Working with Imaginary Numbers
*** Example 1
\[\sqrt{-16}\]

+ Similarly to what we've been doing with all other Square Roots, -16 can be factored into it's pieces
+ In this case, what we'd find useful would be to factor \(\sqrt{-16}\) into \(\sqrt{16}\sqrt{-1}\)
+ Evaluating both of these, we end up with the result \(4i\) (remember: i is just a shorthand way of writing \(\sqrt{-1}\))

\[\therefore \sqrt{-16}=4i\]

*** Example 2
\[x^{2}-6x+13=0\]

+ Now we're moving onto a Quadratic Equation! Remember that the two ways of solving these are to Factor or to use the Quadratic Formula
+ 13 is a completely Prime Number and as such there are no pair of Numbers that add to -6 and multiply to 13, and so this is Unfactorable
+ Let's try plugging in our values into the Quadratic Formula then:

\begin{align*}
x&=\frac{6\pm\sqrt{(-6)^{2}-4(13)}}{2} \\
&=\frac{6\pm\sqrt{36-52}}{2} \\
&=\frac{6\pm\sqrt{-16}}{2} \\
&=\frac{6\pm\sqrt{16}\sqrt{-1}}{2} \\
&=\frac{6\pm4i}{2}
\end{align*}

+ And so as you can see, although we still have no way to get solutions within the Real Number System, by using Complex numbers we're at least able to represent what our solutions would be in a cleaner way

\[\therefore x=3+2i \lor x=3-2i\]

+ We can even check that these Roots are valid by plugging them back into the original formula
  + Treat Complex terms as if the i were a regular variable; they can only be collected with /other/ Complex terms of the same degree

when x=3+2i

\begin{align*}
0&=(3+2i)^{2}-6(3+2i)+13 \\
&=(3+2i)(3+2i)-6(3+2i)+13 \\
&=9+6i+6i+4i^{2}-18-12i+13 \\
&=4i^{2}+4 \\
&=4\left(-1\right)+4 \\
&=4-4 \\
&=0
\end{align*}

If you're confused, remember that since \(i=\sqrt{-1}\), then that means \(i^{2}\) cancels out the Radical giving us just -1

when x=3-2i

\begin{align*}
0&=(3-2i)^{2}-6(3-2i)+13 \\
&=(3-2i)(3-2i)-6(3-2i)+13 \\
&=9-6i-6i-4i^{2}-18+12i+13 \\
&=4i^{2}+4 \\
&=4\left(-1\right)+4 \\
&=4-4 \\
&=0
\end{align*}

** Exponential Properties of Imaginary
+ Even with as simple of a definition as \(i=\sqrt{-1}\), there is a lot of strange patterns and properties that can be applied to i
+ Let's, for instance, run through i being raised to various degrees ranging from degree 3 to 5, and you'll see that we get some unique results

*** Degree 3
\[i^{3}=-i\]

+ Expanding \(i^{3}\), we get \(i^{3}=\sqrt{-1}\sqrt{-1}\sqrt{-1}\)
+ The first multiplication cancels out the Radicals leaving us with just -1 and leaving us with \(i^{3}=-1\sqrt{-1}\)
+ While there is no way to further evaluate this, \(\sqrt{-1}\) can be expressed as i and so we get \(i^{3}=-1i\) or simply \(i^{3}=-i\)

*** Degree 4
\[i^{4}=1\]

+ Expanding \(i^{4}\), we get \(i^{4}=\sqrt{-1}\sqrt{-1}\sqrt{-1}\sqrt{-1}\)
+ This can be read essentially as \(i^{4}=\left(\sqrt{-1}\sqrt{-1}\right)\left(\sqrt{-1}\sqrt{-1}\right)\) which evaluates into \(i^{4}=-1\cdot-1\)
+ This is why finally \(i^{4}=1\)

*** Degree 5
\[i^{5}=i\]

+ This is maybe the strangest power of i because we've never really seen an instance where a base raised to a power greater than 1 is equal to the base
+ Expanding \(i^{5}\), we get \(i^{5}=\sqrt{-1}\sqrt{-1}\sqrt{-1}\sqrt{-1}\sqrt{-1}\)
+ As explained before, the first 4 multiplications here evaluate into 1 because squaring a square root cancels both operations leaving us with just \(i^{5}=-1\cdot-1\cdot-1\cdot-1\sqrt{-1}\)
+ This evaluates cleanly into \(i^{5}=1\sqrt{-1}\) or just \(i^{5}=\sqrt{-1}\) and of course, this means that \(i^{5}=i\)

*** Higher Degrees
+ From here, these patterns repeat, starting again at \(i^{6}=-1\) meaning that \(i^{2}\equiv i^{6}\)
+ This also means that the Function \(f(x)=i^{x}\) is *Periodic* (You'll learn about Functions, Exponential Functions, and Periodic Functions all later on)

* Partial Factoring
+ In Grade 10, we've always used the methods of Completing the Square as well as averaging out the Zeroes of a Quadratic to find it's Vertex
+ The problem with Completing the Square/converting to Vertex Form is it's slow and the problem with Factoring or using the Quadratic Formula is it assumes our Quadratic has Zeroes
  + All Quadratics have Zeroes but sometimes those Zeroes are Imaginary and Averaging out Imaginary Numbers can, as you may understand, get tricky
+ The thing is, every Quadratic Function may not always have Real X-intercepts, but *they always have Real y-intercepts* so what if we try to find the coordinates of the 2 points when along our y-intercept instead of when along x-intercept?

** The Process of Partial Factoring
\[y=5x^{2}+40x+100\]

+ Take this Quadratic for example; we could factor this or Complete the Square but we can also modify our process for Completing the Square to look for y-intercepts instead of x-intercepts
+ Start by factoring \(ax\) out of the first 2 terms instead of just \(a\) to get \(y=5x\left(x+8\right)+100\)
+ Now since we're looking for y-intercepts, let's make this equal to our y-intercept which we know from our c term: \(100=5x\left(x+8\right)+100\)
+ Now we just solve for x; we already know since we're looking for our y-intercept that one of our x's will be 0, but what about the other? \(0=5x\left(x+8\right)\)
+ As you can see, pulling the Constants together cancels them out and now we can read our solutions: \(\therefore x=0 \lor x=-8\)
+ Now where these come in handy is when we average them out to find our axis of symmetry; since one of the x's is 0, we can essentially just take half of the other one to know our axis of symmetry is \(x=-4\)
+ Finally, Let's plug that into our initial formula to get what our Optimal Value is for a complete vertex:

\begin{align*}
f(-4)&=5(-4)^{2}+40\left(-4\right)+100 \\
&=80-160+100 \\
&=20
\end{align*}

\therefore The vertex is at \(\left(-4,20\right)\)

** Example 1
\[y=-6x^{2}+24x+72\]

+ Now let's try going a little faster; start by factoring \(ax\) out of the first 2 terms

\[y=-6x\left(x-4\right)+72\]

+ At this point we can ignore the constant at the end since it cancels out when looking for what our x coordinates are at the y-intercept anyways and just read our roots for the y-intercept: \(x=0 \lor x=4\)
+ We take half of the second x value and plug it back into the function

\begin{align*}
f(2)&=-6\left(2\right)^{2}+24\left(2\right)+72 \\
&=-24+48+72 \\
&=96
\end{align*}

\therefore The vertex is at \(\left(2,96\right)\)

** Example 2
\[y=-2x^{2}+12x-3\]

+ Again, we'll go faster to emphasize how quickly this method can be done:

\begin{align*}
y&=-2x\left(x-6\right)-3 \\
&\therefore x=0 \lor x=6 \\
f(3)&=-2\left(3\right)^{2}+12\left(3\right)-3 \\
&=-18+36-3 \\
&=51
\end{align*}

\therefore The vertex is at \(\left(3,51\right)\)

** A More Formulaic Approach to the exact same thing
+ Another way to think of this is when you have a standard form Quadratic, your axis of symmetry is always \(\frac{-b}{2a}\)
+ Back to the example of \(y=-2x^{2}+12x-3\), we know that the x we'd need to use as our input is \(-\frac{12}{\left(2\right)\left(-2\right)}\) which evaluates into 3
+ You know this, you have an incredibly quick way to find the Vertex of a Parabola

* Revisiting Applications of Quadratics
** Example 1
The height of a ball thrown upwards from a rooftop is modelled by \(h=-5t^{2}+20t+50\) wherein h is the ball's height in metres at time t seconds

*** Determine the /maximum/ height of the ball
+ What we would've done to find the vertex value in Gr10 would've been completing the square but we know better now
+ Let's start by finding our axis of symmetry

\begin{align*}
t&=\frac{-b}{2a} \\
&=\frac{-20}{-10} \\
&=2 \\
&\therefore t=2
\end{align*}

+ Now to find our maximum height we must input 2 into the function above

\begin{align*}
f\left(2\right)&=-5\left(2\right)^{2}+20\left(2\right)+50 \\
&=-20+40+50 \\
&=70
\end{align*}

\therefore The Maximum height of the ball is 70m.

*** How long does it take for the ball to reach the /maximum/ height?
+ In the process of finding the Optimal Value previously, we found what our corresponding t value already
\therefore It takes the ball 2s to reach it's maximum height.

*** How high is the rooftop?
+ For this question, you need to visualize the Parabola
  + The ball is going up then down like the slope of a Parabola and we can think of the y-intercept as the top of the building the ball was thrown at
+ And so we just must look at our y-intercept for this which we already had when the Function was in standard form
\therefore The rooftop is 50m high.

*** When does the ball hit the ground?
+ /Now/ we must once again do some work. This question is asking for our Roots so we must either factor it or use the Quadratic Formula
+ Let's begin by pulling out our Greatest Common Factor of -5

\[0=-5\left(t^{2}-4t-10\right)\]

+ This expression cannot be factored and so we must use the Quadratic Formula

\begin{align*}
t&=\frac{4\pm\sqrt{\left(-4\right)^{2}-4\left(-10\right)}}{2} \\
&=\frac{4\pm\sqrt{16+40}}{2} \\
&=\frac{4\pm\sqrt{56}}{2} \\
&=\frac{4\pm2\sqrt{14}}{2} \\
&=\frac{\underline{2}\left(2\pm\sqrt{14}\right)}{\underline{2}} \\
&=2\pm\sqrt{14} \\
&\therefore t\approx5.742 \lor t\approx-1.742
\end{align*}

\therefore The ball hits the ground in around 5.742s.

** Example 2
A rectangular lot is bound on one side by a river and on the other 3 sides by a total of 80m of fencing. Find the dimensions of the largest possible lot.

+ Here, we need to use let statements to assign variables to a few things and then make some equations to work with:

Let x and y be the lengths of the different sides of the fence \land let A be the maximum area of the lot

+ our 2 equations are now as follows:

\begin{align*}
2x+y&=80 \\
A&=xy
\end{align*}

+ If you remember from working with Linear Systems, we must now isolate one of the variables that appear on both equations so we can substitute it into the other equation:

\begin{align*}
2x+y=80 &\rightarrow y=-2x+80 \\
xy=A &\rightarrow x\left(-2x+80\right)=A
\end{align*}

+ Now, last year we would have expanded this and then completed the square to find our vertex, but we can now instead work with this in it's current form speeding things up significantly!
+ From this partially factored form of the Function, we know \(x=0 \lor x=40\) which means our Axis of symmetry is \(x=20\)

\begin{align*}
A&=\left(20\right)\left(-2\left(20\right)+80\right) \\
&=\left(20\right)\left(40\right) \\
&=800
\end{align*}

+ And so we know that our vertex is (20,800) for (x,A) and so we know that our maximum area can be \(800m^{2}\)
+ The problem asks us, however, for the /dimensions/ of the maximum area, and so we still need to find our y-value when our x-value is at the maximum

\begin{align*}
y&=-2\left(20\right)+80 \\
&=40
\end{align*}

\therefore The dimensions of the largest possible lot are \(20m\times40m\)

** Example 3
Find the /minimum/ product of two numbers with a difference of 12.

+ Again, we must use Let statements to assign 3 variables for our equations:

Let x and y be our unknown numbers \land let P be their product

\begin{align*}
x-y=12 &\rightarrow x=y+12 \\
xy=P &\rightarrow \left(y+12\right)y=P \\
&\therefore y=0 \lor y=-12
\end{align*}

And so we know that our axis of symmetry is -6, and so we carry on:

\begin{align*}
\left(y+12\right)y=P &\rightarrow \left(\left(-6\right)+12\right)\left(-6\right)=P \\
-36&=P
\end{align*}

\therefore The minimum product of the 2 numbers is -36 when our 2 numbers are 6 and -6.

* Linear-Quadratic Systems
+ A Linear-Quadratic System is one that involves both a Quadratic and a Linear Function
+ To find the Roots/Solutions of a Linear-Quadratic System, you must determine all the points at which these 2 Functions *intersect* (what points they share)

** Possible Nature of Roots
+ As with regular Quadratic Functions, you will always have 2 Roots
+ The most common possibility is 2 different *distinct* Roots in \(\mathbb{R}\)
  + This means that on a graph, your Line runs through the Parabola and intersects with it twice
  + When this happens, the Line is called a *Secant Line*
+ Another possibility is having 2 different *identical* Roots in \(\mathbb{R}\)
  + This means that on a graph, your Line runs through the Parabola in such a way where it only touches it once
  + When this happens, the Line is called a *Tangent Line*
+ The final possibility is having no Roots in \(\mathbb{R}\) or better known as having 2 Roots in \(\mathbb{I}\)
  + This means that on a graph, your Line does not intersect through the Parabola at all
  + There is not a specific name for a Line like this, and similar to having 2 Imaginary Roots in a Quadratic Function, algebraically it is still possible to find these Roots

** Example 1
\begin{align*}
y&=-2x^{2}-5x+20 \\
y&=6x-1
\end{align*}

+ Here we have a Quadratic and a Linear Function and we want to find along what points they intersect
+ The main way of finding Roots to a Linear-Quadratic System starts as a Linear System would; do either Substitution or Elimination
  + This specific example is set up perfectly for Substitution as y is isolated so let's go with that

\[6x-1=-2x^{2}-5x+20\]

+ Now we have what is a single Quadratic Function; to solve Quadratics, we must make one side of the equal sign be 0 so let's move things around

\begin{align*}
6x-1+2x^{2}+5x-20&=0 \\
2x^{2}+11x-21&=0
\end{align*}

+ Now we can just solve this as we would any Quadratic Function; let's try factoring

\begin{align*}
\left(x+\frac{14}{2}\right)\left(x-\frac{3}{2}\right)&=0 \\
\left(x+7\right)\left(2x-3\right)&=0 \\
\therefore x=-7 \lor x&=\frac{3}{2}
\end{align*}

+ Since we have gotten 2 different x-coordinates in \(\mathbb{R}\), we know this is a Secant Line
+ Now we must plug these 2 x values into either of our original Functions to get our corresponding y-value; we're gonna choose the Linear Function since it's easier and they both give the same output with the same input

\begin{align*}
f\left(-7\right)&=6\left(-7\right)-1 \\
&=-42-1 \\
&=-43 \\
f\left(\frac{3}{2}\right)&=6\left(\frac{3}{2}\right)-1 \\
&=9-1 \\
&=8 \\
&\therefore \left(-7,-43\right) \land \left(\frac{3}{2},8\right)
\end{align*}
