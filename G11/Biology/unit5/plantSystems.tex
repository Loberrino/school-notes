% Created 2024-01-17 Wed 14:20
% Intended LaTeX compiler: pdflatex
\documentclass[letterpaper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage{gensymb}
\usepackage{fourier}
\usepackage{tikz}
\author{Arad Fathalian}
\date{\today}
\title{Plant Systems}
\hypersetup{
 pdfauthor={Arad Fathalian},
 pdftitle={Plant Systems},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 29.1 (Org mode 9.7)}, 
 pdflang={English}}
\begin{document}

\maketitle
\section*{Plant Structure and Function}
\label{sec:orgb147f8f}
\begin{itemize}
\item The \textbf{Shoot} System of a Plant is above ground, and the \textbf{Root} System is below-ground
\item The Shoot System carries out Photosynthesis, transports food and water, carries the reproductive system, and does general storage
\item The Root System does things such as anchoring the Plant and gathering Nutrients from within the ground
\end{itemize}
\subsection*{Root System}
\label{sec:orgc512450}
\begin{itemize}
\item \textbf{Fibrous} Roots split into many different directions
\item \textbf{Tap} Roots are one big central Root stemmed into the ground
\begin{itemize}
\item Carrots are a great example of a Tap-rooted Plant
\end{itemize}
\item Finally, \textbf{Adventitious} Roots plant roots that form from any non-root tissue such as Stems or Leaves
\begin{itemize}
\item These are basically the exception class of the Root Systems
\end{itemize}
\item Water moves into the roots via \textbf{Osmosis}, and minerals from the soil enter through active transport into the root hair Cells
\item Water as well as dissolved minerals then travel around the plant in the \textbf{Xylem}
\end{itemize}
\subsubsection*{Xylem}
\label{sec:orgb0cc902}
\begin{itemize}
\item Xylem is a type of Vascular tissue; an internal system of one-way (lower to upper) tubes that run lengthwise throughout the Stem of a plant of a plant and connect the roots to the leaves
\item This allows for water and dissolved solutes to travel throughout the plant; much like our Circulatory or Digestive System
\item Most Xylem Cells are actually dead and form the main parts of wood and bark
\item Xylem tubes are usually directly next to Phloem tubes
\end{itemize}
\subsection*{Shoot System}
\label{sec:org57541a7}
\subsubsection*{Stem}
\label{sec:org9f1047f}
\begin{itemize}
\item The Stem of the Shoot System supports branches, leaves, and flowers
\item It also transports substances between the Root System and the leaves
\item Stems can bend towards light in a process called \textbf{Phototropism}
\begin{itemize}
\item This is caused by a hormone called \textbf{Auxin}
\end{itemize}
\end{itemize}
\subsubsection*{Leaves}
\label{sec:org4601e0a}
\begin{itemize}
\item This is where Photosynthesis occurs!
\item Leaves are designed to capture the maximum amount of Sunlight and minimize water loss
\item Leaves have different layers of Cells which include:
\begin{itemize}
\item A \textbf{Cuticle} which is a waxy outer layer to reduce water loss
\item The \textbf{Epidermis} which protects the plant; it's the second-most outer layer behind the thin Cuticle
\item The \textbf{Palisade Mesophyll} where Photosynthesis happens
\item The \textbf{Spongy Mesophyll} which allows for easy movement of Oxygen and Carbon Dioxide
\end{itemize}
\item The underside of leaves have \textbf{Stomata} pores which allow, Oxygen, Carbon Dioxide, and Water Vapour to enter and leave the leaf
\begin{itemize}
\item Guard Cells surround each Stoma and control their opening and closing
\item Stoma generally open during the day and close during the night
\end{itemize}
\end{itemize}
\subsubsection*{Phloem}
\label{sec:org54090e9}
\begin{itemize}
\item Whereas Xylem starts at the Roots and goes up, Phloem starts at the Leaves and goes down
\item Sugars that are produced from Photosynthesis travel in this system of Vascular tubes
\item Phloem is composed of \textbf{living cells} and it actually supports two-way movement
\item Phloem is made of \textbf{sieve tube elements} and associated companion cells
\begin{itemize}
\item The Sieve Tube Cells themselves don't have nuclei, and rely heavily on the Companion Cells
\end{itemize}
\item Sieve tube Elements have end wall plates with perforated holes in them
\item Unlike Xylem, Phloem does not utilize Osmosis and so it needs \textbf{Active} transport to carry the sugars and other nutrients
\item Phloem tubes are usually directly next to Xylem tubes
\end{itemize}
\section*{Plant Reproductive Systems}
\label{sec:org5e33820}
\begin{itemize}
\item Plant Reproduction can be Sexual or Asexual
\item Asexual reproduction produces identical plants; clones, essentially
\begin{itemize}
\item That's to say it occurs through Mitosis
\end{itemize}
\item Sexual reproduction involves the union of two haploid gametes, being pollen and the ovules, to produce a seed
\item This can be achieved through using rhizomes and tubers, as well as cuttings and grafts
\begin{itemize}
\item A \textbf{Rhizome} is a horizontal underground plant stem that's capable of producing the shoot and root systems of a new plant
\item Ginger can be an example of a Rhizome
\item A \textbf{Tuber} is a type of enlarged structure used as storage organs for nutrients in some plants that can then grow the stems of new plants
\item A Potato is an example of a Tuber; the Potato itself is a Tuber for the Potato plant
\end{itemize}
\item So what Rhizomes do is stretch out horizontally and create a new plant entirely
\begin{itemize}
\item This can make the appearance of multiple different plants but they all come from the same Rhizome
\end{itemize}
\end{itemize}
\subsection*{Gymnosperms}
\label{sec:org446ca05}
\begin{itemize}
\item Gymnosperms are \emph{cone}-bearing plants and they produce both male and female cones in separate parts of the plant
\item Pollination occurs when Pollen from the male cone is transferred to an Ovule of a female cone; this usually happens via wind
\item Once a pollen grain reaches a female cone, a pollen tube forms, carrying the Nucleus of the pollen to the Ovule
\item Fertilization then occurs and a seed forms; if this seed germinates, a new plant forms
\begin{itemize}
\item Germination is the development of a new plant from a seed after a period of remaining dormant
\end{itemize}
\item Seeds are typically distributed by Animals and Wind
\end{itemize}
\subsection*{Angiosperms}
\label{sec:orgd859596}
\begin{itemize}
\item Angiosperms have \emph{flowers} to act as the key organs in sexual reproduction
\item The \textbf{Stamen} is the male reproductive part; it's composed of \textbf{Filament}, and an \textbf{Anther} on top which actually ejects the Pollen
\item The \textbf{Pistil} is the female reproductive part; it's composed of \textbf{Stigma}; a sticky surface to trap Pollen, the \textbf{Style}, which is a tube-like structure that leads to the Ovary
\item The \textbf{Ovary} stores the \textbf{Ovules} which are the eggs of the Plant, sort of
\item Once a pollen grain reaches the Stigma, it grows a Pollen Tube down the Style into the Ovary
\item Then, it fertilizes an Ovule and turns into a Seed
\item The surrounding Ovary matures and turns into a \textbf{fruit} with seeds within
\item Fruit helps protect and disperse the seeds; the head of the Flower with Petals falls off to make space for the Fruit
\end{itemize}
\subsubsection*{Angiosperm Reproductive System types}
\label{sec:org8c7c464}
\begin{itemize}
\item A majority of Plants are, in truth, actually \textbf{Monoecious}
\begin{itemize}
\item That is to say that the single plant organism has both Male \emph{and} Female Reproductive Organs
\end{itemize}
\item \textbf{Dioecious} Plants, in contrast, are very much like us with separate Reproductive Systems per Organism
\item From there, they can be categorized into \textbf{Hermaphrodites} if the Male and Female Reproductive Systems exist on the same Flower
\item This means that there are Monoecious Plants; more likely Hermaphroditic Plants, that can pollinate themselves
\item When an Angiosperm reproduces with another individual, it's called \textbf{Cross-Pollination}
\item Cross-Pollination requires Pollinators such as Bees and Hummingbirds or Wind
\item Pollinators go for the Nectar that plants produce and all the while transport Pollen
\end{itemize}
\end{document}
